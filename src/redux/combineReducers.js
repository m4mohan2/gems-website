import { combineReducers } from 'redux';

// Import reducer files
import Auth from './reducers/auth';
//import Registration from "./reducers/registration";
//import Payment from "./reducers/payment";
import Invoice from './reducers/invoice';
import business from './reducers/business';
import businessRegistration from './reducers/businessRegistration';
import businessEdit from './reducers/businessEdit';
import username from './reducers/username';
import product from './reducers/product';

export const AppCombineReducers = combineReducers({
    auth: Auth,
    business: business,
    product: product,
    businessRegistration: businessRegistration,
    businessEdit: businessEdit,
    //registration: Registration,
    //payment : Payment,
    invoice : Invoice,
    user: username
});
