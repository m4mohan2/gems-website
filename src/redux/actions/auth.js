import {
	AUTH_START,
	AUTH_SUCCESS,
	AUTH_FAIL,
	AUTH_LOGOUT,
	ACCESS_TOKEN,
	AUTH_ALL_DATA_UPDATE,
	USER_NAME,
	AUTH_LOGOUT_MSG
} from './actionTypes';
import axios from './../../shared/eaxios';
import * as AppConst from '../../common/constants';


export const authenticationStart = () => {
	return {
		type: AUTH_START
	};
};

export const authenticationFail = (error) => {
	return {
		type: AUTH_FAIL,
		error
	};
};

export const authenticationSuccess = (token, id) => {
	return {
		type: AUTH_SUCCESS,
		token,
		id
	};
};

// export const logout = () => {
//     return {
//         type: AUTH_LOGOUT
//     };
// };

export const logout = () => {
	return dispatch => {
		axios({
			method: 'post',
			url: AppConst.APIURL + '/api/logout',
		}).then((result) => {
			//console.log('logout action result', result);
			dispatch({
				type: AUTH_LOGOUT,
				payload: result.data
			});

			dispatch({
				type: USER_NAME,
				payload: {
					firstName: null,
					lastName: null,
					timeStamp: (new Date()).getTime()
				}
			});
		}).catch((error) => {
			dispatch({
				type: AUTH_LOGOUT,
				payload: error
			});
			dispatch({
				type: USER_NAME,
				payload: {
					firstName: null,
					lastName: null,
					timeStamp: (new Date()).getTime()
				}
			});
		});
	};
};

export const logoutMsg = (data) => {
	return {
		type: AUTH_LOGOUT_MSG,
		logoutMsg: data
	};
};

export const accessToken = (key, token) => {
	return {
		type: ACCESS_TOKEN,
		token,
		authToken: key
	};
};

export const authDataUpdate = (data) => {
	return {
		type: AUTH_ALL_DATA_UPDATE,
		data
	};
};

export const onAuthenticationProcess = () => {
	return dispatch => {
		dispatch(authenticationStart());
		// Process Request
		// success
		// failure
	};
};
