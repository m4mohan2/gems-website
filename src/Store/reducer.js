const initialState = {
    loginDetails: {
        token: '',
        active_status: '',
        email: '',
        first_name: '',
        last_name: '',
        user_id: '',
        profile_pic: ''
    },
    userDetails: ''
}

const reducer = (state = initialState, action) => {
    if (action.type === 'SAVE_LOGINDETAILS') {
        return {
            ...initialState,
            loginDetails: action.login
        }
    }
    if (action.type === 'SAVE_USERDETAILS') {
        return {
            ...initialState,
            userDetails: action.user
        }
    }
    return state;
};

export default reducer;