import React from 'react';
//import { HashRouter as Router, Route, Switch } from 'react-router-dom';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import './App.scss';
import * as AppConst from './common/constants';
import Header from './component/Header/header';
import Footer from './component/Footer/footer';
import Home from './component/Home/home';
import Explore from './component/Explore/explore';
import ClaimYourGem from './component/Claim-your-gem/claim-your-gem';
import ClaimYourGemNoResult from './component/Claim-your-gem/claim-your-gem-no-result';
import ClaimYourGemSearchResult from './component/Claim-your-gem/claim-your-gem-search-result';
import GemDetails from './component/Gem-details/gem-details';
import ResetPassword from './component/Reset-password/reset-password';
import HowItWorks from './component/How-it-works/how-it-woks';
import ErrorPage from './component/Error/error-page';
import GemsProvider from './component/Provider/provider';

import News from './component/News/news';
//import Cms from './component/Cms/cms';
import About from './component/Cms/about';
import Contact from './component/Cms/contact';
import PrivacyPolicy from './component/Cms/privacy-policy';
import TermsAndCondition from './component/Cms/terms-and-condition';
import WhatIsGems from './component/Cms/what-is-gems';
import Help from './component/Cms/help';
import Blog from './component/Cms/blog';
import DownloadApp from './component/Cms/download-app';
import Coupon from './component/Coupon/coupon';
import Dashboard from './component/Dashboard/dashboard';
import PaymentOrders from './component/Payment-orders/payment-orders';
import AccountVerify from './component/Account-verify/account-verify';

class App extends React.Component {
	render() {
		return (
			<div className="App">
				<Router>
					<Header />
					<Switch>
						<Route exact path={AppConst.SERVERFOLDER + "/"} component={Home} />
						<Route exact path={AppConst.SERVERFOLDER + "/dashboard"} component={Dashboard} />
						<Route exact path={AppConst.SERVERFOLDER + "/explore"} component={Explore} />
						<Route exact path={AppConst.SERVERFOLDER + "/claim-your-gem"} component={ClaimYourGem} />
						<Route exact path={AppConst.SERVERFOLDER + "/claim-your-gem-no-result"} component={ClaimYourGemNoResult} />
						<Route exact path={AppConst.SERVERFOLDER + "/claim-your-gem-search-result"} component={ClaimYourGemSearchResult} />
						<Route exact path={AppConst.SERVERFOLDER + "/gem-details"} component={GemDetails} />
						<Route exact path={AppConst.SERVERFOLDER + "/reset-password/:token"} component={ResetPassword} />
						<Route exact path={AppConst.SERVERFOLDER + "/how-it-works"} component={HowItWorks} />
						<Route exact path={AppConst.SERVERFOLDER + "/about"} component={About} />
						<Route exact path={AppConst.SERVERFOLDER + "/contact"} component={Contact} />
						<Route exact path={AppConst.SERVERFOLDER + "/privacy-policy"} component={PrivacyPolicy} />
						<Route exact path={AppConst.SERVERFOLDER + "/terms-and-condition"} component={TermsAndCondition} />
						<Route exact path={AppConst.SERVERFOLDER + "/what-is-gems"} component={WhatIsGems} />
						<Route exact path={AppConst.SERVERFOLDER + "/help"} component={Help} />
						<Route exact path={AppConst.SERVERFOLDER + "/blog"} component={Blog} />
						<Route exact path={AppConst.SERVERFOLDER + "/download-app"} component={DownloadApp} />
						<Route exact path={AppConst.SERVERFOLDER + "/news/:slug"} component={News} />
						<Route exact path={AppConst.SERVERFOLDER + "/provider"} component={GemsProvider} />
						<Route exact path={AppConst.SERVERFOLDER + "/coupon"} component={Coupon} />
						<Route exact path={AppConst.SERVERFOLDER + "/paymentdetails"} component={PaymentOrders} />
						<Route exact path={AppConst.SERVERFOLDER + "/verify-account/:token"} component={AccountVerify} />
						<Route exact path={AppConst.SERVERFOLDER + "/error"} component={ErrorPage} />
						<Route component={ErrorPage} />
						{/* <Redirect from='*' to='/error' /> */}

					</Switch>
					<Footer />
				</Router>

			</div>
		);
	}
}

export default App;
