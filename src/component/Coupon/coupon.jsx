import React, { Component, Fragment } from 'react';
import { Link } from 'react-router-dom';
import { Button, Col, FormControl, FormGroup, Modal, Row, Image, Table } from 'react-bootstrap';
import { Field, Form, Formik } from 'formik';
import Moment from 'react-moment';
import moment from 'moment';
import LoadingSpinner from '../../common/LoadingSpinner/LoadingSpinner';
import { fetchCoupon, couponStatusChange, addCoupon, viewCouponDetails, deleteCoupon, userList, assignCoupon, redeemCoupon, editCoupon } from '../../action/cmsAction';
import refreshIcon from '../../assets/images/refreshIcon.png';
import Pagination from 'react-js-pagination';
import SuccessIco from '../../assets/images/success-ico.png';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import * as Yup from 'yup';
import Select from 'react-select';
import * as AppConst from '../../common/constants';

const initialValues = {
	coupon_type: '',
	amount: '',
	amount_type: '',
	start_date: '',
	end_date: '',
	description: '',
	active_status: ''
};

const addCouponSchema = Yup.object().shape({
	coupon_type: Yup.string()
		.trim('Please remove whitespace')
		.strict()
		.required('Please select coupon type'),
	amount: Yup.string()
		.when('coupon_type', {
			is: '0',
			then: Yup.string()
				.strict()
				.required('Please enter amount'),
		}),
	amount_type: Yup.string()
		.when('coupon_type', {
			is: '0',
			then: Yup.string()
				.strict()
				.required('Please select amount type'),
		}),
	description: Yup.string()
		.trim('Please remove whitespace')
		.strict()
		.required('Please enter description'),
	active_status: Yup.string()
		.trim('Please remove whitespace')
		.strict()
		.required('Please select status')
});

const editCouponSchema = Yup.object().shape({
	coupon_type: Yup.string()
		.trim('Please remove whitespace')
		.strict()
		.required('Please select coupon type'),
	amount: Yup.string()
		.when('coupon_type', {
			is: '0',
			then: Yup.string()
				.strict()
				.required('Please enter amount').nullable(),
		}),
	amount_type: Yup.string()
		.when('coupon_type', {
			is: '0',
			then: Yup.string()
				.strict()
				.required('Please select amount type').nullable(),
		}),
	description: Yup.string()
		.trim('Please remove whitespace')
		.strict()
		.required('Please enter description'),
	active_status: Yup.string()
		.required('Please select status')
});

class Coupon extends Component {
	state = {
		activePage: 1,
		totalCount: 0,
		itemPerPage: 10,
		sort: 1,
		fetchErrorMsg: null,
		couponList: [],
		loading: true,
		sortingActiveID: 1,
		statusChange: false,
		statuserrorMsg: null,
		couponId: null,
		status: null,
		couponSearchKey: '',
		viewAssignModel: false,
		assignCouponConfirMmsg: false,
		couponDetails: '',
		viewCouponDetails: false,
		viewRedeemCouponModel: false,
		redeemCouponConfirMmsg: false,
		loadingForCouponDetail: false,

		showConfirMmsg: false,
		errorMessge: null,
		addAssignCouponLoader: false,
		addErrorMessge: null,
		coupon_id: '',
		coupon: '',
		user: [],
		selectedOption: '',
		assignUser: '',
		editAssignCoupon: false,
		successData: '',
		assignuserdetails: '',

		addRedeemCouponLoader: false,

		showCoupon: false,
		editCouponEnable: false,
		disabled: false,
		editCouponLoader: false,
		editErrorMessge: false,
		startDate: '',
		endDate: '',
		startDateAddCoupon: '',
		endDateAddCoupon: '',
		coupondata: '',

		coupondataforredeem: ''
	}

	displayError = (e) => {
		let errorMessge = '';
		try {
			errorMessge = e.data.message ? e.data.message : e.data.error_description;
		} catch (e) {
			errorMessge = 'Access is denied!';
		}
		return errorMessge;
	}

	componentDidMount() {
		if (localStorage.getItem('token') === null) {
			this.props.history.push(AppConst.SERVERFOLDER);
		}
		window.scrollTo(0, 0);
		this.fetchCoupon();
		userList().then(res => {
			this.setState({ addAssignCouponLoader: false, user: res.data.data });
		});
	}

	fetchCoupon = (sort = this.state.sort, field = this.state.field) => {
		this.setState({
			loading: true,
			sort: sort,
			field: field
		}, () => {
			fetchCoupon(this.state.itemPerPage, this.state.sort, this.state.couponSearchKey)
				.then(res => {
					const couponList = res.data.data.data;
					const totalCount = res.data.data.total;
					this.setState({
						couponList,
						totalCount: totalCount,
						loading: false
					});
				})
				.catch(e => {
					let errorMsg = this.displayError(e);
					this.setState({
						fetchErrorMsg: errorMsg,
						loading: false
					});
					setTimeout(() => {
						this.setState({ fetchErrorMsg: null });
					}, 5000);
				});
		});
	}

	handleStatus(id, status) {
		couponStatusChange(id, status)
			.then(res => {
				this.setState({
					couponId: null,
					status: null,
					successMessage: res.data.message,
					statusConfirMmsg: true
				});
				this.handleHide();
				this.fetchCoupon();
			})
			.catch(e => {
				let errorMsg = this.displayError(e);
				this.setState({
					statuserrorMsg: errorMsg,
					loading: false
				});
				setTimeout(() => {
					this.setState({ errorMessge: null });
				}, 5000);
			});
	}

	handleChangeForSearch = (e) => {
		this.setState({ couponSearchKey: e.target.value.trim() });
	}

	handelSearch = () => {
		this.fetchCoupon();
	}

	resetSearch = () => {
		this.setState({ couponSearchKey: '' });
		this.fetchCoupon();
	}

	handelStatusModal = (id, status) => {
		this.setState({
			statusChange: true,
			couponId: id,
			status: status
		});
	}

	handleHide = () => {
		this.setState({
			statusChange: false,
			statuserrorMsg: null
		});
	}

	handleStatusChangedClose = () => {
		this.setState({
			statusConfirMmsg: false,
			successMessage: null
		});
	}

	handelAddModal = () => {

		this.setState({
			addModal: true,
		});
	}

	handelAddModalClose = () => {
		this.setState({
			addModal: false,
		});
	}

	handleSubmit = (values, { resetForm, setSubmitting }) => {
		this.setState({
			addCouponLoader: true,
		});
		const formData = new window.FormData();
		for (const key in values) {
			if (values.hasOwnProperty(key)) {
				formData.append(key, values[key]);
			}
		}
		let newValue = {
			coupon_type: values.coupon_type,
			amount: values.amount,
			amount_type: values.amount_type,
			start_date: moment(this.state.startDateAddCoupon).format('YYYY-MM-DD HH:mm:ss'),
			end_date: moment(this.state.endDateAddCoupon).format('YYYY-MM-DD HH:mm:ss'),
			description: values.description,
			active_status: values.active_status,
			creator_id: localStorage.getItem('user_id')
		};
		addCoupon(newValue)
			.then(res => {
				resetForm({});
				setSubmitting(false);
				this.setState({
					startDateAddCoupon: '',
					endDateAddCoupon: '',
					successMessage: res.data.message,
					statusConfirMmsg: true,
					addModal: false,
					statuserrorMsg: null,
					addCouponLoader: false,
				});
				this.fetchCoupon();
			}).catch(e => {
				let errorMsg = this.displayError(e);
				this.setState({
					addCouponLoader: false,
					addErrorMessge: errorMsg,
				});
				setTimeout(() => {
					this.setState({ deleteErrorMessge: null });
				}, 5000);
			});
	};

	handleChangeStartDateAddCoupon = date => {
		this.setState({
			startDateAddCoupon: date,
			endDateAddCoupon: ''
		});
	}

	handleChangeEndDateAddCoupon = date => {
		this.setState({
			endDateAddCoupon: date,
		});
	}

	handleChangeStartDateForEdit = date => {
		this.setState({
			startDate: date,
			endDate: ''
		});
	}

	handleChangeEndDateForEdit = date => {
		this.setState({
			endDate: date,
		});
	}

	handleCouponDetail = (code) => {
		this.setState({ loadingForCouponDetail: true, viewCouponDetails: true, couponDetails: '' });
		viewCouponDetails(code)
			.then(response => {
				this.setState({
					couponDetails: response.data,
					loadingForCouponDetail: false
				});
			})
			.catch(e => {
				let errorMsg = this.displayError(e);
				this.setState({
					fetchErrorMsg: errorMsg,
				});
				setTimeout(() => {
					this.setState({ fetchErrorMsg: null });
				}, 5000);
			});
	}

	handleCouponDetailClose = () => {
		this.setState({ viewCouponDetails: false });
	}

	handelDeleteModal = (id) => {
		this.setState({
			deleteCoupon: true,
			couponId: id
		});
	}

	handleDeleteHide = () => {
		this.setState({
			deleteCoupon: false,
			deleteerrorMsg: null
		});
	}

	handleDelete(id) {
		deleteCoupon(id)
			.then(res => {
				this.setState({
					couponId: null,
					successMessage: res.data.message,
					statusConfirMmsg: true
				});
				this.handleDeleteHide();
				this.fetchCoupon();
			})
			.catch(e => {
				let errorMsg = this.displayError(e);
				this.setState({
					deleteerrorMsg: errorMsg,
					loading: false
				});
				setTimeout(() => {
					this.setState({ errorMessge: null });
				}, 5000);
			});
	}

	handelAssignModal = (data) => {
		this.setState({
			viewAssignModel: true,
			coupon_id: data.id,
			coupon: data.coupon,
			assignUser: data.assignuser_email,
			assignuserdetails: data.assignuser
		});
	}

	handelAssignModalClose = () => {
		this.setState({
			viewAssignModel: false,
			addErrorMessge: null
		});
	}

	handleChange = (selectedOption) => {
		this.setState({ selectedOption, addErrorMessge: null });
	}

	handleeditCouponEnable = () => {
		this.setState({
			assignUser: '',
			selectedOption: ''
		});
	}

	handleAssignCouponSubmit = () => {
		if (this.state.selectedOption === '' || this.state.selectedOption === null || this.state.selectedOption === undefined) {
			this.setState({ addErrorMessge: 'Please select user' });
		} else {
			this.setState({
				addAssignCouponLoader: true,
			});
			let newValue = {
				coupon_id: this.state.coupon_id,
				user_id: this.state.selectedOption.value
			};
			console.log(newValue);
			assignCoupon(newValue)
				.then(res => {
					this.setState({
						successData: res,
						statusConfirMmsg: true,
						successMessage: res.data.message,
						viewAssignModel: false,
						statuserrorMsg: null,
						addAssignCouponLoader: false
					});
					this.fetchCoupon();
				}).catch(e => {
					let errorMsg = this.displayError(e);
					this.setState({
						addAssignCouponLoader: false,
						addErrorMessge: errorMsg,
					});
					setTimeout(() => {
						this.setState({ deleteErrorMessge: null });
					}, 5000);
				});
		}
	};

	handleRedeemCoupon = (data) => {
		this.setState({
			viewRedeemCouponModel: true,
			coupon_id: data.id,
			coupon: data.coupon,
			assignUser: data.assignuser_email,
			assignuserdetails: data.assignuser,
			coupondataforredeem: data
		});
	}

	handelRedeemCouponModalClose = () => {
		this.setState({
			viewRedeemCouponModel: false,
		});
	}

	handleRedeemCouponSubmit = () => {
		this.setState({
			addRedeemCouponLoader: true,
		});
		let newValue = {
			coupon: this.state.coupon,
			user_id: this.state.assignUser.user_id
		};
		redeemCoupon(newValue).then(res => {
			this.setState({ successData: res });
			if (res.data.success === 'false') {
				this.setState({
					addRedeemCouponLoader: false,
					addErrorMessge: res.data.message,
				});
				setTimeout(() => {
					this.setState({ addErrorMessge: null, viewRedeemCouponModel: false });
				}, 5000);
			} else {
				this.setState({
					statusConfirMmsg: true,
					successMessage: res.data.message,
					viewRedeemCouponModel: false,
					statuserrorMsg: null,
					addRedeemCouponLoader: false,
				});
				this.fetchCoupon();
			}
		}).catch(error => {
			let errorMsg = '';
			if (moment(this.state.coupondataforredeem.start_date).toDate() > new Date()) {
				errorMsg = 'Coupon will be available on ' + moment(this.state.coupondataforredeem.start_date).format('MMMM Do, YYYY HH:mm:ss');
			} else {
				errorMsg = 'Coupon has expired';
			}
			this.setState({
				addRedeemCouponLoader: false,
				addErrorMessge: errorMsg,
			});
			setTimeout(() => {
				this.setState({ addErrorMessge: null });
			}, 5000);
		});
	};

	handelviewEditModal = (data) => {
		this.setState({
			viewEditModal: true,
			coupondata: data,
			startDate: data && moment(data.start_date).toDate(),
			endDate: data && moment(data.end_date).toDate(),
		});
	}

	handelviewEditModalClose = () => {
		this.setState({
			viewEditModal: false,
		});
	}

	handleeditCoupon = () => {
		this.setState({
			editCouponEnable: true,
			disabled: true
		});
	}

	handleSubmitForEdit = (values, { setSubmitting }) => {
		this.setState({
			editCouponLoader: true,
		});
		let newValue = {
			coupon_type: values.coupon_type,
			amount: values.amount,
			amount_type: values.amount_type,
			start_date: moment(this.state.startDate).format('YYYY-MM-DD HH:mm:ss'),
			end_date: moment(this.state.endDate).format('YYYY-MM-DD HH:mm:ss'),
			description: values.description,
			active_status: values.active_status
		};
		const couponData = {};
		Object.assign(couponData, newValue);
		editCoupon(values.id, newValue)
			.then(res => {
				setSubmitting(false);
				this.setState({
					editCouponLoader: false,
					editCouponEnable: false,
					disabled: false,
					couponData,
					statusConfirMmsg: true,
					successMessage: res.data.message,
					viewEditModal: false
				});
				this.fetchCoupon();
			})
			.catch(e => {
				let errorMsg = this.displayError(e);
				this.setState({
					editCouponLoader: false,
					editErrorMessge: errorMsg,
				});

				setTimeout(() => {
					this.setState({ deleteErrorMessge: null });
				}, 1000);

			});
	};

	handelEditModalClose = () => {
		this.setState({
			viewEditModal: false,
			disabled: false,
			editCouponEnable: false,
		});
	}

	handleChangeItemPerPage = (e) => {
		this.setState({ itemPerPage: e.target.value },
			() => {
				this.fetchCoupon();
			});
	}

	handlePageChange = pageNumber => {
		this.setState({ activePage: pageNumber });
		this.fetchCoupon(pageNumber > 0 ? pageNumber : 0, this.state.field);
	};

	resetPagination = () => {
		this.setState({ activePage: 1 });
	}

	rePagination = () => {
		this.setState({ activePage: 0 });
	}

	render() {
		const options = [];
		this.state.user.map(data => {
			options.push({ value: data.id, label: data.email });
		});
		const colourStyles = {
			option: (provided) => ({
				...provided,
				color: 'black',
			}),

		};
		const couponDetails = this.state.couponDetails;
		const values = {
			id: this.state.coupondata.id,
			coupon_type: this.state.coupondata.coupon_type,
			amount: this.state.coupondata.amount,
			amount_type: this.state.coupondata.amount_type,
			description: this.state.coupondata.description,
			active_status: this.state.coupondata.active_status,
		};
		const {
			disabled
		} = this.state;
		return (
			<div>
				<div className="claimBannerPan">
					<div className="container">

						<h2>Coupon</h2>

					</div>
				</div>
				<div className="contentPan pt-4">
					<div className="container pt-4">
						<div className="row">
							<div className="col-12">
								<Row className="mb-3">
									<Col sm={8} md={8}>
										<div className="searchBox">
											<div className="searchWrap">
												<input className="form-control" type="text" placeholder="Enter coupon code here..." value={this.state.couponSearchKey} onChange={this.handleChangeForSearch} />
												<button
													type="button"
													onClick={() => this.handelSearch()}
												><i className="fa fa-search" aria-hidden="true"></i></button>
											</div>

											<Link to="#" className='refreshIcon' onClick={() => this.resetSearch()}>
												<i className="fa fa-refresh" aria-hidden="true"></i>
											</Link>

										</div>
									</Col>
									<Col sm={4} md={4}>
										<button className="btn blue float-right" onClick={() => this.handelAddModal()}>Add Coupon</button>
									</Col>
								</Row>
								<Row className="show-grid pt-1">
									<Col sm={12} md={12}>
										<div className="tableBox">
											<Table responsive hover>
												<thead className="theaderBg">
													<tr>
														<th>Coupon</th>
														<th>Amount</th>
														<th>Amount Type</th>
														<th>Start Date</th>
														<th>End Date</th>
														<th className="text-center">Staus</th>
														<th className="text-center">Action</th>
													</tr>
												</thead>
												<tbody>
													{this.state.loading ? (<tr><td colSpan={12}><LoadingSpinner /></td></tr>) :
														this.state.couponList.length > 0 ?
															(this.state.couponList.map(coupon => (
																<tr key={coupon.id}>
																	<td>{coupon.coupon}</td>
																	<td>{coupon.amount === null ? 'N/A' : coupon.amount}</td>
																	<td>{coupon.amount === null ? 'N/A' : coupon.amount_type}</td>
																	<td><Moment format="MMM Do, YYYY HH:mm:ss">{coupon.start_date}</Moment></td>
																	<td><Moment format="MMM Do, YYYY HH:mm:ss">{coupon.end_date}</Moment></td>
																	<td className="text-center">
																		{coupon.assignuser !== null &&
																			coupon.assignuser.is_redeem === 'Yes' || moment(coupon.end_date).toDate() < new Date()
																			? <i className="fa fa-circle text-danger" aria-hidden="true" title="Inactive"></i>
																			: coupon.active_status === 1
																				? <i className="fa fa-circle text-success" aria-hidden="true" title="Active" onClick={() => this.handelStatusModal(coupon.id, '0')}></i>
																				: <i className="fa fa-circle text-danger" aria-hidden="true" title="Inactive" onClick={() => this.handelStatusModal(coupon.id, '1')}></i>
																		}
																		{coupon.assignuser === null ?
																			<i className="fa fa-tasks text-danger" aria-hidden="true" title="Coupon not assigned"></i>
																			: <i className="fa fa-tasks text-success" aria-hidden="true" title="Coupon assigned"></i>
																		}
																		{coupon.assignuser !== null ?
																			coupon.assignuser.is_redeem === 'No' ?
																				<i className="fa fa-tags text-danger" aria-hidden="true" title="Coupon not redeem"></i> :
																				<i className="fa fa-tags text-success" aria-hidden="true" title="Coupon redeem"></i> :
																			<i className="fa fa-tags text-danger" aria-hidden="true" title="Coupon not redeem"></i>
																		}
																	</td>
																	<td className="text-center actionTd">
																		{coupon.assignuser && coupon.assignuser.is_redeem === 'Yes' ?
																			<React.Fragment>
																				<i className="fa fa-tags" aria-hidden="true" title="View Coupon Details" onClick={() => this.handleCouponDetail(coupon.coupon)}></i>
                                                                				&nbsp;
                                                                				<i className="fa fa-trash-o" aria-hidden="true" title="Delete" onClick={() => this.handelDeleteModal(coupon.id)}></i>
																			</React.Fragment> :
																			<React.Fragment>
																				{moment(coupon.end_date).toDate() < new Date() ? '' :
																					coupon.assignuser &&
																					<i className="fa fa-gift" aria-hidden="true" title="Redeem Coupon" onClick={() => this.handleRedeemCoupon(coupon)}></i>
																				}
                                                                				&nbsp;
                                                                				{moment(coupon.end_date).toDate() < new Date() ? '' :
																					<i className="fa fa-tasks" aria-hidden="true" title="Assign Coupon" onClick={() => this.handelAssignModal(coupon)}></i>
																				}
																				&nbsp;
																				<i className="fa fa-tags" aria-hidden="true" title="View Coupon Details" onClick={() => this.handleCouponDetail(coupon.coupon)}></i>
																				&nbsp;
																				{moment(coupon.end_date).toDate() < new Date() ? '' :
																					<i className="fa fa-eye" aria-hidden="true" title="View" onClick={() => this.handelviewEditModal(coupon)}></i>
																				}
																				&nbsp;
                                                                				<i className="fa fa-trash-o" aria-hidden="true" title="Delete" onClick={() => this.handelDeleteModal(coupon.id)}></i>
																			</React.Fragment>
																		}
																	</td>
																</tr>
															)))
															: this.state.fetchErrorMsg ? null : (<tr>
																<td colSpan={12}>
																	<p className="text-center">No records found</p>
																</td>
															</tr>)
													}
												</tbody>
											</Table>
										</div>
									</Col>
								</Row>
								{this.state.totalCount ? (
									<Row className="paginationPan">
										<Col md={6} sm={12} className="paginationSelect">
											<span>Items per page</span>
											<select
												id={this.state.itemPerPage}
												className="form-control truncatefloat-left w-90"
												onChange={this.handleChangeItemPerPage}
												value={this.state.itemPerPage}>
												<option value='10'>10</option>
												<option value='50'>50</option>
												<option value='100'>100</option>
												<option value='150'>150</option>
												<option value='200'>200</option>
												<option value='250'>250</option>
											</select>
										</Col>
										<Col md={6} sm={12} className="text-right">
											<div className="">
												<Pagination
													activePage={this.state.activePage}
													itemsCountPerPage={this.state.itemPerPage}
													totalItemsCount={this.state.totalCount}
													onChange={this.handlePageChange}
												/>
											</div>
										</Col>
									</Row>
								) : null}

								{/*========================= Modal for Status change =====================*/}
								<Modal
									size="xl"
									show={this.state.statusChange}
									onHide={this.handleHide}
									dialogClassName="modal-90w"
									aria-labelledby="example-custom-modal-styling-title"
								>
									<Modal.Body>
										<div className="m-auto text-center">
											<h6 className="mb-3 text-dark">Do you want to change this status?</h6>
										</div>
										{this.state.statuserrorMsg ? <div className="alert alert-danger my-3 text-center col-12" role="alert">{this.state.statuserrorMsg}</div> : null}
										<div className="m-auto text-center">
											<button className="btn mr-3 small" onClick={() => this.handleHide()}>Return</button>
											{this.state.statuserrorMsg == null ?
												<button className="btn danger small" onClick={() => this.handleStatus(this.state.couponId, this.state.status)}>Confirm</button> : null}
										</div>

									</Modal.Body>
								</Modal>

								{/*====== Status change confirmation popup  ===== */}
								<Modal
									show={this.state.statusConfirMmsg}
									onHide={this.handleStatusChangedClose}
									className="payOptionPop"
								>
									<Modal.Body className="text-center">
										<Row>
											<Col md={12} className="text-center">
												<Image src={SuccessIco} />
											</Col>
										</Row>
										<Row>
											<Col md={12} className="text-center">
												<h5>{this.state.successMessage}</h5>
											</Col>
										</Row>
										<Button
											onClick={this.handleStatusChangedClose}
											className=""
										>
											Return
                                    </Button>
									</Modal.Body>
								</Modal>

								{/* Add Coupon Modal */}
								<Modal show={this.state.addModal}
									onHide={this.handelAddModalClose}
									size="lg"
								>
									<Modal.Header closeButton>
										<div className="modal-title">Add Coupon</div>
									</Modal.Header>
									<Modal.Body className="couponDetails">
										{this.state.addErrorMessge ? (
											<div className="alert alert-danger" role="alert">
												{this.state.addErrorMessge}
											</div>
										) : null}
										<Row>
											<Col sm={12}>
												{this.state.addCouponLoader ? <LoadingSpinner /> : null}
											</Col>
										</Row>
										<Row className="show-grid">
											<Col xs={12} className="brd-right">
												<Formik
													initialValues={initialValues}
													validationSchema={addCouponSchema}
													onSubmit={this.handleSubmit}
												>
													{({
														values,
														errors,
														touched,
														isSubmitting,
													}) => {
														return (
															<Form>
																<Row className="show-grid">
																	<Col xs={12} md={12}>
																		<FormGroup controlId="formControlsTextarea">
																			<label>Coupon Type<span className="required">*</span></label>
																			<Field
																				name="coupon_type"
																				component="select"
																				className={'form-control'}
																				autoComplete="nope"
																				placeholder="select"
																			>
																				<option value="" defaultValue="">Select Coupon Type</option>
																				<option value="0" key="0">Discount</option>
																				<option value="1" key="1">Complimentary Reward</option>
																			</Field>
																			{errors.coupon_type && touched.coupon_type ? (
																				<span className="errorMsg">{errors.coupon_type}</span>
																			) : null}
																			<FormControl.Feedback />
																		</FormGroup>
																	</Col>
																	{values.coupon_type === '0' || values.coupon_type === '' ?
																		<Col xs={12} md={12}>
																			<FormGroup controlId="formControlsTextarea">
																				<label>Amount ($)<span className="required">*</span></label>
																				<Field
																					name="amount"
																					type="number"
																					className={'form-control'}
																					autoComplete="nope"
																					placeholder="Enter Amount"
																					value={values.amount || ''}
																				/>
																				{errors.amount && touched.amount ? (
																					<span className="errorMsg">{errors.amount}</span>
																				) : null}
																				<FormControl.Feedback />
																			</FormGroup>
																		</Col> : ''}
																	{values.coupon_type === '0' || values.coupon_type === '' ?
																		<Col xs={12} md={12}>
																			<FormGroup controlId="formControlsTextarea">
																				<label>Amount Type<span className="required">*</span></label>
																				<Field
																					name="amount_type"
																					component="select"
																					className={'form-control'}
																					autoComplete="nope"
																					placeholder="select"
																				>
																					<option value="" defaultValue="">Select Amount Type</option>
																					<option value="Flat" key="0">Flat</option>
																					<option value="Percent" key="1">Percent</option>
																				</Field>
																				{errors.amount_type && touched.amount_type ? (
																					<span className="errorMsg">{errors.amount_type}</span>
																				) : null}
																				<FormControl.Feedback />
																			</FormGroup>
																		</Col> : ''}

																	<Col xs={6} md={6}>
																		<FormGroup controlId="formControlsTextarea">
																			<label>Start Date<span className="required">*</span></label>
																			<DatePicker
																				selected={this.state.startDateAddCoupon}
																				onChange={this.handleChangeStartDateAddCoupon}
																				className='form-control'
																				minDate={new Date()}
																				required
																				timeInputLabel="Time:"
																				dateFormat="dd-MM-yyyy HH:mm:ss"
																				showTimeInput
																				showMonthDropdown
																				showYearDropdown
																			/>
																			{errors.start_date && touched.start_date ? (
																				<span className="errorMsg">{errors.start_date}</span>
																			) : null}
																			<FormControl.Feedback />
																		</FormGroup>
																	</Col>

																	<Col xs={6} md={6}>
																		<FormGroup controlId="formControlsTextarea">
																			<label>End Date<span className="required">*</span></label>
																			<DatePicker
																				selected={this.state.endDateAddCoupon}
																				onChange={this.handleChangeEndDateAddCoupon}
																				className='form-control'
																				minDate={this.state.startDateAddCoupon}
																				minTime={this.state.startDateAddCoupon}
																				required
																				timeInputLabel="Time:"
																				dateFormat="dd-MM-yyyy HH:mm:ss"
																				showTimeInput
																				showMonthDropdown
																				showYearDropdown
																			/>
																			{errors.end_date && touched.end_date ? (
																				<span className="errorMsg">{errors.end_date}</span>
																			) : null}
																			<FormControl.Feedback />
																		</FormGroup>
																	</Col>

																	<Col xs={12} md={12}>
																		<FormGroup controlId="formControlsTextarea">
																			<label>Description<span className="required">*</span></label>
																			<Field
																				name="description"
																				type="text"
																				className={'form-control'}
																				autoComplete="nope"
																				placeholder="Enter Description"
																				value={values.description || ''}
																			/>
																			{errors.description && touched.description ? (
																				<span className="errorMsg">{errors.description}</span>
																			) : null}
																			<FormControl.Feedback />
																		</FormGroup>
																	</Col>

																	<Col xs={12} md={6}>
																		<FormGroup controlId="formControlsTextarea">
																			<label>Status <span className="required">*</span></label>
																			<Field
																				name="active_status"
																				component="select"
																				className={'form-control'}
																				autoComplete="nope"
																				placeholder="select"
																			>
																				<option value="" defaultValue="">Select Status</option>
																				<option value="1" key="1">Active</option>
																				<option value="0" key="0">Inactive</option>
																			</Field>
																			{errors.active_status && touched.active_status ? (
																				<span className="errorMsg">{errors.active_status}</span>
																			) : null}
																			<FormControl.Feedback />
																		</FormGroup>
																	</Col>
																</Row>
																<Row className="show-grid">
																	<Col xs={12} md={12}>&nbsp;</Col>
																</Row>
																<Row className="show-grid">
																	<Col xs={12} md={12} className="text-center">
																		<Button className="" type="submit" disabled={isSubmitting}>Save</Button>
																	</Col>
																</Row>
																<Row>
																	<Col md={12}>
																		<p style={{ paddingTop: '10px' }}><span className="required">*</span> These fields are required.</p>
																	</Col>
																</Row>
															</Form>
														);
													}}
												</Formik>
											</Col>
										</Row>
									</Modal.Body>
								</Modal>

								{/* View Coupon Details */}
								<Modal size="lg" show={this.state.viewCouponDetails} onHide={this.handleCouponDetailClose}>
									<Modal.Header closeButton>
										<div className="modal-title">Coupon Details</div>
									</Modal.Header>
									<Modal.Body className="couponDetails">
										<Row>
											<Col sm={12}>
												{this.state.loadingForCouponDetail ? <LoadingSpinner /> : null}
											</Col>
										</Row>
										{couponDetails &&
											<table responsive>
												<tr>
													<td width="30%"><b>Coupon Code</b></td>
													<td width="10%">:</td>
													<td width="60%">{couponDetails.details.coupon}</td>
												</tr>
												<tr>
													<td width="30%"><b>Coupon Type</b></td>
													<td width="10%">:</td>
													<td width="60%">{couponDetails.details.coupon_type === '0' ? 'Discount' : 'Complimentary Reward'}</td>
												</tr>
												<tr>
													<td width="30%"><b>Description</b></td>
													<td width="10%">:</td>
													<td width="60%">{couponDetails.details.description}</td>
												</tr>
												{couponDetails.details.coupon_type === '0' &&
													<React.Fragment>
														<tr>
															<td width="30%"><b>Amount ($)</b></td>
															<td width="10%">:</td>
															<td width="60%">{couponDetails.details.amount}</td>
														</tr>
														<tr>
															<td width="30%"><b>Amount Type</b></td>
															<td width="10%">:</td>
															<td width="60%">{couponDetails.details.amount_type}</td>
														</tr>
													</React.Fragment>
												}
												<tr>
													<td width="30%"><b>Start Date</b></td>
													<td width="10%">:</td>
													<td width="60%"><Moment format="MMMM Do, YYYY HH:mm:ss">{couponDetails.details.start_date}</Moment></td>
												</tr>
												<tr>
													<td width="30%"><b>End Date</b></td>
													<td width="10%">:</td>
													<td width="60%"><Moment format="MMMM Do, YYYY HH:mm:ss">{couponDetails.details.end_date}</Moment></td>
												</tr>
												{couponDetails.details.assignuser &&
													<React.Fragment>
														<tr>
															<td width="30%"><b>Assigned User Name</b></td>
															<td width="10%">:</td>
															<td width="60%">{couponDetails.details.assignuser.first_name}&nbsp;{couponDetails.details.assignuser.last_name}</td>
														</tr>
														<tr>
															<td width="30%"><b>Email</b></td>
															<td width="10%">:</td>
															<td width="60%">{couponDetails.details.assignuser_email.email}</td>
														</tr>
														<tr>
															<td width="30%"><b>Assigned Date</b></td>
															<td width="10%">:</td>
															<td width="60%"><Moment format="MMMM Do, YYYY HH:mm:ss">{couponDetails.details.assign_info[0].created_at}</Moment></td>
														</tr>
														{couponDetails.details.assignuser.redeem_date && <tr>
															<td width="30%"><b>Redeem Date</b></td>
															<td width="10%">:</td>
															<td width="60%"><Moment format="MMMM Do, YYYY HH:mm:ss">{couponDetails.details.assignuser.redeem_date}</Moment></td>
														</tr>}
													</React.Fragment>
												}
												<tr>
													<td width="30%"><b>Status</b></td>
													<td width="10%">:</td>
													<td width="60%">{couponDetails.details.active_status === 0 ? 'Inactive' : 'Active'}</td>
												</tr>
												<tr>
													<td width="30%"><b>Created Date</b></td>
													<td width="10%">:</td>
													<td width="60%"><Moment format="MMMM Do, YYYY HH:mm:ss">{couponDetails.details.created_at}</Moment></td>
												</tr>
												<tr>
													<td width="30%"><b>Updated Date</b></td>
													<td width="10%">:</td>
													<td width="60%"><Moment format="MMMM Do, YYYY HH:mm:ss">{couponDetails.details.updated_at}</Moment></td>
												</tr>
												<tr>
													<td width="30%"><b>Created By</b></td>
													<td width="10%">:</td>
													<td width="60%">{couponDetails.details.creator.first_name}&nbsp;{couponDetails.details.creator.last_name}</td>
												</tr>
												<tr>
													<td width="30%"><b>Creater Email</b></td>
													<td width="10%">:</td>
													<td width="60%">{couponDetails.details.creator_email.email}</td>
												</tr>
											</table>
										}
									</Modal.Body>
								</Modal>

								{/*========================= Modal for Delete Coupon =====================*/}
								<Modal
									show={this.state.deleteCoupon}
									onHide={this.handleHide}
									size="md"
								>
									<Modal.Body>
										<div className="m-auto text-center">
											<h6 className="confirmTxt">Do you want to delete this Coupon?</h6>
										</div>
										{this.state.deleteerrorMsg ? <div className="alert alert-danger my-3 text-center col-12" role="alert">{this.state.deleteerrorMsg}</div> : null}
										<div className="m-auto text-center">
											<button className="btn mr-3 small" onClick={() => this.handleDeleteHide()}>Return</button>
											{this.state.deleteerrorMsg == null ? <button className="btn small danger" onClick={() => this.handleDelete(this.state.couponId)}>Confirm</button> : null}
										</div>
									</Modal.Body>
								</Modal>

								{/* Add Assign Coupon Modal */}
								<Modal show={this.state.viewAssignModel}
									onHide={this.handelAssignModalClose}
									size="lg"
								>
									<Modal.Header closeButton><div className="modal-title">Assign Coupon</div></Modal.Header>
									<Modal.Body className="providerDetails">
										{this.state.addErrorMessge ? (
											<div className="alert alert-danger" role="alert">
												{this.state.addErrorMessge}
											</div>
										) : null}
										<Row>
											<Col sm={12}>
												{this.state.addAssignCouponLoader ? <LoadingSpinner /> : null}
											</Col>
										</Row>
										<Row className="show-grid">
											<Col xs={12} className="brd-right">
												<Formik
													onSubmit={this.handleAssignCouponSubmit}
												>
													{({
														values,
													}) => {
														return (
															<Form>
																<Row className="show-grid">
																	<Col xs={12} md={12}>
																		<FormGroup controlId="formControlsTextarea">
																			<label>Coupon<span className="required">*</span></label>
																			<Field
																				name="coupon"
																				type="text"
																				className={'form-control'}
																				autoComplete="nope"
																				placeholder=""
																				value={this.state.coupon ? this.state.coupon : values.coupon}
																				readOnly
																			/>
																			<FormControl.Feedback />
																		</FormGroup>
																	</Col>
																	<Col xs={12} md={12}>
																		<FormGroup controlId="formControlsTextarea">
																			<label>{this.state.assignUser ? 'Assigned User' : 'Select User'}<span className="required">*</span></label>
																			{this.state.assignUser ?
																				<Field
																					name="coupon"
																					type="text"
																					className={'form-control'}
																					autoComplete="nope"
																					placeholder=""
																					value={this.state.assignUser.email}
																					readOnly
																				/> :
																				<Select
																					className=""
																					classNamePrefix="select"
																					isClearable={true}
																					isSearchable={true}
																					name="user_id"
																					options={options}
																					styles={colourStyles}
																					onChange={this.handleChange}
																					theme={
																						theme => ({
																							...theme,
																							borderRadius: 0,
																							colors: {
																								...theme.colors,
																								primary25: '#0186cf',
																								primary: 'green',
																							},
																						})}
																				/>}
																			<FormControl.Feedback />
																		</FormGroup>
																	</Col>
																</Row>
																<Row className="show-grid">
																	<Col xs={12} md={12}>
																		&nbsp;
                                                                        </Col>
																</Row>
																<Row className="show-grid text-center">
																	<Col xs={12} md={12}>
																		<Fragment>
																			{this.state.assignUser ?
																				this.state.assignuserdetails.is_redeem === 'Yes' ? <Button
																					onClick={this.handelAssignModalClose}
																					className="but-gray border-0 mr-2">
																					Back
                                                                                    </Button> : <Fragment>
																						{/* <Button className="blue-btn border-0" onClick={this.handleeditCouponEnable}>
                                                                                            Edit
                                                                                        </Button> */}
																						<Button className="but-gray border-0 mr-2" onClick={this.handelAssignModalClose}>
																							Back
                                                                                        </Button>
																					</Fragment> : <Fragment>
																					<Button
																						onClick={this.handelAssignModalClose}
																						className="but-gray border-0 mr-2">
																						Cancel
                                                                                    </Button>
																					<Button type="submit" className="blue-btn ml-2 border-0">
																						Save
                                                                                    </Button>
																				</Fragment>
																			}
																		</Fragment>
																	</Col>
																</Row>
																<Row>
																	<Col md={12}>
																		<p style={{ paddingTop: '10px' }}><span className="required">*</span> These fields are required.</p>
																	</Col>
																</Row>
															</Form>
														);
													}}
												</Formik>
											</Col>
										</Row>
									</Modal.Body>
								</Modal>

								{/* Redeem Coupon Modal */}
								<Modal show={this.state.viewRedeemCouponModel}
									onHide={this.handelRedeemCouponModalClose}
									size="lg"
								>
									<Modal.Header closeButton><div className="modal-title">Redeem Coupon</div></Modal.Header>
									<Modal.Body className="couponDetails">
										{this.state.addErrorMessge ? (
											<div className="alert alert-danger" role="alert">
												{this.state.addErrorMessge}
											</div>
										) : null}
										<Row>
											<Col sm={12}>
												{this.state.addRedeemCouponLoader ? <LoadingSpinner /> : null}
											</Col>
										</Row>
										<Row className="show-grid">
											<Col xs={12} className="brd-right">
												<Formik
													onSubmit={this.handleRedeemCouponSubmit}
												>
													{({
														values,
													}) => {
														return (
															<Form>
																<Row className="show-grid">
																	<Col xs={12} md={12}>
																		<FormGroup controlId="formControlsTextarea">
																			<label>Coupon<span className="required">*</span></label>
																			<Field
																				name="coupon"
																				type="text"
																				className={'form-control'}
																				autoComplete="nope"
																				placeholder=""
																				value={this.state.coupon ? this.state.coupon : values.coupon}
																				readOnly
																			/>
																			<FormControl.Feedback />
																		</FormGroup>
																	</Col>
																</Row>
																<Row className="show-grid text-center">
																	<Col xs={12} md={12}>
																		<Fragment>
																			{this.state.assignuserdetails &&
																				this.state.assignuserdetails.is_redeem === 'No' ?
																				<Fragment>
																					<Button
																						onClick={this.handelRedeemCouponModalClose}
																						className="btn danger small mr-2">
																						Cancel
                                                                                    </Button>
																					<Button type="submit" className="ml-2 btn small">
																						Redeem
                                                                                    </Button>
																				</Fragment> :
																				<Button
																					onClick={this.handelRedeemCouponModalClose}
																					className="btn small mr-2">
																					Back
                                                                                </Button>
																			}
																		</Fragment>
																	</Col>
																</Row>
																<Row>
																	<Col md={12}>
																		<p style={{ paddingTop: '10px' }}><span className="required">*</span> {this.state.assignuserdetails &&
																			this.state.assignuserdetails.is_redeem === 'No' ? 'These fields are required.' : 'This coupon is already redeem.'}</p>
																	</Col>
																</Row>
															</Form>
														);
													}}
												</Formik>
											</Col>
										</Row>
									</Modal.Body>
								</Modal>

								{/* Edit Coupon Modal */}
								<Modal
									show={this.state.viewEditModal}
									onHide={this.handelEditModalClose}
									size="lg"
								>
									<Modal.Header closeButton>
										<div className="modal-title">{this.state.editCouponEnable !== true ? 'View' : 'Edit'} Coupon</div>
									</Modal.Header>
									<Modal.Body className="couponDetails">
										<Row>
											<Col sm={12}>
												{this.state.editCouponLoader ? <LoadingSpinner /> : null}
											</Col>
										</Row>
										<Row className="show-grid">
											<Col xs={12} className="brd-right">
												<Formik
													initialValues={values}
													validationSchema={editCouponSchema}
													onSubmit={this.handleSubmitForEdit}
													enableReinitialize={true} >
													{({
														values,
														errors,
														touched,
														isSubmitting,
													}) => {
														return (
															<Form className={disabled === false ? ('hideRequired') : null}>
																<Row className="show-grid">
																	<Col xs={12} md={12}>
																		<FormGroup controlId="formControlsTextarea">
																			<label>Coupon Type<span className="required">*</span></label>
																			<Field
																				name="coupon_type"
																				component="select"
																				className={'form-control'}
																				autoComplete="nope"
																				placeholder="select"
																				disabled={disabled === false ? 'disabled' : ''}
																			>
																				<option value="" defaultValue="">Select Coupon Type</option>
																				<option value="0" key="0">Discount</option>
																				<option value="1" key="1">Complimentary Reward</option>
																			</Field>
																			{errors.coupon_type && touched.coupon_type ? (
																				<span className="errorMsg">{errors.coupon_type}</span>
																			) : null}
																			<FormControl.Feedback />
																		</FormGroup>
																	</Col>
																	{values.coupon_type === '0' || values.coupon_type === '' ?
																		<Col xs={12} md={12}>
																			<FormGroup controlId="formControlsTextarea">
																				<label>Amount ($)<span className="required">*</span></label>
																				<Field
																					name="amount"
																					type="number"
																					className={'form-control'}
																					autoComplete="nope"
																					placeholder="Enter Amount"
																					value={values.amount || ''}
																					disabled={disabled === false ? 'disabled' : ''}
																				/>
																				{errors.amount && touched.amount ? (
																					<span className="errorMsg">{errors.amount}</span>
																				) : null}
																				<FormControl.Feedback />
																			</FormGroup>
																		</Col> : ''}

																	{values.coupon_type === '0' || values.coupon_type === '' ?
																		<Col xs={12} md={12}>
																			<FormGroup controlId="formControlsTextarea">
																				<label>Amount Type<span className="required">*</span></label>
																				<Field
																					name="amount_type"
																					component="select"
																					className={'form-control'}
																					autoComplete="nope"
																					placeholder="select"
																					disabled={disabled === false ? 'disabled' : ''}
																				>
																					<option value="" defaultValue="">Select Amount Type</option>
																					<option value="Flat" key="0">Flat</option>
																					<option value="Percent" key="1">Percent</option>
																				</Field>
																				{errors.amount_type && touched.amount_type ? (
																					<span className="errorMsg">{errors.amount_type}</span>
																				) : null}
																				<FormControl.Feedback />
																			</FormGroup>
																		</Col> : ''}

																	<Col xs={6} md={6}>
																		<FormGroup controlId="formControlsTextarea">
																			<label>Start Date<span className="required">*</span></label>
																			<DatePicker
																				selected={this.state.startDate}
																				onChange={this.handleChangeStartDateForEdit}
																				className='form-control'
																				minDate={new Date()}
																				required
																				timeInputLabel="Time:"
																				dateFormat="dd-MM-yyyy HH:mm:ss"
																				showTimeInput
																				showMonthDropdown
																				showYearDropdown
																				disabled={disabled === false ? 'disabled' : ''}
																			/>
																			<FormControl.Feedback />
																		</FormGroup>
																	</Col>

																	<Col xs={6} md={6}>
																		<FormGroup controlId="formControlsTextarea">
																			<label>End Date<span className="required">*</span></label>
																			<DatePicker
																				selected={this.state.endDate}
																				onChange={this.handleChangeEndDateForEdit}
																				className='form-control'
																				minDate={this.state.startDate}
																				required
																				timeInputLabel="Time:"
																				dateFormat="dd-MM-yyyy HH:mm:ss"
																				showTimeInput
																				showMonthDropdown
																				showYearDropdown
																				disabled={disabled === false ? 'disabled' : ''}
																			/>
																			<FormControl.Feedback />
																		</FormGroup>
																	</Col>

																	<Col xs={12} md={12}>
																		<FormGroup controlId="formControlsTextarea">
																			<label>Description<span className="required">*</span></label>
																			<Field
																				name="description"
																				type="text"
																				className={'form-control'}
																				autoComplete="nope"
																				placeholder="Enter Description"
																				value={values.description || ''}
																				disabled={disabled === false ? 'disabled' : ''}
																			/>
																			{errors.description && touched.description ? (
																				<span className="errorMsg">{errors.description}</span>
																			) : null}
																			<FormControl.Feedback />
																		</FormGroup>
																	</Col>

																	<Col xs={12} md={6}>
																		<FormGroup controlId="formControlsTextarea">
																			<label>Status <span className="required">*</span></label>
																			<Field
																				name="active_status"
																				component="select"
																				className={'form-control'}
																				autoComplete="nope"
																				placeholder="select"
																				disabled={disabled === false ? 'disabled' : ''}
																			>
																				<option value="">Select Status</option>
																				<option value="1" key="1">Active </option>
																				<option value="0" key="0">Inactive </option>
																			</Field>
																			{errors.active_status && touched.active_status ? (
																				<span className="errorMsg">{errors.active_status}</span>
																			) : null}
																			<FormControl.Feedback />
																		</FormGroup>
																	</Col>
																</Row>

																<Row className="show-grid">
																	<Col xs={12} md={12}>
																		&nbsp;
                                                                    </Col>
																</Row>
																<Row>&nbsp;</Row>
																<Row className="show-grid text-center">
																	<Col xs={12} md={12}>
																		<Fragment>
																			{this.state.editCouponEnable !== true ? (
																				<Fragment>
																					<Button className="blue-btn border-0" onClick={this.handleeditCoupon}>
																						Edit
                                                                                    </Button>
																				</Fragment>) :
																				(<Fragment>
																					<Button
																						onClick={this.handelEditModalClose}
																						className="but-gray border-0 mr-2">
																						Cancel
                                                                                    </Button>
																					<Button type="submit" className="blue-btn ml-2 border-0" disabled={isSubmitting}>
																						Save
                                                                                    </Button>
																				</Fragment>)
																			}
																		</Fragment>
																	</Col>
																</Row>
																{disabled === false ? null : (<Fragment>
																	<Row>
																		<Col md={12}>
																			<p style={{ paddingTop: '10px' }}><span className="required">*</span> These fields are required.</p>
																		</Col>
																	</Row>
																</Fragment>)}
															</Form>
														);
													}}
												</Formik>
											</Col>
										</Row>
									</Modal.Body>
								</Modal>

							</div>
						</div>
					</div>
				</div>
			</div >
		);
	}
}

export default Coupon;