import React, { Component } from 'react';
import * as Yup from 'yup';
import axios from '../../shared/eaxios';
import * as AppConst from '../../common/constants';
import { Button, Col, FormControl, FormGroup, Modal, Row } from 'react-bootstrap';
import { Field, Form, Formik } from 'formik';
import LoadingSpinner from '../../common/LoadingSpinner/LoadingSpinner';
import PropTypes from 'prop-types';
import iconGems from '../../assets/images/icon-gems.png';
import iconGemsOther from '../../assets/images/gems-loader.gif';
import { Link } from 'react-router-dom';
import { Map, GoogleApiWrapper, InfoWindow, Marker } from 'google-maps-react';
import GooglePlacesAutocomplete from 'react-google-places-autocomplete';
import { geocodeByAddress, getLatLng } from 'react-google-places-autocomplete';
import Geocode from "react-geocode";
import { fetchGemsList, fetchCountryList } from '../../action/cmsAction';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import valid from 'card-validator';

const initialValues = {
	first_name: '',
	last_name: '',
	phone: '',
	email: '',
	address: '',
	address2: '',
	personalCountry: '',
	personalState: '',
	personalCity: '',
	personalZip: '',
	company_name: '',
	company_phone: '',
	//company_email: '',
	addr1: '',
	addr2: '',
	companyCountry: '',
	companyState: '',
	companyCity: '',
	companyZip: '',
	prefer_purchase: true,
	selectedFile: null,
	card_number: '',
	name_on_card: '',
	expiry_date: '',
	cvc: ''
};
const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/

const FILE_SIZE = 160 * 1024;
const SUPPORTED_FORMATS = [
	"image/jpg",
	"image/jpeg",
	"image/png",
	"application/pdf",
];

// export function checkIfFilesAreTooBig(files?: [File]): boolean {
// 	let valid = true
// 	if (files) {
// 		files.map(file => {
// 			const size = file.size / 1024 / 1024
// 			if (size > 10) {
// 				valid = false
// 			}
// 		})
// 	}
// 	return valid
// }

// export function checkIfFilesAreCorrectType(files?: [File]): boolean {
// 	let valid = true
// 	if (files) {
// 		files.map(file => {
// 			if (!['application/pdf', 'image/jpeg', 'image/png'].includes(file.type)) {
// 				valid = false
// 			}
// 		})
// 	}
// 	return valid
// }

const addProviderSchema = Yup.object().shape({
	first_name: Yup.string()
		.trim('Please remove whitespace')
		.strict()
		.required('Please enter first name')
		.max(40, 'maximum characters length 40'),
	last_name: Yup.string()
		.trim('Please remove whitespace')
		.strict()
		.required('Please enter last name')
		.max(40, 'maximum characters length 40'),
	phone: Yup.string()
		.required('Please enter phone number')
		.max(10, 'Phone number must be 10 digit')
		.min(10, 'Phone number must be 10 digit')
		.matches(phoneRegExp, 'Phone number is not valid'),
	email: Yup.string()
		.email('Email must be valid')
		.trim('Please remove whitespace')
		.strict()
		.required('Please enter company email')
		.max(40, 'maximum characters length 40'),
	address: Yup.string()
		.trim('Please remove whitespace')
		.required('Please enter address')
		.strict()
		.max(40, 'maximum characters length 40'),
	address2: Yup.string()
		.trim('Please remove whitespace')
		.strict()
		.max(40, 'maximum characters length 40'),
	personalCountry: Yup.string().required('Please select country'),
	personalZip: Yup.string()
		.required('Please enter zip'),
	company_name: Yup.string()
		.trim('Please remove whitespace')
		.strict()
		.required('Please enter company name')
		.max(40, 'maximum characters length 40'),
	company_phone: Yup.string()
		.required('Please enter company phone number')
		.max(10, 'Phone number must be 10 digit')
		.min(10, 'Phone number must be 10 digit')
		.matches(phoneRegExp, 'Phone number is not valid'),
	addr1: Yup.string()
		.trim('Please remove whitespace')
		.required('Please enter company address')
		.strict()
		.max(40, 'maximum characters length 40'),
	addr2: Yup.string()
		.trim('Please remove whitespace')
		.strict()
		.max(40, 'maximum characters length 40'),
	companyCountry: Yup.string().required('Please select company country'),
	companyZip: Yup.string()
		.required('Please enter zip'),


	/* company_doc: Yup.array()
		  .nullable()
		  .required('VALIDATION_FIELD_REQUIRED')
		  .test('is-correct-file', 'VALIDATION_FIELD_FILE_BIG', checkIfFilesAreTooBig)
		  .test(
			'is-big-file',
			'VALIDATION_FIELD_FILE_WRONG_TYPE',
			checkIfFilesAreCorrectType
		  ),*/
	prefer_purchase: Yup.boolean(),
	card_number: Yup.string()
		.when('prefer_purchase', {
			is: true,
			then: Yup.string()
				.test('test-number', 'Credit/Debit Card number is invalid', value => valid.number(value).isValid)
				.required('Credit/Debit number is required')
		}),
	name_on_card: Yup.string()
		.when('prefer_purchase', {
			is: true,
			then: Yup.string().required('Name on card is required')
		}),
	expiry_date: Yup.string()
		.when('prefer_purchase', {
			is: true,
			then: Yup.string()
				.test('test-expiry_date', 'Expiration date is invalid', value => valid.expirationDate(value).isValid)
				.matches(/([0-9]{2})\/([0-9]{2})/, 'Not a valid expiration date. Example: MM/YY')
				.required('Expiration date is required')
		}),
	cvc: Yup.string()
		.when('prefer_purchase', {
			is: true,
			then: Yup.string()
				.test('test-cvv-number', 'CVV is invalid', value => valid.cvv(value).isValid)
				.required('cvv is required')
		}),
});

const assignProviderSchema = Yup.object().shape({});


class ClaimYourGemSearchResult extends Component {

	constructor(props) {
		super(props);
		this.searchScrollRef = React.createRef();
		this.state = {
			checked: true,
			showConfirMmsg: false,
			showHide: false,
			showHideLogin: false,
			latitude: '',
			longitude: '',
			address: '',
			showingInfoWindow: false,
			activeMarker: {},
			selectedPlace: {},
			gemsList: [],
			gemDetail: '',
			gems_name: '',
			countryList: [],
			personalStateList: [],
			personalCityList: [],
			companyStateList: [],
			companyCityList: [],
			showAlert: false,
			showLoadingSpinner: false,
			searchKey: '',
			activePage: 1,
			totalCount: 0,
			pageSize: 10,
			page: 1,
			field: null,
			current_page: 0,
			last_page: 0,
			showLoadingSpinnerForSearch: false,
			showFirst: false,
			from: 0,
			to: 0,
			preventScrolling: false,
			showFromSearchOnMap: false,
			longitudeForSearchAndScroll: '',
			latitudeForSearchAndScroll: '',
			addressForSearchAndScroll: '',
			gemNameForSearchAndScroll: '',
			name: '',
			latitudeForSearchResultClick: '',
			longitudeForSearchResultClick: ''
		}
	}


	fetchGems = (gems_name, latitude, longitude, pageSize, page) => {
		fetchGemsList(gems_name, latitude, longitude, pageSize, page).then(response => {
			const gemsList = response.data.data;
			if (gemsList.length === 0) {
				this.props.history.push(AppConst.SERVERFOLDER + "/claim-your-gem-no-result");
				localStorage.setItem('gems_name', gems_name);
				localStorage.setItem('latitude', latitude);
				localStorage.setItem('longitude', longitude);
				//console.log(this.state.address !== '');
				if (this.state.address !== null || this.state.address !== '')
					localStorage.setItem('address', this.state.address);
			}
			this.setState({
				gemsList: gemsList,
				gemDetail: gemsList[0],
				totalCount: response.data.total,
				current_page: response.data.current_page,
				last_page: response.data.last_page,
				to: response.data.to,
				from: response.data.from,
				showLoadingSpinnerForSearch: false,
				preventScrolling: false,
				showFromSearchOnMap: false
			});
		});
	}

	handleChangeGemsName = (event) => {
		this.setState({ gems_name: event.target.value });
	}

	handleModalShowHide() {
		localStorage.getItem('token') ?
			this.setState({ showHideLogin: !this.state.showHideLogin }) :
			this.setState({ showHide: !this.state.showHide })
	}

	countries = () => {
		fetchCountryList().then(response => {
			const countryList = response.data.data.map(data => {
				return data;
			});
			this.setState({ countryList: countryList });
		});
	}

	/******* Provider Registartion Start *********/

	componentDidMount() {
		window.scrollTo(0, 0)
		this.setState({
			gems_name: localStorage.getItem('gems_name'),
			latitude: localStorage.getItem('latitude'),
			longitude: localStorage.getItem('longitude'),
			address: localStorage.getItem('address'),
			latitudeForSearchAndScroll: localStorage.getItem('latitude'),
			longitudeForSearchAndScroll: localStorage.getItem('longitude'),
			gemNameForSearchAndScroll: localStorage.getItem('gems_name'),
			addressForSearchAndScroll: localStorage.getItem('address')
		});
		this.fetchGems(localStorage.getItem('gems_name'), localStorage.getItem('latitude'), localStorage.getItem('longitude'), this.state.pageSize, this.state.page);

		this.countries();

		/*axios.get(AppConst.APIURL + '/api/countries')
			.then(res => {
				const countryList = res.data.data;
				this.setState({ countryList: countryList });
			})
			.catch(e => {
				let errorMsg = this.displayError(e);
				console.log(errorMsg);
			});*/
		// this.props.history.push("/claim-your-gem-search-result");
		this.setState({
			latitude: '',
			longitude: '',
			address: ''
		});
	}


	handleConfirmReviewClose = () => {
		this.setState({ showConfirMmsg: false });
	};

	handleConfirmReviewShow = () => {
		this.setState({ showConfirMmsg: true });
	};

	handleChange = (e, field) => {
		this.setState({
			[field]: e.target.value
		});
	};

	handleStateChange = (event) => {
		this.handleState(event.target.name, event.target.value)
	}

	handleState = (section, id) => {
		//console.log(section)
		let params = {
			'id': id,
		};
		axios
			.post(AppConst.APIURL + `/api/getStates`, params)
			.then(res => {
				//console.log('--------------res------', res.data.data);
				const stateList = res.data.data;
				if (section == 'personalCountry') {
					this.setState({ personalStateList: stateList });
				} else {
					this.setState({ companyStateList: stateList });
				}

			})
			.catch(e => {
				let errorMsg = this.displayError(e);
			});
	}

	handleCityChange = (event) => {
		this.handleCity(event.target.name, event.target.value)
	}

	handleCity = (section, id) => {
		let params = {
			'id': id,
		};
		axios
			.post(AppConst.APIURL + `/api/getCities`, params)
			.then(res => {
				//console.log('--------------res------', res.data.data);
				const cityList = res.data.data;
				if (section == 'personalState') {
					this.setState({ personalCityList: cityList });
				} else {
					this.setState({ companyCityList: cityList });
				}

			})
			.catch(e => {
				let errorMsg = this.displayError(e);
			});
	}

	handleSetGem = (id) => {
		//console.log("gems_id", id);
		this.setState({
			gems_id: id
		});
	};

	handleProviderSubmit = (values, { resetForm, isSubmitting }) => {
		this.setState({
			addServiceLoader: true,
		});
		const formData = new window.FormData();
		for (const key in values) {
			if (values.hasOwnProperty(key)) {
				formData.append(key, values[key]);
			}
		}

		const expiry_date = values.expiry_date.split('/');

		let newValue = {
			gems_id: this.state.gems_id,
			first_name: values.first_name,
			last_name: values.last_name,
			phone: values.phone,
			email: values.email,
			address: values.address,
			address2: values.address2,
			personalCountry: values.personalCountry,
			personalState: values.personalState,
			personalCity: values.personalCity,
			personalZip: values.personalZip,
			company_name: values.company_name,
			company_phone: values.company_phone,
			addr1: values.addr1,
			addr2: values.addr2,
			companyCountry: values.companyCountry,
			companyState: values.companyState,
			companyCity: values.companyCity,
			companyZip: values.companyZip,
			prefer_purchase: values.prefer_purchase,
			card_number: values.card_number,
			exp_month: expiry_date[0],
			exp_year: expiry_date[1],
			cvc: values.cvc
		};

		const companyFD = new FormData();

		if (this.state.companyDocArr) {
			for (const file of this.state.companyDocArr) {
				companyFD.append("company_doc[]", file);
			}
		}


		companyFD.append('gems_id', newValue.gems_id);
		companyFD.append('first_name', newValue.first_name);
		companyFD.append('last_name', newValue.last_name);
		companyFD.append('phone', newValue.phone);
		companyFD.append('email', newValue.email);
		companyFD.append('address', newValue.address);
		companyFD.append('address2', newValue.address2);
		companyFD.append('personalCountry', newValue.personalCountry);
		companyFD.append('personalState', newValue.personalState);
		companyFD.append('personalZip', newValue.personalZip);
		companyFD.append('company_name', newValue.company_name);
		companyFD.append('company_phone', newValue.company_phone);
		//companyFD.append('company_email', newValue.company_email);
		companyFD.append('addr1', newValue.addr1);
		companyFD.append('addr2', newValue.addr2);
		companyFD.append('companyCountry', newValue.companyCountry);
		companyFD.append('companyState', newValue.companyState);
		companyFD.append('companyCity', newValue.companyCity);
		companyFD.append('companyZip', newValue.companyZip);
		companyFD.append('prefer_purchase', newValue.prefer_purchase);
		companyFD.append('exp_month', newValue.exp_month);
		companyFD.append('exp_year', newValue.exp_year);
		companyFD.append('card_number', newValue.card_number);
		companyFD.append('cvc', newValue.cvc);

		axios
			.post(AppConst.APIURL + '/api/registerProvider', companyFD)
			.then(res => {
				console.log(res);
				resetForm({});
				//isSubmitting(false);
				this.handleModalShowHide();
				//this.handleAddConfirMmsg();
				this.setState({
					successMessage: res.data.message,
					showConfirMmsg: true,
					addServiceLoader: false
				});
			}).catch(e => {
				let errorMsg = this.displayError(e.response);
				this.setState({
					addServiceLoader: false,
					addErrorMessge: errorMsg,
				});

				toast.error(errorMsg, {
					position: "bottom-right",
					autoClose: 5000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true,
					progress: undefined,
				});

				setTimeout(() => {
					this.setState({ deleteErrorMessge: null });
				}, 5000);
			});
	};

	handleLoggedProviderSubmit = (values, { resetForm, isSubmitting }) => {
		this.setState({
			addServiceLoader: true,
		});
		const formData = new window.FormData();
		for (const key in values) {
			if (values.hasOwnProperty(key)) {
				formData.append(key, values[key]);
			}
		}

		let newValue = {
			gems_id: this.state.gems_id,
			user_id: localStorage.getItem('user_id'),
		};

		const companyFD = new FormData();

		if (this.state.companyDocArr) {
			for (const file of this.state.companyDocArr) {
				companyFD.append("company_doc[]", file);
			}
		}

		companyFD.append('gems_id', newValue.gems_id);
		companyFD.append('user_id', newValue.user_id);

		axios
			.post(AppConst.APIURL + '/api/assignProvider', companyFD)
			.then(res => {
				console.log(res);
				resetForm({});
				//isSubmitting(false);
				this.handleModalShowHide();
				//this.handleAddConfirMmsg();
				this.setState({
					successMessage: res.data.message,
					showConfirMmsg: true,
					addServiceLoader: false
				});
			}).catch(e => {
				let errorMsg = this.displayError(e.response);
				this.setState({
					addServiceLoader: false,
					addErrorMessge: errorMsg,
				});

				toast.error(errorMsg, {
					position: "bottom-right",
					autoClose: 5000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true,
					progress: undefined,
				});

				setTimeout(() => {
					this.setState({ deleteErrorMessge: null });
				}, 5000);
			});
	};

	onFileChange = e => {
		//console.log("files=>",e.target.files)

		this.setState({
			companyDocArr: e.target.files
		});
	};

	/******* Provider Registartion End *********/

	onClose = props => {
		if (this.state.showingInfoWindow) {
			this.setState({
				showingInfoWindow: false,
				activeMarker: null
			});
		}
	};

	handleSubmit = (e) => {
		e.preventDefault();
		if (this.state.address === '' && this.state.gems_name === '') {
			toast.error('Company Name OR Location is require to search gems.', {
				position: "bottom-right",
				autoClose: 5000,
				hideProgressBar: false,
				closeOnClick: true,
				pauseOnHover: true,
				draggable: true,
				progress: undefined,
			});
		}
		else {
			this.setState({ showLoadingSpinner: true });
			localStorage.setItem('gems_name', this.state.gems_name);
			localStorage.setItem('latitude', this.state.latitude);
			localStorage.setItem('longitude', this.state.longitude);
			localStorage.setItem('address', this.state.address);
			fetchGemsList(this.state.gems_name, this.state.latitude, this.state.longitude, this.state.pageSize, this.state.page).then(response => {
				const gemsList = response.data.data;
				if (gemsList.length === 0) {
					this.props.history.push(AppConst.SERVERFOLDER + "/claim-your-gem-no-result");
				}
				else {
					// this.fetchGems(this.state.gems_name, this.state.latitude, this.state.longitude, this.state.pageSize, this.state.page);
					this.setState({
						gemsList: gemsList,
						gemDetail: gemsList[0],
						totalCount: response.data.total,
						current_page: response.data.current_page,
						last_page: response.data.last_page,
						to: response.data.to,
						from: response.data.from,
						showLoadingSpinnerForSearch: false,
						preventScrolling: false,
						showFromSearchOnMap: false,
						activePage: this.state.page,
						showLoadingSpinner: false,
						latitudeForSearchAndScroll: this.state.latitude,
						longitudeForSearchAndScroll: this.state.longitude,
						addressForSearchAndScroll: this.state.address,
						gemNameForSearchAndScroll: this.state.gems_name,
						longitude: '',
						latitude: '',
						address: '',
					});
				}
			});
		}
	}

	displayError = (e) => {
		//console.log('error=>',e)
		let errorMessge = '';
		try {
			errorMessge = e.data.message[0] ? e.data.message[0] : e.data.error_description;
		} catch (e) {
			errorMessge = 'Access is denied!';
		}
		return errorMessge;
	}

	position = async () => {
		Geocode.setApiKey("AIzaSyD_BBiV2jnDTjEMxjvFncobeQ3RY0wUP-Y");
		navigator.geolocation.getCurrentPosition(position => {
			Geocode.fromLatLng(position.coords.latitude, position.coords.longitude).then(
				response => {
					const address = response.results[0].formatted_address;
					this.setState({
						address,
						latitude: position.coords.latitude,
						longitude: position.coords.longitude
					});
				})
		});

	}

	handleLngLon = (description) => {
		geocodeByAddress(description)
			.then(results => getLatLng(results[0]))
			.then(({ lat, lng }) =>
				this.setState({
					latitude: lat,
					longitude: lng,
					address: description,
				})
			);
	}

	handleScroll = (event) => {
		let element = event.target;
		if (Math.trunc(element.scrollHeight - element.scrollTop) === element.clientHeight) {
			this.setState({ showLoadingSpinnerForSearch: true, preventScrolling: true });
			let page = this.state.current_page + 1;
			if (page === this.state.last_page + 1) {
				// const pageForApi = 1;
				// this.fetchGems(this.state.gems_name, this.state.latitudeForSearchAndScroll, this.state.longitudeForSearchAndScroll, this.state.pageSize, pageForApi);
				// this.setState({ activePage: pageForApi });
				this.setState({ showLoadingSpinnerForSearch: false, preventScrolling: false });
			} else {
				this.fetchGems(this.state.gems_name, this.state.latitudeForSearchAndScroll, this.state.longitudeForSearchAndScroll, this.state.pageSize, page);
				this.setState({ activePage: page });
				setTimeout(() => {
					element.scrollTo(0, 0);
				}, 100);
			}
		}
	}


	handlePageChange = pageNumber => {
		this.setState({ showLoadingSpinnerForSearch: true, preventScrolling: true });
		this.searchScrollRef.current.scrollTo(0, 0);
		// this.setState({ activePage: pageNumber });
		// this.fetchGems(this.state.gems_name, this.state.latitudeForSearchAndScroll, this.state.longitudeForSearchAndScroll, this.state.pageSize, pageNumber > 0 ? pageNumber : 0);
		if (pageNumber === 'prev') {
			if (this.state.current_page !== 1) {
				const prevpage = this.state.current_page - 1;
				this.fetchGems(this.state.gems_name, this.state.latitudeForSearchAndScroll, this.state.longitudeForSearchAndScroll, this.state.pageSize, prevpage);
			} else {
				this.setState({ showLoadingSpinnerForSearch: false, preventScrolling: false });
			}
		} else if (pageNumber === 'next') {
			if (this.state.current_page === this.state.last_page) {
				this.setState({ showLoadingSpinnerForSearch: false, preventScrolling: false });
			} else {
				const prevpage = this.state.current_page + 1;
				this.fetchGems(this.state.gems_name, this.state.latitudeForSearchAndScroll, this.state.longitudeForSearchAndScroll, this.state.pageSize, prevpage);
			}
		}
	};

	handleSeacrchOnmouseUp = (lat, lng, name) => {
		this.setState({
			latitudeForSearchResultClick: lat,
			longitudeForSearchResultClick: lng,
			name: name,
			showFromSearchOnMap: true,
		});
	}

	handleClick(a) {
		a.preventDefault();
		document.body.classList.toggle('searchResultOpen');
	}

	onMarkerClick = (props, marker, e) => {
		this.setState({
			selectedPlace: props,
			activeMarker: marker,
			showingInfoWindow: true
		});
	}

	displayMarkers() {
		return this.state.gemsList.map((place, index) => {
			return <Marker
				key={index}
				id={index}
				onClick={this.onMarkerClick}
				name={this.state.showFromSearchOnMap ? this.state.name : place.name}
				position={{
					lat: this.state.showFromSearchOnMap ? this.state.latitudeForSearchResultClick : place.lat,
					lng: this.state.showFromSearchOnMap ? this.state.longitudeForSearchResultClick : place.lng
				}}
				icon={{
					url: this.state.showFromSearchOnMap ? iconGemsOther : iconGems,
					anchor: new window.google.maps.Point(32, 32),
					scaledSize: new window.google.maps.Size(40, 40)
				}}
			/>
		})
	}

	render() {
		let pages = [];
		for (let number = 1; number <= this.state.last_page; number++) {
			pages.push(number);
		}

		const hidden = this.state.checked ? '' : 'hidden';
		//values.prefer_purchase
		return (
			<div className="claimSearchPage">
				<div className="claimBannerPan">
					<div className="container">
						<h2>Find and Claim your Gems Company Page</h2>
						<div className="claimForm">
							<form onSubmit={this.handleSubmit}>
								<div className="row">
									<div className="col-12 col-md-5">
										<input type="text" value={this.state.gems_name} onChange={this.handleChangeGemsName} className="form-control" placeholder="Company Name" />
										<span className="helpTxt">e.g. Jacks Coffee Corner</span>
									</div>
									<div className="col-12 col-md-5">
										<GooglePlacesAutocomplete
											placeholder='Location'
											inputClassName={'form-control'}
											suggestionsClassNames={
												{
													container: 'locationDrop',
													suggestion: 'custom-suggestion-classname cursorPointer',
													suggestionActive: 'custom-suggestions-classname--active',
												}
											}
											onSelect={({ description }) => (this.handleLngLon(description))}
											initialValue={this.state.address}
											loader={<LoadingSpinner />}
										/>
										<Link to="/" onClick={this.position} style={{ cursor: 'pointer' }}><i className="fa fa-location-arrow" aria-hidden="true"></i></Link>
										<span className="helpTxt">Address, City, State, Country</span>
									</div>
									<div className="col-12 col-md-2">
										<button type="submit" className="btn btn-primary">Submit</button>
										<span className="helpTxt"><Link to="/">Need help?</Link></span>
									</div>
								</div>
							</form>
							{/* {this.state.showAlert ?
								<div className="alert alert-danger" role="alert">
									Company Name OR Location is require to search gems.
								</div>
								: ''} */}
							{this.state.showLoadingSpinner ? <LoadingSpinner /> : ''}
						</div>
					</div>
				</div>

				<div className="claimBodyPan py-0">
					<div className="container-fluid">
						<div className="mapRow">
							<div className="searchLeftPan">
								<div className="searchHead">
									<h3>{this.state.totalCount === 1 ? 'Search Result' : 'Search Results'}</h3>
									<p className=""><strong>{this.state.totalCount}</strong> {this.state.totalCount === 1 ? 'Result' : 'Results'} {this.state.gemNameForSearchAndScroll ? 'for' : ''} <strong>{this.state.gemNameForSearchAndScroll}</strong> {this.state.addressForSearchAndScroll ? 'near ' : ''}<strong>{this.state.addressForSearchAndScroll}</strong></p>

									<div className="paginationWrap">
										<p className="paginationTxt">{this.state.from}-{this.state.to} of {this.state.totalCount}</p>
										{this.state.totalCount > 10 ?
											<Link className={this.state.current_page === 1 ? 'paginationBtn disable' : 'paginationBtn active'}
												style={{ cursor: this.state.current_page === 1 ? '' : 'pointer' }}
												onClick={() => this.handlePageChange('prev')}>
												<i className="fa fa-angle-left"></i>
											</Link>
											: ''}
										{this.state.totalCount > 10 ?
											<Link className={this.state.current_page === this.state.last_page ? 'paginationBtn disable' : 'paginationBtn active'}
												style={{ cursor: this.state.current_page === this.state.last_page ? '' : 'pointer' }}
												onClick={() => this.handlePageChange('next')}>
												<i className="fa fa-angle-right"></i>
											</Link>
											: ''}
									</div>

									{this.state.showLoadingSpinnerForSearch ? <LoadingSpinner /> : ''}
								</div>
								<div className="searchContent" style={{ overflowY: this.state.preventScrolling ? 'hidden' : 'auto' }} onScroll={this.handleScroll} ref={this.searchScrollRef}>
									<ul>
										{this.state.gemsList.map(data => {
											return <li key={data.id} onClick={() => this.handleSeacrchOnmouseUp(data.lat, data.lng, data.name)} style={{ cursor: 'pointer' }}>
												<div className="searchImgPan">
													{data.images === undefined || data.images.length === 0 ? <h5>No Image</h5> :
														<div className="searchImgWrap">
															{data.images.map((img, index) => {
																return index === 0 ?
																	img.source === 'google_link' ?
																		<img key={index} src={img.image} alt="GEMS" /> :
																		<img key={index} src={'http://54.147.235.207/gems_uat/service/public/upload/gems/' + img.image} alt="GEMS" /> : ''
															})}
														</div>}
												</div>
												<div className="searchConentPan">
													<h4>{data.name}</h4>
													<div className="likesPan">
														<p>Likes <strong>{data.total_like_count ? data.total_like_count : '0'}</strong></p>
													</div>
													<p>{data.address}</p>

													{data.providers.length ?
														data.providers[0].action === 1 ?
															(<span className="btn claimed" title="Already claimed">Already claimed</span>) :
															(<span className="btn claimed" title="Already claimed">Already claimed</span>) : (
															<span className="btn" onClick={() => { this.handleModalShowHide(); this.handleSetGem(data.id) }} title="Claim this Company">Claim this Company</span>
														)
													}

												</div>
											</li>
										})}
									</ul>
								</div>
								<div className="searchFoot">
									<div className="paginationWrap">
										<p className="paginationTxt">{this.state.from}-{this.state.to} of {this.state.totalCount}</p>
										{this.state.totalCount > 10 ?
											<Link className={this.state.current_page === 1 ? 'paginationBtn disable' : 'paginationBtn active'}
												style={{ cursor: this.state.current_page === 1 ? '' : 'pointer' }}
												onClick={() => this.handlePageChange('prev')}>
												<i className="fa fa-angle-left"></i>
											</Link>
											: ''}
										{this.state.totalCount > 10 ?
											<Link className={this.state.current_page === this.state.last_page ? 'paginationBtn disable' : 'paginationBtn active'}
												style={{ cursor: this.state.current_page === this.state.last_page ? '' : 'pointer' }}
												onClick={() => this.handlePageChange('next')}>
												<i className="fa fa-angle-right"></i>
											</Link>
											: ''}
									</div>
								</div>
							</div>
							<div className="mapPan">
								<span className="arrow" onClick={a => this.handleClick(a)}>
									<i className="fa fa-arrow-right" aria-hidden="true"></i>
									<i className="fa fa-arrow-left" aria-hidden="true"></i>
								</span>
								<div className="mapWrap">
									<Map
										google={this.props.google}
										zoom={this.state.showFromSearchOnMap ? 20 : 14}
										center={{
											lat: this.state.showFromSearchOnMap ? this.state.latitudeForSearchResultClick : this.state.gemDetail.lat,
											lng: this.state.showFromSearchOnMap ? this.state.longitudeForSearchResultClick : this.state.gemDetail.lng
										}}
										disableDoubleClickZoom={false}
										scrollwheel={false}
									>
										{this.displayMarkers()}
										<InfoWindow
											marker={this.state.activeMarker}
											visible={this.state.showingInfoWindow}
											onClose={this.onClose}
										>
											{this.state.gemsList.map(data => {
												return data.name === this.state.selectedPlace.name &&
													<div className="" key={data.id}>
														<div className="container-fluid">
															<div className="row">
																<div className="col-md-7">
																	<h6>{this.state.selectedPlace.name}</h6>
																</div>
																<div className="col-md-5 text-right">
																	Likes <strong className="text-primary bolder">{data.total_like_count ? data.total_like_count : '0'}</strong>
																</div>
															</div>
															<p className="">{data.address}</p>
															{/* <a className="" style={{ cursor: 'pointer' }} onMouseOver={this.handleclick}>Claim this Company</a> */}
														</div>
													</div>
											})}

										</InfoWindow>

									</Map>
								</div>
							</div>
						</div>


						<Modal size="xl" show={this.state.showHide} onHide={() => this.handleModalShowHide()}>
							<Modal.Header closeButton>
								<Modal.Title>Fillup The Registration Form and Claim</Modal.Title>
							</Modal.Header>
							<Modal.Body className="registerForm">
								{this.state.addErrorMessge ? (
									<div className="alert alert-danger" role="alert">
										{this.state.addErrorMessge}
									</div>
								) : null}
								<Row>
									<Col sm={12}>
										{this.state.addServiceLoader ? <LoadingSpinner /> : null}
									</Col>
								</Row>
								<Formik
									initialValues={initialValues}
									validationSchema={addProviderSchema}
									onSubmit={this.handleProviderSubmit}
								>
									{({ values, errors, touched, isSubmitting, handleChange, handleBlur, setFieldValue }) => {
										return (

											<Form>
												<div className="formSection">
													<div className="sectionTitle">
														<h3>Personal Information</h3>
														<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras mollis vulputate</p>
													</div>
													<div className="row">
														<div className="col-6">
															<FormGroup controlId="formControlsTextarea">
																<Field
																	name="first_name"
																	type="text"
																	className={'form-control'}
																	autoComplete="nope"
																	placeholder="First Name"
																	value={values.first_name || ''}
																/>
																{errors.first_name && touched.first_name ? (
																	<span className="errorMsg ml-3">{errors.first_name}</span>
																) : null}
																<FormControl.Feedback />
															</FormGroup>
														</div>
														<div className="col-6">
															<FormGroup controlId="formControlsTextarea">
																<Field
																	name="last_name"
																	type="text"
																	className={'form-control'}
																	autoComplete="nope"
																	placeholder="Last Name"
																	value={values.last_name || ''}
																/>
																{errors.last_name && touched.last_name ? (
																	<span className="errorMsg ml-3">{errors.last_name}</span>
																) : null}
																<FormControl.Feedback />
															</FormGroup>
														</div>
														<div className="col-6">
															<FormGroup controlId="formControlsTextarea">
																<Field
																	name="phone"
																	type="text"
																	className={'form-control'}
																	autoComplete="nope"
																	placeholder="Phone No"
																	value={values.phone || ''}
																/>
																{errors.phone && touched.phone ? (
																	<span className="errorMsg ml-3">{errors.phone}</span>
																) : null}
																<FormControl.Feedback />
															</FormGroup>
														</div>
														{/*<div className="col-6">
															<FormGroup controlId="formControlsTextarea">
																<Field
																	name="email"
																	type="text"
																	className={'form-control'}
																	autoComplete="nope"
																	placeholder="EMAIL ADDRESS"
																	value={values.email || ''}
																/>
																{errors.email && touched.email ? (
																	<span className="errorMsg ml-3">{errors.email}</span>
																) : null}
																<FormControl.Feedback />
															</FormGroup>
														</div>*/}
														<div className="col-12">
															<FormGroup controlId="formControlsTextarea">
																<Field
																	name="address"
																	component="textarea"
																	className={'form-control'}
																	autoComplete="nope"
																	placeholder="ADDRESS LINE 1"
																	value={values.address || ''}
																/>
																{errors.address && touched.address ? (
																	<span className="errorMsg ml-3">{errors.address}</span>
																) : null}
																<FormControl.Feedback />
															</FormGroup>
														</div>
														<div className="col-12">
															<FormGroup controlId="formControlsTextarea">
																<Field
																	name="address2"
																	component="textarea"
																	className={'form-control'}
																	autoComplete="nope"
																	placeholder="ADDRESS2 LINE 2(optional)"
																	value={values.address2 || ''}
																/>
																{errors.address2 && touched.address2 ? (
																	<span className="errorMsg">{errors.address2}</span>
																) : null}
																<FormControl.Feedback />
															</FormGroup>
														</div>
														<div className="col-4">
															<FormGroup controlId="formBasicText">
																<Field
																	name="personalCountry"
																	component="select"
																	className={`input-elem ${values.personalCountry &&
																		'input-elem-filled'} form-control`}
																	autoComplete="nope"
																	value={values.personalCountry || ''}
																	onChange={e => {
																		handleChange(e);
																		this.handleStateChange(e);
																	}}
																>
																	<option value="">
																		{this.state.countryList.length ? 'Select Country' : 'Loading...'}
																	</option>
																	{this.state.countryList.map(country => (
																		<option
																			key={country.id}
																			value={country.id}
																		>
																			{country.name}
																		</option>
																	))}
																</Field>
																{errors.personalCountry && touched.personalCountry ? (
																	<span className="errorMsg ml-3">
																		{errors.personalCountry}
																	</span>
																) : null}
															</FormGroup>
														</div>
														<div className="col-4">
															<FormGroup controlId="formBasicText">
																<Field
																	name="personalState"
																	component="select"
																	className={`input-elem topShift ${values.personalState &&
																		'input-elem-filled'} form-control`}
																	autoComplete="nope"
																	value={values.personalState || ''}
																	onChange={e => {
																		handleChange(e);
																		this.handleCityChange(e);
																	}}
																	disabled={
																		this.state.personalStateList.length ? false : true
																	}
																>
																	<option value="">
																		Select state
	                                                    </option>
																	{this.state.personalStateList.map(state => (
																		<option value={state.id} key={state.id}>
																			{state.name}
																		</option>
																	))}
																</Field>
																{errors.personalState &&
																	touched.personalState ? (
																		<span className="errorMsg ml-3">
																			{errors.personalState}
																		</span>
																	) : null}
															</FormGroup>
														</div>
														<div className="col-4">
															<FormGroup controlId="formBasicText">
																<Field
																	component="select"
																	name="personalCity"
																	placeholder="select"
																	className={`input-elem topShift ${values.personalCity &&
																		'input-elem-filled'} form-control`}
																	value={values.personalCity || ''}
																	onChange={e => {
																		handleChange(e);
																	}}
																	disabled={this.state.personalCityList.length ? false : true}
																>
																	<option value="">Select City</option>

																	{this.state.personalCityList.map(city => (
																		<option value={city.id} key={city.id}>
																			{city.name}
																		</option>
																	))}
																</Field>
																{errors.personalCity && touched.personalCity ? (
																	<span className="errorMsg ml-3">
																		{errors.personalCity}
																	</span>
																) : null}
															</FormGroup>
														</div>
														<div className="col-4">
															<FormGroup controlId="formBasicText">
																<Field
																	name="personalZip"
																	type="text"
																	className={'form-control'}
																	autoComplete="nope"
																	placeholder="Zip"
																	value={values.personalZip || ''}
																/>
																{errors.personalZip && touched.personalZip ? (
																	<span className="errorMsg ml-3">{errors.personalZip}</span>
																) : null}
																<FormControl.Feedback />
															</FormGroup>
														</div>
													</div>
												</div>
												<div className="formSection">
													<div className="sectionTitle">
														<h3>Company Information</h3>
														<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras mollis vulputate</p>
													</div>
													<div className="row">
														<div className="col-12">
															<FormGroup controlId="formControlsTextarea">
																<Field
																	name="company_name"
																	type="text"
																	className={'form-control'}
																	autoComplete="nope"
																	placeholder="Company Name"
																	value={values.company_name || ''}
																/>
																{errors.company_name && touched.company_name ? (
																	<span className="errorMsg ml-3">{errors.company_name}</span>
																) : null}
																<FormControl.Feedback />
															</FormGroup>
														</div>
														<div className="col-6">
															<FormGroup controlId="formControlsTextarea">
																<Field
																	name="company_phone"
																	type="text"
																	className={'form-control'}
																	autoComplete="nope"
																	placeholder="Company Phone No"
																	value={values.company_phone || ''}
																/>
																{errors.company_phone && touched.company_phone ? (
																	<span className="errorMsg ml-3">{errors.company_phone}</span>
																) : null}
																<FormControl.Feedback />
															</FormGroup>
														</div>
														<div className="col-6">
															<FormGroup controlId="formControlsTextarea">
																<Field
																	name="email"
																	type="text"
																	className={'form-control'}
																	autoComplete="nope"
																	placeholder="Company EMAIL ADDRESS (Willbe used as login)"
																	value={values.email || ''}
																/>
																{errors.email && touched.email ? (
																	<span className="errorMsg ml-3">{errors.email}</span>
																) : null}
																<FormControl.Feedback />
															</FormGroup>
														</div>
														<div className="col-12">
															<FormGroup controlId="formControlsTextarea">
																<Field
																	name="addr1"
																	component="textarea"
																	className={'form-control'}
																	autoComplete="nope"
																	placeholder="ADDRESS LINE 1"
																	value={values.addr1 || ''}
																/>
																{errors.addr1 && touched.addr1 ? (
																	<span className="errorMsg ml-3">{errors.addr1}</span>
																) : null}
																<FormControl.Feedback />
															</FormGroup>
														</div>
														<div className="col-12">
															<FormGroup controlId="formControlsTextarea">
																<Field
																	name="addr2"
																	component="textarea"
																	className={'form-control'}
																	autoComplete="nope"
																	placeholder="ADDRESS LINE 2(optional)"
																	value={values.addr2 || ''}
																/>
																{errors.addr2 && touched.addr2 ? (
																	<span className="errorMsg ml-3">{errors.addr2}</span>
																) : null}
																<FormControl.Feedback />
															</FormGroup>
														</div>
														<div className="col-4">
															<FormGroup controlId="formBasicText">
																<Field
																	name="companyCountry"
																	component="select"
																	className={`input-elem ${values.companyCountry &&
																		'input-elem-filled'} form-control`}
																	autoComplete="nope"
																	value={values.companyCountry || ''}
																	onChange={e => {
																		handleChange(e);
																		this.handleStateChange(e);
																	}}
																>
																	<option value="">
																		{this.state.countryList.length ? 'Select Country' : 'Loading...'}
																	</option>
																	{this.state.countryList.map(country => (
																		<option
																			key={country.id}
																			value={country.id}
																		>
																			{country.name}
																		</option>
																	))}
																</Field>
																{errors.companyCountry && touched.companyCountry ? (
																	<span className="errorMsg ml-3">
																		{errors.companyCountry}
																	</span>
																) : null}
															</FormGroup>
														</div>
														<div className="col-4">
															<FormGroup controlId="formBasicText">
																<Field
																	name="companyState"
																	component="select"
																	className={`input-elem topShift ${values.companyState &&
																		'input-elem-filled'} form-control`}
																	autoComplete="nope"
																	value={values.companyState || ''}
																	onChange={e => {
																		handleChange(e);
																		this.handleCityChange(e);
																	}}
																	disabled={
																		this.state.companyStateList.length ? false : true
																	}
																>
																	<option value="">
																		Select state
	                                                    </option>
																	{this.state.companyStateList.map(state => (
																		<option value={state.id} key={state.id}>
																			{state.name}
																		</option>
																	))}
																</Field>
																{errors.companyState &&
																	touched.companyState ? (
																		<span className="errorMsg ml-3">
																			{errors.companyState}
																		</span>
																	) : null}


															</FormGroup>
														</div>
														<div className="col-4">
															<FormGroup controlId="formBasicText">
																<Field
																	component="select"
																	name="companyCity"
																	placeholder="select"
																	className={`input-elem topShift ${values.companyCity &&
																		'input-elem-filled'} form-control`}
																	value={values.companyCity || ''}
																	onChange={e => {
																		handleChange(e);
																	}}
																	disabled={this.state.companyCityList.length ? false : true}
																>
																	<option value="">Select City</option>

																	{this.state.companyCityList.map(city => (
																		<option value={city.id} key={city.id}>
																			{city.name}
																		</option>
																	))}
																</Field>
																{errors.companyCity && touched.companyCity ? (
																	<span className="errorMsg ml-3">
																		{errors.companyCity}
																	</span>
																) : null}
															</FormGroup>
														</div>
														<div className="col-4">
															<FormGroup controlId="formBasicText">
																<Field
																	name="companyZip"
																	type="text"
																	className={'form-control'}
																	autoComplete="nope"
																	placeholder="Zip"
																	value={values.companyZip || ''}
																/>
																{errors.companyZip && touched.companyZip ? (
																	<span className="errorMsg ml-3">{errors.companyZip}</span>
																) : null}
																<FormControl.Feedback />
															</FormGroup>
														</div>

														<div className="col-12">
															<FormGroup controlId="formControlsTextarea">
																<span>Upload Documents</span>
																<input id="company_doc" name="company_doc" type="file" className="form-control" onChange={this.onFileChange} multiple />
																{/*<Field
													                name="company_doc"
													                type="file"
													                title="Select a file"
													                className={'form-control'}
													                onChange={this.onFileChange} 
													                multiple
													              />
		                                                        {errors.company_doc && touched.company_doc ? (
																	<span className="errorMsg ml-3">{errors.company_doc}</span>
																) : null}*/}
																<FormControl.Feedback />
															</FormGroup>
														</div>
														<div className="col-12">
															<div className="form-check">
																<FormGroup controlId="formBasicText">
																	<Field
																		name="prefer_purchase"
																		type="checkbox"
																		className={'form-check-input'}
																		autoComplete="nope"
																		checked={values.prefer_purchase}
																		value={values.prefer_purchase}
																	/>
																	<FormControl.Feedback />
																</FormGroup>
																<label className="form-check-label" htmlFor="prefer_purchase">
																	I want to purchase preferred package after 3 months free trail.</label>
															</div>
														</div>
													</div>
												</div>
												{values.prefer_purchase === true &&
													<div className="formSection">
														<div className="sectionTitle">
															<h3>Payment Information</h3>
															<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras mollis vulputate</p>
															<h4><i className="fa fa-credit-card" aria-hidden="true"></i> Credit/Debit Cards Details</h4>
														</div>
														<div className="row">
															<div className="col-12">
																<FormGroup controlId="formControlsTextarea">
																	<Field
																		name="card_number"
																		type="number"
																		className={'form-control'}
																		autoComplete="nope"
																		placeholder="Card Number"
																		value={values.card_number || ''}
																	/>
																	{errors.card_number && touched.card_number ? (
																		<span className="errorMsg ml-3">{errors.card_number}</span>
																	) : null}
																	<FormControl.Feedback />
																</FormGroup>
															</div>
															<div className="col-4">
																<FormGroup controlId="formControlsTextarea">
																	<Field
																		name="name_on_card"
																		type="text"
																		className={'form-control'}
																		autoComplete="nope"
																		placeholder="Name on Card"
																		value={values.name_on_card || ''}
																	/>
																	{errors.name_on_card && touched.name_on_card ? (
																		<span className="errorMsg ml-3">{errors.name_on_card}</span>
																	) : null}
																	<FormControl.Feedback />
																</FormGroup>
															</div>
															<div className="col-4">
																<FormGroup controlId="formControlsTextarea">
																	<Field
																		name="expiry_date"
																		type="text"
																		className={'form-control'}
																		autoComplete="nope"
																		placeholder="Expiry Date MM/YY"
																		value={values.expiry_date || ''}
																	/>
																	{errors.expiry_date && touched.expiry_date ? (
																		<span className="errorMsg ml-3">{errors.expiry_date}</span>
																	) : null}
																	<FormControl.Feedback />
																</FormGroup>
															</div>
															<div className="col-4 cvv">
																<FormGroup controlId="formControlsTextarea">
																	<Field
																		name="cvc"
																		type="number"
																		className={'form-control'}
																		autoComplete="nope"
																		placeholder="CVV"
																		value={values.cvc || ''}
																	/>
																	{errors.cvc && touched.cvc ? (
																		<span className="errorMsg ml-3">{errors.cvc}</span>
																	) : null}
																	<FormControl.Feedback />
																</FormGroup>
															</div>
														</div>
													</div>}
												<div className="btnPan">
													<Button className="btn blue" type="submit">Register Now</Button>
												</div>
											</Form>
										);
									}}
								</Formik>

							</Modal.Body>
						</Modal>

						{/*======  After login company documents popup popup  ===== */}
						<Modal size="xl" show={this.state.showHideLogin}>
							<Modal.Header closeButton onClick={() => this.handleModalShowHide()}>
								<Modal.Title>Please Upload Documents for Company Information and Claim</Modal.Title>
							</Modal.Header>
							<Modal.Body className="registerForm">
								{this.state.addErrorMessge ? (
									<div className="alert alert-danger" role="alert">
										{this.state.addErrorMessge}
									</div>
								) : null}
								<Row>
									<Col sm={12}>
										{this.state.addServiceLoader ? <LoadingSpinner /> : null}
									</Col>
								</Row>
								<Formik
									initialValues={initialValues}
									validationSchema={assignProviderSchema}
									onSubmit={this.handleLoggedProviderSubmit}
								>
									{({ values, errors, touched, isSubmitting, handleChange }) => {
										return (

											<Form>
												<div className="formSection">
													<div className="sectionTitle">
														<h3>Company Information</h3>
														<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras mollis vulputate</p>
													</div>
													<div className="row">
														<div className="col-12">
															<FormGroup controlId="formControlsTextarea">
																<span>Upload Documents</span>
																<input id="company_doc" name="company_doc" type="file" className="form-control" onChange={this.onFileChange} multiple />
																<FormControl.Feedback />
															</FormGroup>
														</div>
													</div>
												</div>
												<div className="btnPan">
													<Button className="btn blue" type="submit">Submit</Button>
												</div>
											</Form>
										);
									}}
								</Formik>
							</Modal.Body>
						</Modal>
						{/*======  confirmation popup  ===== */}
						<Modal
							show={this.state.showConfirMmsg}
							onHide={this.handleConfirmReviewClose}
							className="payOptionPop"
							size="xl"
						>
							<Modal.Body>
								<Row>
									<Col md={12} className="text-center">
										<h5>{this.state.successMessage}</h5>
									</Col>
								</Row>
							</Modal.Body>
							<Modal.Footer>
								<Button
									onClick={this.handleConfirmReviewClose}
									className="but-gray" > Done
		                        </Button>
							</Modal.Footer>
						</Modal>

					</div>
				</div>
				<ToastContainer />
			</div>
		);
	}
}
ClaimYourGemSearchResult.propTypes = {
	handleAddConfirMmsg: PropTypes.func,
	businessId: PropTypes.number,
	handelAddModalClose: PropTypes.func,
};
export default GoogleApiWrapper({
	apiKey: 'AIzaSyD_BBiV2jnDTjEMxjvFncobeQ3RY0wUP-Y'
})(ClaimYourGemSearchResult);