import React, { Component } from 'react';
import searchCompany from '../../assets/images/img-search.png';
import createAccount from '../../assets/images/img-create-account.png';
import validateAccount from '../../assets/images/img-validate-account.png';
import claimCompany from '../../assets/images/img-claim.png';
import VisibilitySensor from 'react-visibility-sensor';

import GooglePlacesAutocomplete from 'react-google-places-autocomplete';
import { geocodeByAddress, getLatLng } from 'react-google-places-autocomplete';
import Geocode from "react-geocode";
import { fetchGemsList } from '../../action/cmsAction'
import LoadingSpinner from '../../common/LoadingSpinner/LoadingSpinner';
import { ToastContainer, toast } from 'react-toastify';
import { Link } from 'react-router-dom';
import * as AppConst from '../../common/constants';

class ClaimYourGem extends Component {
	state = {
		address: '',
		latitude: '',
		longitude: '',
		gemsName: '',
		showAlert: false,
		showLoadingSpinner: false,
		pageSize: 10,
		page: 1,
	}

	componentDidMount() {
		window.scrollTo(0, 0)
	}

	handleLngLon = (description) => {
		geocodeByAddress(description)
			.then(results => getLatLng(results[0]))
			.then(({ lat, lng }) =>
				this.setState({
					latitude: lat,
					longitude: lng,
					address: description
				})
			);
	}

	handleSubmit = (e) => {
		e.preventDefault();
		if (this.state.address.trim() === '' && this.state.gemsName.trim() === '') {
			toast.error('Company Name OR Location is require to search gems.', {
				position: "bottom-right",
				autoClose: 5000,
				hideProgressBar: false,
				closeOnClick: true,
				pauseOnHover: true,
				draggable: true,
				progress: undefined,
			});
		}
		else {
			this.setState({ showLoadingSpinner: true });
			localStorage.setItem('gems_name', this.state.gemsName);
			localStorage.setItem('latitude', this.state.latitude);
			localStorage.setItem('longitude', this.state.longitude);
			localStorage.setItem('address', this.state.address);
			fetchGemsList(this.state.gemsName, this.state.latitude, this.state.longitude, this.state.pageSize, this.state.page).then(response => {
				const gemsList = response.data.data;
				if (gemsList.length === 0) {
					this.props.history.push(AppConst.SERVERFOLDER + "/claim-your-gem-no-result");
				}
				else {
					this.props.history.push(AppConst.SERVERFOLDER + "/claim-your-gem-search-result");
				}
			});
		}
	}

	position = async () => {
		Geocode.setApiKey("AIzaSyD_BBiV2jnDTjEMxjvFncobeQ3RY0wUP-Y");
		navigator.geolocation.getCurrentPosition(position => {
			Geocode.fromLatLng(position.coords.latitude, position.coords.longitude).then(
				response => {
					const address = response.results[0].formatted_address;
					this.setState({
						address,
						latitude: position.coords.latitude,
						longitude: position.coords.longitude
					});
				})
		});
	}

	handleChange = (event) => {
		this.setState({ gemsName: event.target.value });
	}

	onVisibilityChange1 = isVisible => {
		if (isVisible) {
			document.getElementById("counter1").classList.add("countStart");
		}
	}
	onVisibilityChange2 = isVisible => {
		if (isVisible) {
			document.getElementById("counter2").classList.add("countStart");
		}
	}
	onVisibilityChange3 = isVisible => {
		if (isVisible) {
			document.getElementById("counter3").classList.add("countStart");
		}
	}
	onVisibilityChange4 = isVisible => {
		if (isVisible) {
			document.getElementById("counter4").classList.add("countStart");
		}
	}
	onVisibilityChange5 = isVisible => {
		if (isVisible) {
			document.getElementById("counter5").classList.add("countStart");
		}
	}

	topClick(e) {
		e.preventDefault();
		//window.scrollTo(0, 0)
		window.scrollTo({ top: 0, behavior: 'smooth' });
	}

	render() {
		return (
			<div className="claimGemPage">
				<div className="claimBannerPan">
					<div className="container mb-5">
						<h2>Find and Claim your Gems Company Page</h2>
						<div className="claimForm">
							<form onSubmit={this.handleSubmit}>
								<div className="row">
									<div className="col-12 col-md-5">
										<input type="text" value={this.state.gemsName} onChange={this.handleChange} className="form-control" placeholder="Company Name" />
										<span className="helpTxt">e.g. Jacks Coffee Corner</span>
									</div>
									<div className="col-12 col-md-5">
										<GooglePlacesAutocomplete
											placeholder='Location'
											inputClassName={'form-control'}
											suggestionsClassNames={
												{
													container: 'locationDrop',
													suggestion: 'custom-suggestion-classname cursorPointer',
													suggestionActive: 'custom-suggestions-classname--active',
												}
											}
											onSelect={({ description }) => (this.handleLngLon(description))}
											initialValue={this.state.address}
											loader={<LoadingSpinner />}
										/>
										<span onClick={this.position} style={{ cursor: 'pointer' }}><i className="fa fa-location-arrow" aria-hidden="true"></i></span>
										<span className="helpTxt">Address, City, State, Country</span>
									</div>
									<div className="col-12 col-md-2">
										<button type="submit" className="btn btn-primary">Submit</button>
										<span className="helpTxt"><Link to="/">Need help?</Link></span>
									</div>
								</div>
							</form>
							{/* {this.state.showAlert ?
								<p className="text-light bolder small mt-4 mb-n3 text-center">*Company Name OR Location is require to search gems.</p>
								: ''} */}
							{this.state.showLoadingSpinner ? <LoadingSpinner /> : ''}
						</div>
						<ul className="claimList">
							<li>
								<span className="claimIcon1"></span>
								<div className="claimTxt">Etiam pharetra orci ut nisi sagittis ut interdum orci ornare.</div></li>
							<li>
								<span className="claimIcon2"></span>
								<div className="claimTxt">Etiam pharetra orci ut nisi sagittis ut interdum orci ornare.</div></li>
							<li>
								<span className="claimIcon3"></span>
								<div className="claimTxt">Etiam pharetra orci ut nisi sagittis ut interdum orci ornare.</div></li>
						</ul>
					</div>
				</div>

				{localStorage.getItem('token') ? '' :
					<div className="claimBodyPan claimMainPage">
						<div className="container">

							<div className="row">
								<div className="col-12 col-sm-5 claimImg">
									<VisibilitySensor onChange={this.onVisibilityChange1}>
										<div className="claimImgPan">
											<img src={searchCompany} alt="GEMS" />
											<div className="counter" id="counter1">
												<div className="numbers">
													<div>00</div>
													<div>10</div>
													<div>09</div>
													<div>08</div>
													<div>07</div>
													<div>06</div>
													<div>05</div>
													<div>04</div>
													<div>03</div>
													<div>02</div>
													<div>01</div>
												</div>
											</div>
										</div>
									</VisibilitySensor>
								</div>
								<div className="col-12 col-sm-7 claimTxt">
									<h3>Search your company</h3>
									<p>On the gems.community website, on the Claim your Company page, type the name of your company, and click the search button. If your company doesn’t pop-up, than your company is not yet defined as a Gem. However, if your company does pop-up, it’s a Gem, go to step 2</p>
								</div>
							</div>
							<div className="row">
								<div className="col-12 col-sm-7 claimTxt">
									<h3>Create your account</h3>
									<p>On the Create a free account page, enter your full name, work email, and phone number in the relevant fields. Select your country from the drop-down menu, then click the Create account button</p>
								</div>
								<div className="col-12 col-sm-5 claimImg">
									<VisibilitySensor onChange={this.onVisibilityChange2}>
										<div className="claimImgPan">
											<img src={createAccount} alt="GEMS" />
											<div className="counter right" id="counter2">
												<div className="numbers">
													<div>01</div>
													<div>00</div>
													<div>10</div>
													<div>09</div>
													<div>08</div>
													<div>07</div>
													<div>06</div>
													<div>05</div>
													<div>04</div>
													<div>03</div>
													<div>02</div>
												</div>
											</div>
										</div>
									</VisibilitySensor>
								</div>
							</div>
							<div className="row">
								<div className="col-12 col-sm-5 claimImg">
									<VisibilitySensor onChange={this.onVisibilityChange3}>
										<div className="claimImgPan">
											<img src={validateAccount} alt="GEMS" />
											<div className="counter" id="counter3">
												<div className="numbers">
													<div>02</div>
													<div>01</div>
													<div>00</div>
													<div>10</div>
													<div>09</div>
													<div>08</div>
													<div>07</div>
													<div>06</div>
													<div>05</div>
													<div>04</div>
													<div>03</div>
												</div>
											</div>
										</div>
									</VisibilitySensor>
								</div>
								<div className="col-12 col-sm-7 claimTxt">
									<h3>Validate your Account</h3>
									<p>In your inbox, you'll receive an Activate your Gems account email. Click the Activate account button</p>
								</div>
							</div>
							<div className="row">
								<div className="col-12 col-sm-7 claimTxt">
									<h3>Claim your Gem</h3>
									<p>On the Set password screen, enter your password in the Password field. Select the Gems Terms and Conditions check box, then click the Complete setup button</p>
								</div>
								<div className="col-12 col-sm-5 claimImg">
									<VisibilitySensor onChange={this.onVisibilityChange4}>
										<div className="claimImgPan">
											<img src={claimCompany} alt="GEMS" />
											<div className="counter right" id="counter4">
												<div className="numbers">
													<div>03</div>
													<div>02</div>
													<div>01</div>
													<div>00</div>
													<div>10</div>
													<div>09</div>
													<div>08</div>
													<div>07</div>
													<div>06</div>
													<div>05</div>
													<div>04</div>
												</div>
											</div>
										</div>
									</VisibilitySensor>
								</div>
							</div>
						</div>
						<div className="findGemCol">
							<div className="container">
								<VisibilitySensor onChange={this.onVisibilityChange5}>
									<div className="findGem">
										<div className="link" onClick={e => this.topClick(e)}><strong>Find</strong> and <strong>Claim</strong> your Gems Company Page</div>
										<div className="counter" id="counter5">
											<div className="numbers">
												<div>04</div>
												<div>03</div>
												<div>02</div>
												<div>01</div>
												<div>00</div>
												<div>10</div>
												<div>09</div>
												<div>08</div>
												<div>07</div>
												<div>06</div>
												<div>05</div>
											</div>
										</div>
									</div>
								</VisibilitySensor>
							</div>
						</div>
					</div>
				}

				<ToastContainer />
			</div>
		);
	}
}

export default ClaimYourGem;