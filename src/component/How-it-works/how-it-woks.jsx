import React, { Component } from 'react';
import VisibilitySensor from 'react-visibility-sensor';

import started from '../../assets/images/img-started.png';
import explore from '../../assets/images/img-explore.png';
import creat from '../../assets/images/img-creat.png';
import share from '../../assets/images/img-share.png';


window.onbeforeunload = function () {
	window.scrollTo(0, 0);
}


class HowItWorks extends Component {

	componentDidMount() {
		window.scrollTo(0, 0)
	}


	state = {}

	onVisibilityChange1 = isVisible => {
		if (isVisible) {
			document.getElementById("line1").classList.add("show");
		}
	}
	onVisibilityChange2 = isVisible => {
		if (isVisible) {
			document.getElementById("line2").classList.add("show");
		}
	}
	onVisibilityChange3 = isVisible => {
		if (isVisible) {
			document.getElementById("line3").classList.add("show");
		}
	}
	onVisibilityChange4 = isVisible => {
		if (isVisible) {
			document.getElementById("line4").classList.add("show");
		}
	}
	onVisibilityChange5 = isVisible => {
		if (isVisible) {
			document.getElementById("line5").classList.add("show");
		}
	}
	onVisibilityChange6 = isVisible => {
		if (isVisible) {
			document.getElementById("line6").classList.add("show");
		}
	}
	onVisibilityChange7 = isVisible => {
		if (isVisible) {
			document.getElementById("line7").classList.add("show");
		}
	}

	render() {
		return (
			<div className="howWorksPage">

				<div className="commonBanner">
					<div className="container">
						<div className="line"></div>
						<div className="row">
							<div className="col-12">
								<h2>How it Works</h2>
							</div>
						</div>
					</div>
				</div>

				<div className="howWorksContainer contentPan">
					<div className="container">
						<div className="row">
							<VisibilitySensor onChange={this.onVisibilityChange1}>
								<div className="col-12 registerCol">
									<div className="stepBox register">
										<em className="gems-login stepIcon"></em>
										<div className="stepTxt">
											<span>Step 01</span>
										Register
									</div>
									</div>

									<div className="line1" id="line1">
										{/*Line1*/}
										<div className="gemsImg1"></div>
									</div>
								</div>
							</VisibilitySensor>
						</div>
						<div className="row">
							<div className="col-12 col-lg-7 col-xl-8 startedImg">
								<img src={started} alt="GEMS" />
							</div>
							<div className="col-12 col-lg-5 col-xl-4 startedTxt">
								<h3>Let’s get Started</h3>
								<ul>
									<li><strong>Lorem ipsum dolor sit amet,</strong> consectetur adipiscing elit. Cras mollis vulputate rhoncus. Ut auctor nulla ut ligula</li>

									<li><strong>Nulla facilisi. Aenean eu nisi diam.</strong> Cras non felis lobortis massa congue varius et et eros</li>

									<li><strong>Nunc sed dui commodo, condimentum nibh nec,</strong> sagittis est. Aliquam non orci vel lectus lacinia tempor. Mauris vitae ullamcorper ante. In euismod id nulla ut facilisis. Aliquam sollicitudin erat sed dignissim gravida. Ut tincidunt nibh vitae massa molestie vestibulum</li>
								</ul>
							</div>
						</div>
						<div className="row">
							<div className="col-12 exploreCol">
								<VisibilitySensor onChange={this.onVisibilityChange2}>
									<div className="line2" id="line2">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100">
											<path className="lineDraw" fill="none" stroke="black" strokeWidth="0.4"
												d="M99 0 a11 11 0 0 1 -11 11 H12 a11 11 0 0 0 -11 11" />
										</svg>
									</div>
								</VisibilitySensor>
								<div className="stepBox explore">
									<em className="gems-compass stepIcon"></em>
									<div className="stepTxt">
										<span>Step 02</span>
									Explore
								</div>
								</div>
								<VisibilitySensor onChange={this.onVisibilityChange3}>
									<div className="line3" id="line3">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100">
											<path className="lineDraw" fill="none" stroke="black" strokeWidth="0.4"
												d="M1 0 a11 11 0 0 0 11 11 H88 a11 11 0 0 1 11 11" />
										</svg>
									</div>
								</VisibilitySensor>
							</div>
						</div>
						<div className="row">
							<div className="col-12 col-lg-5 col-xl-4 exploreTxt">
								<h3>Explore the Gems</h3>
								<ul>
									<li><strong>Lorem ipsum dolor sit amet,</strong> consectetur adipiscing elit. Cras mollis vulputate rhoncus. Ut auctor nulla ut ligula</li>

									<li><strong>Nulla facilisi. Aenean eu nisi diam.</strong> Cras non felis lobortis massa congue varius et et eros</li>

									<li><strong>Nunc sed dui commodo, condimentum nibh nec,</strong> sagittis est. Aliquam non orci vel lectus lacinia tempor. Mauris vitae ullamcorper ante. In euismod id nulla ut facilisis. Aliquam sollicitudin erat sed dignissim gravida. Ut tincidunt nibh vitae massa molestie vestibulum</li>
								</ul>
							</div>
							<div className="col-12 col-lg-7 col-xl-8 exploreImg">
								<img src={explore} alt="GEMS" />
							</div>
						</div>
						<div className="row">
							<div className="col-12 createCol">
								<VisibilitySensor onChange={this.onVisibilityChange4}>
									<div className="line4" id="line4">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100">
											<path className="lineDraw" fill="none" stroke="black" strokeWidth="0.4"
												d="M1 0 a11 11 0 0 0 11 11 H88 a11 11 0 0 1 11 11" />
										</svg>
									</div>
								</VisibilitySensor>
								<div className="stepBox create">
									<em className="gems-edit stepIcon"></em>
									<div className="stepTxt">
										<span>Step 03</span>
									Create
								</div>
								</div>
								<VisibilitySensor onChange={this.onVisibilityChange5}>
									<div className="line5" id="line5">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100">
											<path className="lineDraw" fill="none" stroke="black" strokeWidth="0.4"
												d="M99 0 a11 11 0 0 1 -11 11 H12 a11 11 0 0 0 -11 11" />
										</svg>
									</div>
								</VisibilitySensor>
							</div>
						</div>
						<div className="row">
							<div className="col-12 col-lg-7 col-xl-8 creatImg">
								<img src={creat} alt="GEMS" />
							</div>
							<div className="col-12 col-lg-5 col-xl-4 creatTxt">
								<h3>Create your own Gem</h3>
								<ul>
									<li><strong>Lorem ipsum dolor sit amet,</strong> consectetur adipiscing elit. Cras mollis vulputate rhoncus. Ut auctor nulla ut ligula</li>

									<li><strong>Nulla facilisi. Aenean eu nisi diam.</strong> Cras non felis lobortis massa congue varius et et eros</li>

									<li><strong>Nunc sed dui commodo, condimentum nibh nec,</strong> sagittis est. Aliquam non orci vel lectus lacinia tempor. Mauris vitae ullamcorper ante. In euismod id nulla ut facilisis. Aliquam sollicitudin erat sed dignissim gravida. Ut tincidunt nibh vitae massa molestie vestibulum</li>
								</ul>
							</div>
						</div>
						<div className="row">
							<div className="col-12 shareCol">
								<VisibilitySensor onChange={this.onVisibilityChange6}>
									<div className="line6" id="line6">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100">
											<path className="lineDraw" fill="none" stroke="black" strokeWidth="0.4"
												d="M99 0 a11 11 0 0 1 -11 11 H12 a11 11 0 0 0 -11 11" />
										</svg>
									</div>
								</VisibilitySensor>
								<div className="stepBox add">
									<em className="gems-add stepIcon"></em>
									<div className="stepTxt">
										<span>Step 04</span>
									Add
								</div>
								</div>
								<VisibilitySensor onChange={this.onVisibilityChange7}>
									<div className="line7" id="line7">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100">
											<path className="lineDraw" fill="none" stroke="black" strokeWidth="0.4"
												d="M1 0 a11 11 0 0 0 11 11 H88 a11 11 0 0 1 11 11" />
										</svg>
									</div>
								</VisibilitySensor>
							</div>
						</div>
						<div className="row">
							<div className="col-12 col-lg-5 col-xl-4 shareTxt">
								<h3>Add and Share</h3>
								<ul>
									<li><strong>Lorem ipsum dolor sit amet,</strong> consectetur adipiscing elit. Cras mollis vulputate rhoncus. Ut auctor nulla ut ligula</li>

									<li><strong>Nulla facilisi. Aenean eu nisi diam.</strong> Cras non felis lobortis massa congue varius et et eros</li>

									<li><strong>Nunc sed dui commodo, condimentum nibh nec,</strong> sagittis est. Aliquam non orci vel lectus lacinia tempor. Mauris vitae ullamcorper ante. In euismod id nulla ut facilisis. Aliquam sollicitudin erat sed dignissim gravida. Ut tincidunt nibh vitae massa molestie vestibulum</li>
								</ul>
							</div>
							<div className="col-12 col-lg-7 col-xl-8 shareImg">
								<img src={share} alt="GEMS" />
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default HowItWorks;