import React, { Component } from 'react';
import { Link } from "react-router-dom";
import { fetchCmsList, checkCmsSlug } from '../../action/cmsAction';
import * as AppConst from '../../common/constants';
import appStore from '../../assets/images/app-store.png';
import playStore from '../../assets/images/play-store.png';

class Footer extends Component {
	state = {
		cmsList: [],
		cmsSlug: []
	}
	componentDidMount() {
		fetchCmsList().then(response => {
			const cmsList = response.data;
			this.setState({ cmsList: cmsList });
		});
		checkCmsSlug().then(response => {
			const cmsSlug = response.data;
			this.setState({ cmsSlug: cmsSlug });
		});
	}
	render() {
		//console.log('cmsSlug=====',this.state.cmsSlug,this.state.cmsSlug.indexOf("download-app"));
		return (

			<div>
				{localStorage.getItem('token') ?
					<React.Fragment>
						<div className="footerWrap providerFooter">
							<div className="container">
								<div className="row">
									<div className="col-12 text-center">
										<h4>Gems</h4>
										<p>Tiam fermentum pretium ipsum, non ullamcorper sem dignissim sed</p>
									</div>
								</div>
							</div>
						</div>
					</React.Fragment>
					:
					<React.Fragment>
						<div className="footerWrap">
							<div className="container">
								<div className="row">
									<div className="col-12 col-md-3 col-lg-3">
										<h4>Gems</h4>
										<p>Tiam fermentum pretium ipsum, non ullamcorper sem dignissim sed</p>
									</div>
									<div className="col-6 col-md-2 col-lg-2">
										<h5>For Company</h5>
										<ul>
											{(this.state.cmsSlug.indexOf("about") != -1) ?
												<li><Link to={AppConst.SERVERFOLDER + "/about"}>About</Link></li> : ''}
											{(this.state.cmsSlug.indexOf("download-app") != -1) ?
												<li><Link to={AppConst.SERVERFOLDER + "/download-app"}>Download App</Link></li> : ''}
											{(this.state.cmsSlug.indexOf("blog") != -1) ?
												<li><Link to={AppConst.SERVERFOLDER + "/blog"}>Blog</Link></li> : ''}
											{(this.state.cmsSlug.indexOf("contact") != -1) ?
												<li><Link to={AppConst.SERVERFOLDER + "/contact"}>Contact</Link></li> : ''}
											{(this.state.cmsSlug.indexOf("help") != -1) ?
												<li><Link to={AppConst.SERVERFOLDER + "/help"}>Help</Link></li> : ''}
											<li><Link to={AppConst.SERVERFOLDER + "/error"}>FAQ</Link></li>
										</ul>
									</div>
									<div className="col-6 col-md-2 col-lg-2">
										<h5>For Providers</h5>
										<ul>
											{(this.state.cmsSlug.indexOf("what-is-gems") != -1) ?
												<li><Link to={AppConst.SERVERFOLDER + "/what-is-gems"}>What is Gems</Link></li> : ''}
											<li><Link to={AppConst.SERVERFOLDER + "/how-it-works"}>How it Works</Link></li>
											<li><Link to={AppConst.SERVERFOLDER + "/claim-your-gem"}>Claim your gems</Link></li>
										</ul>
									</div>
									<div className="col-6 col-md-2 col-lg-2">
										<h5>For Users</h5>
										<ul>
											{(this.state.cmsSlug.indexOf("terms-and-condition") != -1) ?
												<li><Link to={AppConst.SERVERFOLDER + "/terms-and-condition"}>Term and Conditions</Link></li> : ''}
											{(this.state.cmsSlug.indexOf("privacy-policy") != -1) ?
												<li><Link to={AppConst.SERVERFOLDER + "/privacy-policy"}>Privacy Policy</Link></li> : ''}
											<li><Link to={AppConst.SERVERFOLDER + "/error"}>Popular gems</Link></li>
										</ul>
									</div>
									<div className="col-6 col-md-3 col-lg-3">
										<h5>Connect with us</h5>
										<div className="socialIcon">
											<Link to={AppConst.SERVERFOLDER + "/"}><i className="fa fa-twitter" aria-hidden="true"></i></Link>
											<Link to={AppConst.SERVERFOLDER + "/"}><i className="fa fa-facebook-official" aria-hidden="true"></i></Link>
											<Link to={AppConst.SERVERFOLDER + "/"}><i className="fa fa-instagram" aria-hidden="true"></i></Link>
											<Link to={AppConst.SERVERFOLDER + "/"}><i className="fa fa-youtube-play" aria-hidden="true"></i></Link>
										</div>
										<div className="appLinks">
											<Link to={AppConst.SERVERFOLDER + "/"}><img src={appStore} alt="App Store" /></Link>
											<Link to={AppConst.SERVERFOLDER + "/"}><img src={playStore} alt="Google Play" /></Link>
										</div>
									</div>
								</div>
							</div>
						</div>
					</React.Fragment>
				}
			</div>
		);
	}
}

export default Footer;