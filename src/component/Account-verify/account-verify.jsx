import React, { Component } from 'react';
import axios from 'axios';
import * as AppConst from '../../common/constants';

class AccountVerify extends Component {
    state = {
        message: '',
        success: null,
    }

    componentDidMount() {
        window.scrollTo(0, 0);
        axios.get(AppConst.APIURL + `/api/verifyAccount/` + this.props.match.params.token)
            .then(res => {
                this.setState({
                    message: res.data.message,
                    success: res.data.success
                })
            });
    }

    render() {
        return (
            <div>
                <div className="commonBanner">
                    <div className="container">
                        <div className="row">
                            <div className="col-12">

                            </div>
                        </div>
                    </div>
                </div>
                <div className="contentPan">
                    <div className="container">
                        <div className="row">
                            <div className="col-12">
                                <div className="errorPan">
                                    <div className={this.state.success ? 'errorCircle bg-success' : 'errorCircle bg-danger'}>
                                        {this.state.success ?
                                            <i className="fa fa-check text-light" aria-hidden="true"></i> :
                                            <i className="fa fa-close text-light" aria-hidden="true"></i>}
                                    </div>
                                    {this.state.success ? <React.Fragment>
                                        <h4>Awesome!</h4>
                                        <h5>{this.state.message}</h5></React.Fragment> :
                                        <h5 className="mt-4">{this.state.message}</h5>}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default AccountVerify;