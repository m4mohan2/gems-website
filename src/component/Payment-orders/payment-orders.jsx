import React, { Component, Fragment } from 'react';
import Moment from 'react-moment';
import LoadingSpinner from '../../common/LoadingSpinner/LoadingSpinner';
import { paymentOrders, fetchCardList, deleteCard, createCard, updateCard, fetchInvoice } from '../../action/cmsAction';
import * as AppConst from '../../common/constants';
import Pagination from 'react-js-pagination';
import { Button, Col, FormControl, FormGroup, Modal, Row, Image, Table } from 'react-bootstrap';
import { Field, Form, Formik } from 'formik';
import * as Yup from 'yup';
import PaymentIcon from 'react-payment-icons';
import valid from 'card-validator';
import SuccessIco from '../../assets/images/success-ico.png';
import axios from 'axios';
import { ToastContainer, toast } from 'react-toastify';

const initialValues = {
    card_number: '',
    cvc: '',
    expiry_date: '',
    name: ''
};

const addCardSchema = Yup.object().shape({
    card_number: Yup.string()
        .test('test-number', 'Credit/Debit Card number is invalid', value => valid.number(value).isValid)
        .required('Credit/Debit number is required'),
    cvc: Yup.string()
        .test('test-cvv-number', 'CVC is invalid', value => valid.cvv(value).isValid),
    expiry_date: Yup.string()
        .test('test-expiry_date', 'Expiration date is invalid', value => valid.expirationDate(value).isValid)
        .matches(/([0-9]{2})\/([0-9]{4})/, 'Not a valid expiration date. Example: MM/YYYY')
        .required('Expiration date is required'),
    name: Yup.string(),
});

const editCardSchema = Yup.object().shape({
    expiry_date: Yup.string()
        .test('test-expiry_date', 'Expiration date is invalid', value => valid.expirationDate(value).isValid)
        .matches(/([0-9]{2})\/([0-9]{4})/, 'Not a valid expiration date. Example: MM/YYYY')
        .required('Expiration date is required'),
    name: Yup.string().nullable(),
});

class PaymentOrders extends Component {
    state = {
        activePage: 1,
        totalCount: 0,
        itemPerPage: 10,
        page: 1,
        paymentOrdersList: [],
        fetchErrorMsg: null,
        loading: false,
        addCardModal: false,
        addErrorMessge: null,
        addCardLoader: false,
        cardData: '',
        disabled: false,
        editCardEnable: false,
        editCardLoader: false,
        confirmMessage: false,
        deleteCard: false,
        cardId: '',
        cardList: [],
        loadingCard: false,
        deleteCardLoader: false,
        cardMessage: '',
    }

    displayError = (e) => {
        let errorMessge = '';
        try {
            errorMessge = e.data.message ? e.data.message : e.data.error_description;
        } catch (e) {
            errorMessge = 'Access is denied!';
        }
        return errorMessge;
    }

    componentDidMount() {
        if (localStorage.getItem('token') === null) {
            this.props.history.push(AppConst.SERVERFOLDER);
        }
        window.scrollTo(0, 0);
        this.fetchPaymentOrders(this.state.itemPerPage, this.state.page);
        this.handleCardList();
    }

    fetchPaymentOrders = (itemPerPage, page) => {
        this.setState({ loading: true });
        paymentOrders(localStorage.getItem('user_id'), itemPerPage, page)
            .then(res => {
                this.setState({
                    paymentOrdersList: res.data.data.data,
                    totalCount: res.data.data.total,
                    loading: false
                });
            })
            .catch(e => {
                let errorMsg = this.displayError(e);
                this.setState({
                    fetchErrorMsg: errorMsg,
                    loading: false
                });
                setTimeout(() => {
                    this.setState({ fetchErrorMsg: null });
                }, 5000);
            });
    }

    handleCardList = () => {
        this.setState({ loadingCard: true });
        fetchCardList(localStorage.getItem('user_id'))
            .then(res => {
                res.data.success === false ?
                    this.setState({
                        cardMessage: res.data.message,
                        loadingCard: false
                    }) :
                    this.setState({
                        cardList: res.data.message.data,
                        loadingCard: false
                    });
            })
            .catch(e => {
                let errorMsg = this.displayError(e);
                this.setState({
                    fetchErrorMsg: errorMsg,
                    loadingCard: false
                });
                setTimeout(() => {
                    this.setState({ fetchErrorMsg: null });
                }, 5000);
            });
    }

    handleChangeItemPerPage = (e) => {
        this.setState({ itemPerPage: e.target.value },
            () => {
                this.fetchPaymentOrders(this.state.itemPerPage, this.state.page);
            });
    }

    handlePageChange = pageNumber => {
        this.setState({ activePage: pageNumber });
        this.fetchPaymentOrders(this.state.itemPerPage, pageNumber > 0 ? pageNumber : 0);
    };

    resetPagination = () => {
        this.setState({ activePage: 1 });
    }

    rePagination = () => {
        this.setState({ activePage: 0 });
    }

    handelAddCardModal = () => {
        this.setState({
            addCardModal: true,
        });
    }

    handelAddCardModalClose = () => {
        this.setState({
            addCardModal: false,
        });
    }

    handelviewEditModal = (data) => {
        this.setState({
            viewEditModal: true,
            cardData: data,
        });
    }

    handelEditModalClose = () => {
        this.setState({
            viewEditModal: false,
            disabled: false,
            editCardEnable: false,
        });
    }

    handleEditCard = () => {
        this.setState({
            editCardEnable: true,
            disabled: true
        });
    }

    handleConfirmationClose = () => {
        this.setState({
            confirmMessage: false,
            successMessage: null
        });
    }

    handleDeleteHide = () => {
        this.setState({
            deleteCard: false,
            deleteerrorMsg: null
        });
    }

    handelDeleteModal = (card_id) => {
        this.setState({
            deleteCard: true,
            cardId: card_id
        });
    }

    handleDeleteCardSubmit = (card_id) => {
        this.setState({ deleteCardLoader: true });
        deleteCard(card_id)
            .then(res => {
                this.setState({
                    successMessage: 'Your card has been deleted successfully.',
                    confirmMessage: true,
                    deleteCardLoader: false,
                });
                this.handleDeleteHide();
                this.handleCardList();
            })
            .catch(e => {
                let errorMsg = this.displayError(e);
                this.setState({
                    fetchErrorMsg: errorMsg,
                    deleteCardLoader: false,
                });
                setTimeout(() => {
                    this.setState({ fetchErrorMsg: null });
                }, 5000);
            });
    }

    handleEditCardSubmit = (values, { setSubmitting }) => {
        this.setState({
            editCardLoader: true,
        });
        const expiry_date = values.expiry_date.split('/');
        let newValue = {
            card_id: values.id,
            exp_month: expiry_date[0],
            exp_year: expiry_date[1],
            name: values.name
        };
        updateCard(newValue)
            .then(res => {
                setSubmitting(false);
                this.setState({
                    editCardLoader: false,
                    editCardEnable: false,
                    disabled: false,
                    confirmMessage: true,
                    successMessage: 'Your card details has been updated successfully.',
                    viewEditModal: false
                });
                this.handleCardList();
            }).catch(e => {
                let errorMsg = this.displayError(e);
                this.setState({
                    addCardLoader: false,
                    addErrorMessge: errorMsg,
                });
                setTimeout(() => {
                    this.setState({ addErrorMessge: null });
                }, 5000);
            });
    };

    handleAddCardSubmit = (values, { resetForm, setSubmitting }) => {
        this.setState({
            addCardLoader: true,
        });
        const expiry_date = values.expiry_date.split('/');
        let newValue = {
            number: values.card_number,
            exp_month: expiry_date[0],
            exp_year: expiry_date[1],
            cvc: values.cvv,
            name: values.name
        };
        createCard(newValue)
            .then(res => {
                resetForm({});
                setSubmitting(false);
                this.setState({
                    successMessage: 'Your card has been added successfully.',
                    confirmMessage: true,
                    addCardModal: false,
                    addCardLoader: false,
                });
                this.handleCardList();
            }).catch(e => {
                let errorMsg = this.displayError(e);
                this.setState({
                    addCardLoader: false,
                    addErrorMessge: errorMsg,
                });
                setTimeout(() => {
                    this.setState({ addErrorMessge: null });
                }, 5000);
            });
    };

    handlePDF = (id) => {
        fetchInvoice(id)
            .then(res => {
                res.data.success === true ?
                    window.location.href = res.data.message
                    : toast.error('Something went wrong!', {
                        position: "bottom-right",
                        autoClose: 5000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true,
                        progress: undefined,
                    });
            });
    }

    render() {
        const { disabled } = this.state;
        const expMonth = this.state.cardData && this.state.cardData.exp_month && this.state.cardData.exp_month.toString();
        const monthAppendZero = expMonth.length === 1 ? '0' + expMonth : expMonth;
        const expYear = this.state.cardData && this.state.cardData.exp_year && this.state.cardData.exp_year.toString();
        const values = {
            id: this.state.cardData && this.state.cardData.id,
            card_number: 'XXXX-XXXX-XXXX-'.concat(this.state.cardData && this.state.cardData.last4),
            card_type: this.state.cardData && this.state.cardData.brand,
            expiry_date: monthAppendZero + '/' + expYear,
            name: this.state.cardData && this.state.cardData.name,
        };
        const monthNames = ["January", "February", "March", "April", "May", "June",
            "July", "August", "September", "October", "November", "December"
        ];
        return (
            <div className="mb-5">
                <div className="claimBannerPan">
                    <div className="container">
                        <h2>Payment Details</h2>
                    </div>
                </div>
                <div className="contentPan pt-4">
                    <div className="container pt-4">
                        <Row className="mb-3">
                            <Col sm={8} md={8}>
                                <h4 className="mb-3">Card Details</h4>
                            </Col>
                            {this.state.cardMessage !== '' ? '' :
                                <Col sm={4} md={4}>
                                    <button className="btn blue float-right" onClick={() => this.handelAddCardModal()}>Add Card</button>
                                </Col>}
                        </Row>
                        <div className="row">
                            <div className="col-12">
                                <Row className="show-grid pt-1">
                                    <Col sm={12} md={12}>
                                        <div className="tableBox">
                                            <Table responsive hover>
                                                <thead className="theaderBg">
                                                    <tr>
                                                        <th className="text-center">Card type</th>
                                                        <th className="text-center">Card number</th>
                                                        <th className="text-center">Expires</th>
                                                        <th className="text-center">Cardholder name</th>
                                                        <th className="text-center">Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    {this.state.loadingCard ? (<tr><td colSpan={12}><LoadingSpinner /></td></tr>) :
                                                        this.state.cardList.length > 0 ?
                                                            (this.state.cardList.map(card => (
                                                                <tr key={card.id}>
                                                                    <td className="text-center">
                                                                        <PaymentIcon
                                                                            id={card.brand.toLowerCase()}
                                                                            style={{ width: 40 }}
                                                                            className="payment-icon"
                                                                        />
                                                                    </td>
                                                                    <td className="text-center">XXXX-XXXX-XXXX-{card.last4}</td>
                                                                    <td className="text-center">{monthNames[card.exp_month - 1]} {card.exp_year}</td>
                                                                    <td className="text-center">{card.name ? card.name : '-'}</td>
                                                                    <td className="text-center">
                                                                        <i className="fa fa-eye" style={{ cursor: 'pointer' }} aria-hidden="true" title="View" onClick={() => this.handelviewEditModal(card)}></i>
                                                                        &nbsp;
                                                                        <i className="fa fa-trash-o" style={{ cursor: 'pointer' }} aria-hidden="true" title="Delete" onClick={() => this.handelDeleteModal(card.id)}></i>
                                                                    </td>
                                                                </tr>
                                                            )))
                                                            : this.state.fetchErrorMsg ? null :
                                                                this.state.cardMessage !== '' ?
                                                                    (<tr>
                                                                        <td colSpan={12}>
                                                                            <p className="text-center">{this.state.cardMessage}</p>
                                                                        </td>
                                                                    </tr>) :
                                                                    (<tr>
                                                                        <td colSpan={12}>
                                                                            <p className="text-center">No records found</p>
                                                                        </td>
                                                                    </tr>)
                                                    }
                                                </tbody>
                                            </Table>
                                        </div>
                                    </Col>
                                </Row>
                            </div>
                        </div>
                        <hr className="mt-5 mb-3" style={{ border: '2px solid gray' }} />
                        <h4 className="mb-3 mt-5">Payment Orders</h4>
                        <div className="row">
                            <div className="col-12">
                                <Row className="show-grid pt-1">
                                    <Col sm={12} md={12}>
                                        <div className="tableBox">
                                            <Table responsive hover>
                                                <thead className="theaderBg">
                                                    <tr>
                                                        <th className="text-center">Plan Amount</th>
                                                        <th className="text-center">Plan Amount Currency</th>
                                                        <th className="text-center">Plan Interval</th>
                                                        <th className="text-center">Plan Interval Count</th>
                                                        <th>Plan Period Start</th>
                                                        <th>Plan Period End</th>
                                                        <th>Status</th>
                                                        <th className="text-center">Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    {this.state.loading ? (<tr><td colSpan={12}><LoadingSpinner /></td></tr>) :
                                                        this.state.paymentOrdersList.length > 0 ?
                                                            (this.state.paymentOrdersList.map(payment => (
                                                                <tr key={payment.id}>
                                                                    <td className="text-center">{payment.plan_amount}</td>
                                                                    <td className="text-center">{payment.plan_amount_currency.toUpperCase()}</td>
                                                                    <td className="text-center">{payment.plan_interval.toUpperCase()}</td>
                                                                    <td className="text-center">{payment.plan_interval_count}</td>
                                                                    <td><Moment format="MMM Do, YYYY">{payment.plan_period_start}</Moment></td>
                                                                    <td><Moment format="MMM Do, YYYY">{payment.plan_period_end}</Moment></td>
                                                                    <td>{payment.status.toUpperCase()}</td>
                                                                    <td className="text-center">
                                                                        <i className="fa fa-file-pdf-o" style={{ cursor: 'pointer' }} onClick={e => this.handlePDF(payment.id)} aria-hidden="true" title="Download Invoice"></i>
                                                                    </td>
                                                                </tr>
                                                            )))
                                                            : this.state.fetchErrorMsg ? null : (<tr>
                                                                <td colSpan={12}>
                                                                    <p className="text-center">No records found</p>
                                                                </td>
                                                            </tr>)
                                                    }
                                                </tbody>
                                            </Table>
                                        </div>
                                    </Col>
                                </Row>

                                {this.state.totalCount ? (
                                    <Row className="paginationPan">
                                        <Col md={6} sm={12} className="paginationSelect">
                                            <span>Items per page</span>
                                            <select
                                                id={this.state.itemPerPage}
                                                className="form-control truncatefloat-left w-90"
                                                onChange={this.handleChangeItemPerPage}
                                                value={this.state.itemPerPage}>
                                                <option value='10'>10</option>
                                                <option value='50'>50</option>
                                                <option value='100'>100</option>
                                                <option value='150'>150</option>
                                                <option value='200'>200</option>
                                                <option value='250'>250</option>
                                            </select>
                                        </Col>
                                        <Col md={6} sm={12} className="text-right">
                                            <div className="">
                                                <Pagination
                                                    activePage={this.state.activePage}
                                                    itemsCountPerPage={this.state.itemPerPage}
                                                    totalItemsCount={this.state.totalCount}
                                                    onChange={this.handlePageChange}
                                                />
                                            </div>
                                        </Col>
                                    </Row>
                                ) : null}
                            </div>
                            <ToastContainer />
                        </div>

                        {/* Add Card Modal */}
                        <Modal show={this.state.addCardModal}
                            onHide={this.handelAddCardModalClose}
                            size="lg"
                        >
                            <Modal.Header closeButton>
                                <div className="modal-title">Add a card</div>
                            </Modal.Header>
                            <Modal.Body className="couponDetails">
                                {this.state.addErrorMessge ? (
                                    <div className="alert alert-danger" role="alert">
                                        {this.state.addErrorMessge}
                                    </div>
                                ) : null}
                                <Row>
                                    <Col sm={12}>
                                        {this.state.addCardLoader ? <LoadingSpinner /> : null}
                                    </Col>
                                </Row>
                                <Row className="show-grid">
                                    <Col xs={12} className="brd-right">
                                        <Formik
                                            initialValues={initialValues}
                                            validationSchema={addCardSchema}
                                            onSubmit={this.handleAddCardSubmit}
                                        >
                                            {({
                                                values,
                                                errors,
                                                touched,
                                                isSubmitting,
                                            }) => {
                                                return (
                                                    <Form>
                                                        <Row className="show-grid">
                                                            <Col xs={11} md={11}>
                                                                <FormGroup controlId="formControlsTextarea">
                                                                    <label>Card number<span className="required">*</span></label>
                                                                    <Field
                                                                        name="card_number"
                                                                        type="number"
                                                                        className={'form-control'}
                                                                        autoComplete="nope"
                                                                        value={values.card_number || ''}
                                                                    />
                                                                    {errors.card_number && touched.card_number ? (
                                                                        <span className="errorMsg">{errors.card_number}</span>
                                                                    ) : null}
                                                                    <FormControl.Feedback />
                                                                </FormGroup>
                                                            </Col>
                                                            <Col xs={1} md={1}>
                                                                {values.card_number &&
                                                                    values.card_number.toString().length >= 4 &&
                                                                    valid.number(values.card_number).card != null ?
                                                                    <PaymentIcon
                                                                        id={valid.number(values.card_number).card.type}
                                                                        style={{ width: 40 }}
                                                                        className="payment-icon errorMsg mt-2"
                                                                    />
                                                                    : ''}
                                                            </Col>

                                                            <Col xs={6} md={6}>
                                                                <FormGroup controlId="formControlsTextarea">
                                                                    <label>Expiry date<span className="required">*</span></label>
                                                                    <Field
                                                                        name="expiry_date"
                                                                        type="text"
                                                                        className={'form-control'}
                                                                        autoComplete="nope"
                                                                        placeholder="MM/YYYY"
                                                                        value={values.expiry_date || ''}
                                                                    />
                                                                    {errors.expiry_date && touched.expiry_date ? (
                                                                        <span className="errorMsg">{errors.expiry_date}</span>
                                                                    ) : null}
                                                                    <FormControl.Feedback />
                                                                </FormGroup>
                                                            </Col>

                                                            <Col xs={6} md={6}>
                                                                <FormGroup controlId="formControlsTextarea">
                                                                    <label>CVC<span className="required">*</span></label>
                                                                    <Field
                                                                        name="cvc"
                                                                        type="password"
                                                                        className={'form-control'}
                                                                        autoComplete="nope"
                                                                        value={values.cvc || ''}
                                                                    />
                                                                    {errors.cvc && touched.cvc ? (
                                                                        <span className="errorMsg">{errors.cvc}</span>
                                                                    ) : null}
                                                                    <FormControl.Feedback />
                                                                </FormGroup>
                                                            </Col>

                                                            <Col xs={12} md={12}>
                                                                <FormGroup controlId="formControlsTextarea">
                                                                    <label>Cardholder name</label>
                                                                    <Field
                                                                        name="name"
                                                                        type="text"
                                                                        className={'form-control'}
                                                                        autoComplete="nope"
                                                                        value={values.name || ''}
                                                                    />
                                                                    {errors.name && touched.name ? (
                                                                        <span className="errorMsg">{errors.name}</span>
                                                                    ) : null}
                                                                    <FormControl.Feedback />
                                                                </FormGroup>
                                                            </Col>

                                                        </Row>
                                                        <Row className="show-grid">
                                                            <Col xs={12} md={12} className="text-center">
                                                                <Button className="" type="submit" disabled={isSubmitting}>Add card</Button>
                                                            </Col>
                                                        </Row>
                                                        <Row>
                                                            <Col md={12}>
                                                                <p style={{ paddingTop: '10px' }}><span className="required">*</span> These fields are required.</p>
                                                            </Col>
                                                        </Row>
                                                    </Form>
                                                );
                                            }}
                                        </Formik>
                                    </Col>
                                </Row>
                            </Modal.Body>
                        </Modal>

                        {/* Edit Card Modal */}
                        <Modal
                            show={this.state.viewEditModal}
                            onHide={this.handelEditModalClose}
                            size="lg"
                        >
                            <Modal.Header closeButton>
                                <div className="modal-title">{this.state.editCardEnable !== true ? 'View' : 'Edit'} Card</div>
                            </Modal.Header>
                            <Modal.Body className="couponDetails">
                                <Row>
                                    <Col sm={12}>
                                        {this.state.editCardLoader ? <LoadingSpinner /> : null}
                                    </Col>
                                </Row>
                                <Row className="show-grid">
                                    <Col xs={12} className="brd-right">
                                        <Formik
                                            initialValues={values}
                                            validationSchema={editCardSchema}
                                            onSubmit={this.handleEditCardSubmit}
                                            enableReinitialize={true} >
                                            {({
                                                values,
                                                errors,
                                                touched,
                                                isSubmitting,
                                            }) => {
                                                return (
                                                    <Form className={disabled === false ? ('hideRequired') : null}>
                                                        <Row className="show-grid">
                                                            <Col xs={11} md={11}>
                                                                <FormGroup controlId="formControlsTextarea">
                                                                    <label>Card number<span className="required">*</span></label>
                                                                    <Field
                                                                        name="card_number"
                                                                        type="text"
                                                                        className={'form-control'}
                                                                        autoComplete="nope"
                                                                        value={values.card_number || ''}
                                                                        disabled={'disabled'}
                                                                    />
                                                                    {errors.card_number && touched.card_number ? (
                                                                        <span className="errorMsg">{errors.card_number}</span>
                                                                    ) : null}
                                                                    <FormControl.Feedback />
                                                                </FormGroup>
                                                            </Col>
                                                            <Col xs={1} md={1}>
                                                                {<PaymentIcon
                                                                    id={values.card_type.toLowerCase()}
                                                                    style={{ width: 40 }}
                                                                    className="payment-icon errorMsg mt-2"
                                                                />}
                                                            </Col>

                                                            <Col xs={6} md={6}>
                                                                <FormGroup controlId="formControlsTextarea">
                                                                    <label>Expiry date<span className="required">*</span></label>
                                                                    <Field
                                                                        name="expiry_date"
                                                                        type="text"
                                                                        className={'form-control'}
                                                                        autoComplete="nope"
                                                                        placeholder="MM/YYYY"
                                                                        value={values.expiry_date || ''}
                                                                        disabled={disabled === false ? 'disabled' : ''}
                                                                    />
                                                                    {errors.expiry_date && touched.expiry_date ? (
                                                                        <span className="errorMsg">{errors.expiry_date}</span>
                                                                    ) : null}
                                                                    <FormControl.Feedback />
                                                                </FormGroup>
                                                            </Col>

                                                            <Col xs={12} md={12}>
                                                                <FormGroup controlId="formControlsTextarea">
                                                                    <label>Cardholder name</label>
                                                                    <Field
                                                                        name="name"
                                                                        type="text"
                                                                        className={'form-control'}
                                                                        autoComplete="nope"
                                                                        value={values.name || ''}
                                                                        disabled={disabled === false ? 'disabled' : ''}
                                                                    />
                                                                    {errors.name && touched.name ? (
                                                                        <span className="errorMsg">{errors.name}</span>
                                                                    ) : null}
                                                                    <FormControl.Feedback />
                                                                </FormGroup>
                                                            </Col>
                                                        </Row>

                                                        <Row className="show-grid">
                                                            <Col xs={12} md={12}>&nbsp;</Col>
                                                        </Row>
                                                        <Row>&nbsp;</Row>
                                                        <Row className="show-grid text-center">
                                                            <Col xs={12} md={12}>
                                                                <Fragment>
                                                                    {this.state.editCardEnable !== true ? (
                                                                        <Fragment>
                                                                            <Button className="blue-btn border-0" onClick={this.handleEditCard}>
                                                                                Edit card
                                                                            </Button>
                                                                        </Fragment>) :
                                                                        (<Fragment>
                                                                            <Button
                                                                                onClick={this.handelEditModalClose}
                                                                                className="but-gray border-0 mr-2">
                                                                                Cancel
                                                                            </Button>
                                                                            <Button type="submit" className="blue-btn ml-2 border-0" disabled={isSubmitting}>
                                                                                Update card
                                                                            </Button>
                                                                        </Fragment>)
                                                                    }
                                                                </Fragment>
                                                            </Col>
                                                        </Row>
                                                        {disabled === false ? null : (<Fragment>
                                                            <Row>
                                                                <Col md={12}>
                                                                    <p style={{ paddingTop: '10px' }}><span className="required">*</span> These fields are required.</p>
                                                                </Col>
                                                            </Row>
                                                        </Fragment>)}
                                                    </Form>
                                                );
                                            }}
                                        </Formik>
                                    </Col>
                                </Row>
                            </Modal.Body>
                        </Modal>

                        {/*====== confirmation popup  ===== */}
                        <Modal
                            show={this.state.confirmMessage}
                            onHide={this.handleConfirmationClose}
                            className="payOptionPop"
                        >
                            <Modal.Body className="text-center">
                                <Row>
                                    <Col md={12} className="text-center">
                                        <Image src={SuccessIco} />
                                    </Col>
                                </Row>
                                <Row>
                                    <Col md={12} className="text-center">
                                        <h5>{this.state.successMessage}</h5>
                                    </Col>
                                </Row>
                                <Button
                                    onClick={this.handleConfirmationClose}
                                    className=""
                                >
                                    Return
                                    </Button>
                            </Modal.Body>
                        </Modal>

                        {/*========================= Modal for Delete Card =====================*/}
                        <Modal
                            show={this.state.deleteCard}
                            onHide={this.handleDeleteHide}
                            size="md"
                        >
                            <Modal.Body>
                                <Row>
                                    <Col sm={12} className='mb-3'>
                                        {this.state.deleteCardLoader ? <LoadingSpinner /> : null}
                                    </Col>
                                </Row>
                                <div className="m-auto text-center">
                                    <h6 className="confirmTxt">Do you want to delete this Card?</h6>
                                </div>
                                {this.state.deleteerrorMsg ? <div className="alert alert-danger my-3 text-center col-12" role="alert">{this.state.deleteerrorMsg}</div> : null}
                                <div className="m-auto text-center">
                                    <button className="btn mr-3 small" onClick={() => this.handleDeleteHide()}>Return</button>
                                    {this.state.deleteerrorMsg == null ? <button className="btn small danger" onClick={() => this.handleDeleteCardSubmit(this.state.cardId)}>Confirm</button> : null}
                                </div>
                            </Modal.Body>
                        </Modal>

                    </div>
                </div>
            </div >
        );
    }
}

export default PaymentOrders;