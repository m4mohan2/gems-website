import React, { Component } from 'react';
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

import productImg1 from '../../assets/images/product-1.jpg';
import productImg2 from '../../assets/images/product-2.jpg';
import productImg3 from '../../assets/images/product-3.jpg';

class GemDetails extends Component {

	componentDidMount() {
		window.scrollTo(0, 0)
	}

	state = {}
	render() {
		var settings = {
			dots: true,
			infinite: true,
			speed: 500,
			slidesToShow: 1,
			slidesToScroll: 1,
		};


		return (
			<div className="gemDetailsPan">
				<div className="commonBanner">
					<div className="container">
						<div className="row ">
							<div className="col-12">

							</div>
						</div>
					</div>
				</div>
				<div className="gemsDetailsContentPan">
					<div className="container">
						<div className="row">
							<div className="col-6">
								<div className="gemsDetailSlider">
									<Slider {...settings}>
										<div className="slideItem">
											<img src={productImg1} alt="GEMS" />
										</div>
										<div className="slideItem">
											<img src={productImg2} alt="GEMS" />
										</div>
										<div className="slideItem">
											<img src={productImg3} alt="GEMS" />
										</div>
									</Slider>
								</div>
							</div>
							<div className="col-6">
								<h3>Gems Details</h3>
								<div className="starPan">
									<i className="fa fa-star active" aria-hidden="true"></i>
									<i className="fa fa-star active" aria-hidden="true"></i>
									<i className="fa fa-star active" aria-hidden="true"></i>
									<i className="fa fa-star" aria-hidden="true"></i>
									<i className="fa fa-star" aria-hidden="true"></i>
									<span> 41 Likes </span>
								</div>
								<p>Suspendisse quos? Tempus cras iure temporibus? Eu laudantium cubilia sem sem! Repudiandae et! Massa senectus enim minim sociosqu delectus posuere. Tempus cras iure temporibus? Eu laudantium cubilia sem sem! Repudiandae et! Massa senectus enim minim sociosqu delectus posuere.</p>
								<p>Suspendisse quos? Tempus cras iure temporibus? Eu laudantium cubilia sem sem! Repudiandae et! Massa senect Tempus cras iure temporibus? Eu laudantium cubilia sem sem! Repudiandae et! Massa senectus enim minim sociosqu delectus posuere us enim minim sociosqu delectus posuere.</p>
								<p>Suspendisse quos? Tempus cras iure temporibus? Eu laudantium cubilia sem sem! Repudiandae et! Massa senect Tempus cras iure temporibus? Eu laudantium cubilia sem sem! Repudiandae et! Massa senectus enim minim sociosqu delectus posuere us enim minim sociosqu delectus posuere.</p>
								<div className="addressPan"><i class="fa fa-map-marker" aria-hidden="true"></i>
									<p>ure temporibus? Eu laudantium cubilia sem sem! <br />
								Eu laudantium cubilia se<br />
								dantium cubilia</p></div>
							</div>
						</div>
					</div>
				</div>



			</div>
		);
	}
}

export default GemDetails;