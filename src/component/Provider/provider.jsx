import React, { Component } from 'react';
import { Button, Modal, Carousel } from 'react-bootstrap'
import img1 from '../../assets/images/img1.jpg';
import img2 from '../../assets/images/img2.jpg';
import { connect } from 'react-redux';
import { Map, GoogleApiWrapper, InfoWindow, Marker } from 'google-maps-react';
import iconGems from '../../assets/images/icon-gems.png';
import iconGemsOther from '../../assets/images/gems-loader.gif';
import { Link } from 'react-router-dom';
import LoadingSpinner from '../../common/LoadingSpinner/LoadingSpinner';
import { providerGemList } from '../../action/cmsAction';
import Moment from 'react-moment';
import * as AppConst from '../../common/constants';

class GemsProvider extends Component {
	constructor() {
		super();
		this.searchScrollRef = React.createRef();
		this.state = {
			showHide: false,
			showFromSearchOnMap: false,
			latitudeForSearchResultClick: '',
			longitudeForSearchResultClick: '',
			gemDetail: '',
			gemsList: [],
			showingInfoWindow: false,
			activeMarker: {},
			selectedPlace: {},
			totalCount: 0,
			pageSize: 10,
			page: 1,
			gemNameForSearchAndScroll: '',
			addressForSearchAndScroll: '',
			from: 0,
			to: 0,
			current_page: 0,
			last_page: 0,
			showLoadingSpinnerForSearch: false,
			name: '',
			gemsdata: '',
			searchKey: ''
		}
	}

	componentDidMount() {
		window.scrollTo(0, 0);
		if (localStorage.getItem('token') === null) {
			this.props.history.push(AppConst.SERVERFOLDER);
		}
		this.fetchGemList(this.state.pageSize, this.state.page, this.state.searchKey);
	}

	fetchGemList = (pageSize, page, searchKey) => {
		providerGemList(localStorage.getItem('user_id'), pageSize, page, searchKey)
			.then(response => {
				this.setState({
					gemsList: response.data.gems_list.data,
					gemDetail: response.data.gems_list.data[0],
					totalCount: response.data.gems_list.total,
					current_page: response.data.gems_list.current_page,
					last_page: response.data.gems_list.last_page,
					to: response.data.gems_list.to,
					from: response.data.gems_list.from,
					showLoadingSpinnerForSearch: false,
					preventScrolling: false,
					showFromSearchOnMap: false
				});
			});
	}

	componentDidUpdate() {
		if (localStorage.getItem('token') === null) {
			this.props.history.push(AppConst.SERVERFOLDER);
		}
	}

	handleModalShowHide(data) {
		this.setState({ showHide: !this.state.showHide, gemsdata: data })
	}

	handleHide = () => {
		this.setState({ showHide: false });
	}

	displayMarkers() {
		return this.state.gemsList.map((place, index) => {
			return <Marker
				key={index}
				id={index}
				onClick={this.onMarkerClick}
				name={this.state.showFromSearchOnMap ? this.state.name : place.gems.name}
				position={{
					lat: this.state.showFromSearchOnMap ? this.state.latitudeForSearchResultClick : place.gems.lat,
					lng: this.state.showFromSearchOnMap ? this.state.longitudeForSearchResultClick : place.gems.lng
				}}
				icon={{
					url: this.state.showFromSearchOnMap ? iconGemsOther : iconGems,
					anchor: new window.google.maps.Point(32, 32),
					scaledSize: new window.google.maps.Size(40, 40)
				}}
			/>
		})
	}

	onMarkerClick = (props, marker, e) => {
		this.setState({
			selectedPlace: props,
			activeMarker: marker,
			showingInfoWindow: true
		});
	}

	onClose = props => {
		if (this.state.showingInfoWindow) {
			this.setState({
				showingInfoWindow: false,
				activeMarker: null
			});
		}
	};

	handlePageChange = pageNumber => {
		this.setState({ showLoadingSpinnerForSearch: true, preventScrolling: true });
		this.searchScrollRef.current.scrollTo(0, 0);
		if (pageNumber === 'prev') {
			if (this.state.current_page !== 1) {
				const prevpage = this.state.current_page - 1;
				this.fetchGemList(this.state.pageSize, prevpage, this.state.searchKey);
			} else {
				this.setState({ showLoadingSpinnerForSearch: false, preventScrolling: false });
			}
		} else if (pageNumber === 'next') {
			if (this.state.current_page === this.state.last_page) {
				this.setState({ showLoadingSpinnerForSearch: false, preventScrolling: false });
			} else {
				const prevpage = this.state.current_page + 1;
				this.fetchGemList(this.state.pageSize, prevpage, this.state.searchKey);
			}
		}
	};

	handleSeacrchOnmouseUp = (lat, lng, name) => {
		this.setState({
			latitudeForSearchResultClick: lat,
			longitudeForSearchResultClick: lng,
			name: name,
			showFromSearchOnMap: true,
		});
	}

	handleClick(a) {
		a.preventDefault();
		document.body.classList.toggle('searchResultOpen');
	}

	render() {
		return (
			<div className="claimSearchPage">
				<div className="claimBannerPan">
					<div className="container">
						<h2>My Gem</h2>
					</div>
				</div>
				<div className="claimBodyPan py-0">
					<div className="container-fluid">

						<div className="mapRow">
							<div className="searchLeftPan">
								<div className="searchHead">
									<h3>My Gem</h3>
									<p>Praesent sit amet laoreet mi, sed ornare velit.</p>

									<div className="paginationWrap">
										<p className="paginationTxt">{this.state.from}-{this.state.to} of {this.state.totalCount}</p>
										{this.state.totalCount > 10 ?
											<Link className={this.state.current_page === 1 ? 'paginationBtn disable' : 'paginationBtn active'}
												style={{ cursor: this.state.current_page === 1 ? '' : 'pointer' }}
												onClick={() => this.handlePageChange('prev')}>
												<i className="fa fa-angle-left"></i>
											</Link>
											: ''}
										{this.state.totalCount > 10 ?
											<Link className={this.state.current_page === this.state.last_page ? 'paginationBtn disable' : 'paginationBtn active'}
												style={{ cursor: this.state.current_page === this.state.last_page ? '' : 'pointer' }}
												onClick={() => this.handlePageChange('next')}>
												<i className="fa fa-angle-right"></i>
											</Link>
											: ''}
									</div>

									{this.state.showLoadingSpinnerForSearch ? <LoadingSpinner /> : ''}
								</div>
								<div className="searchContent" style={{ overflowY: this.state.preventScrolling ? 'hidden' : 'auto' }} ref={this.searchScrollRef}>
									<ul>
										{this.state.gemsList.map((data, index) => {
											return <li key={index} onClick={() => this.handleSeacrchOnmouseUp(data.gems.lat, data.gems.lng, data.gems.name)} style={{ cursor: 'pointer' }}>
												<div className="searchImgPan">
													{data.gems_images === undefined || data.gems_images.length === 0 ? <h5>No Image</h5> :
														<div className="searchImgWrap">
															{data.gems_images.map((img, index) => {
																return index === 0 ?
																	img.source === 'google_link' ?
																		<img key={index} src={img.image} alt="GEMS" /> :
																		<img key={index} src={'http://54.147.235.207/gems_uat/service/public/upload/gems/' + img.image} alt="GEMS" /> : ''
															})}
														</div>}
												</div>
												<div className="searchConentPan">
													<h4 onClick={() => { this.handleModalShowHide(data); }}>{data.gems.name}</h4>
													<div className="likesPan">
														<p>Likes <strong>{data.gems_like_count ? data.gems_like_count : '0'}</strong></p>
													</div>
													<p>{data.gems.address}</p>
													{data.action === 0 ?
														(<span className="btn" title="Pending">Pending</span>) :
														data.action === 1 ?
															(<span className="btn" title="Already claimed">Already claimed</span>) :
															(<span className="btn claimed">Rejected</span>
															)
													}
												</div>
											</li>
										})}
									</ul>
								</div>
								<div className="searchFoot">
									<div className="paginationWrap">
										<p className="paginationTxt">{this.state.from}-{this.state.to} of {this.state.totalCount}</p>
										{this.state.totalCount > 10 ?
											<Link className={this.state.current_page === 1 ? 'paginationBtn disable' : 'paginationBtn active'}
												style={{ cursor: this.state.current_page === 1 ? '' : 'pointer' }}
												onClick={() => this.handlePageChange('prev')}>
												<i className="fa fa-angle-left"></i>
											</Link>
											: ''}
										{this.state.totalCount > 10 ?
											<Link className={this.state.current_page === this.state.last_page ? 'paginationBtn disable' : 'paginationBtn active'}
												style={{ cursor: this.state.current_page === this.state.last_page ? '' : 'pointer' }}
												onClick={() => this.handlePageChange('next')}>
												<i className="fa fa-angle-right"></i>
											</Link>
											: ''}
									</div>
								</div>
							</div>
							<div className="mapPan">
								<span className="arrow" onClick={a => this.handleClick(a)}>
									<i className="fa fa-arrow-right" aria-hidden="true"></i>
									<i className="fa fa-arrow-left" aria-hidden="true"></i>
								</span>
								<div className="mapWrap">
									<Map
										google={this.props.google}
										zoom={this.state.showFromSearchOnMap ? 20 : 14}
										center={{
											lat: this.state.showFromSearchOnMap ? this.state.latitudeForSearchResultClick : this.state.gemDetail && this.state.gemDetail.gems.lat,
											lng: this.state.showFromSearchOnMap ? this.state.longitudeForSearchResultClick : this.state.gemDetail && this.state.gemDetail.gems.lng
										}}
										disableDoubleClickZoom={false}
										scrollwheel={false}
									>
										{this.displayMarkers()}
										<InfoWindow
											marker={this.state.activeMarker}
											visible={this.state.showingInfoWindow}
											onClose={this.onClose}
										>
											{this.state.gemsList.map((data, index) => {
												return data.gems.name === this.state.selectedPlace.name &&
													<div className="container-fluid" key={index}>
														<div className="row">
															<div className="col-md-7">
																<h6>{this.state.selectedPlace.name}</h6>
															</div>
															{/* <div className="col-md-5 text-right">
																	Likes <strong className="text-primary bolder">{data.total_like_count ? data.total_like_count : '0'}</strong>
																</div> */}
														</div>
														<p className="">{data.gems.address}</p>
														{/* <a className="" style={{ cursor: 'pointer' }} onMouseOver={this.handleclick}>Claim this Company</a> */}
													</div>
											})}
										</InfoWindow>
									</Map>

									{/* <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d471219.7256039201!2d88.32972875!3d22.6759958!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sin!4v1594807603482!5m2!1sen!2sin"></iframe> */}
								</div>
							</div>
						</div>


						<Modal size="lg" show={this.state.showHide} onHide={this.handleHide}>
							<Modal.Header closeButton onClick={() => this.handleModalShowHide()}>
								<Modal.Title>{this.state.gemsdata && this.state.gemsdata.gems.name}</Modal.Title>
							</Modal.Header>
							{this.state.gemsdata &&
								<Modal.Body className="providerDetails">
									<div className="row">
										<div className="col-6 text-center">
											<Carousel>
												{this.state.gemsdata.gems_images.length !== 0 ? this.state.gemsdata.gems_images.map(img => {
													return img.source === 'image_file' ?
														<Carousel.Item key={img.id}>
															<img key={img.id} src={'http://54.147.235.207/gems_uat/service/public/upload/gems/' + img.image} alt="GEMS" />
														</Carousel.Item> :
														<Carousel.Item key={img.id}>
															<img key={img.id} src={img.image} alt="GEMS" />
														</Carousel.Item>
												}) :
													<Carousel.Item>
														<img src={img1} alt="GEMS" />
													</Carousel.Item>
												}
											</Carousel>
										</div>
										<div className="col-6">
											<div className="tagList">
												<ul>
													{this.state.gemsdata.gems_category_detail.map(data => {
														return <li key={data.id}>{data.category_name}</li>;
													})}
													{/* <li>Lodging</li>
													<li>Park</li>
													<li>Tourist Attraction</li>
													<li>Shopping Mall</li>
													<li>Restaurant</li>
													<li>Amusement Park</li>
													<li>Cafe Food</li> */}
												</ul>
											</div>
											<div className="providerTxt">
												<p className="" style={{ wordWrap: 'break-word' }}>{this.state.gemsdata.gems.note}</p>
											</div>
											<div className="addressPan mt-4">
												<ul>
													<li>
														<i className="fa fa-users" aria-hidden="true"></i>Gem Name :  {this.state.gemsdata.gems.name}
													</li>
													<li>
														<i className="fa fa-phone" aria-hidden="true"></i>Phone No :  +919477581597
													</li>
													<li>
														<i className="fa fa-map-marker" aria-hidden="true"></i>Address : {this.state.gemsdata.gems.address}
													</li>
													<li>
														<i className="fa fa-user" aria-hidden="true"></i>Created By :  {this.state.gemsdata.gems_created_by.first_name} {this.state.gemsdata.gems_created_by.last_name}
													</li>
													{this.state.gemsdata && this.state.gemsdata.action === 1 ?
														<li>
															<i className="fa fa-user" aria-hidden="true"></i>Owned By :
															{this.state.gemsdata.provider_details.first_name}&nbsp;
															{this.state.gemsdata.provider_details.last_name}
														</li>
														: ''}
													<li>
														<i className="fa fa-calendar" aria-hidden="true"></i>Created On : <Moment format="MMMM Do, YYYY">{this.state.gemsdata.gems.created_at}</Moment>
													</li>
													<li>
														<i className="fa fa-check" aria-hidden="true"></i>Status : {this.state.gemsdata.gems.status === 1 ? 'Active' : 'Inactive'}
													</li>
													<li>
														<i className="fa fa-thumbs-up" aria-hidden="true"></i>Total Likes : {this.state.gemsdata.gems_like_count}
													</li>
												</ul>
											</div>
										</div>
									</div>

								</Modal.Body>}
						</Modal>

					</div>
				</div >

			</div >
		);
	}
}

const mapStateToProps = state => {
	return {
		token: state.loginDetails.token

	};
};
export default GoogleApiWrapper({
	apiKey: 'AIzaSyD_BBiV2jnDTjEMxjvFncobeQ3RY0wUP-Y'
})(connect(mapStateToProps, null)(GemsProvider));