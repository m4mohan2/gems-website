import React, { Component } from 'react';
import { fetchCmsDetails } from '../../action/cmsAction';
import ReactHtmlParser from 'react-html-parser';
import { Link } from 'react-router-dom';

class Blog extends Component {
	state = {
		cmsDetails: []
	}
	componentDidMount() {
		window.scrollTo(0, 0)
		fetchCmsDetails('blog').then(response => {
			const cmsDetails = response.data.message;
			this.setState({ cmsDetails: cmsDetails });
		});
	}
	
	render() {
		return (
			<div>
				<div className="commonBanner">
					<div className="container">
						<div className="row">
							<div className="col-12">

							</div>
						</div>
					</div>
				</div>
				<div className="contentPan">
					<div className="container">
						<div className="row">
							<div className="col-12">
								{(this.state.cmsDetails)?	
									<div class="cms">						
										<h3>{this.state.cmsDetails.title}</h3>
										{ReactHtmlParser(this.state.cmsDetails.description)}
									</div>
								: 
								<div className="errorPan">
									<div className="errorCircle">
										404
									</div>
									<h4>Page not found</h4>
									<p>Maybe this page moved, got deleted or does not exist.<br />
									Let's back to <Link to="/">home</Link>.</p>
								</div>
								}
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default Blog;