import React, { Component } from 'react';
import { fetchCmsDetails } from '../../action/cmsAction'
import ReactHtmlParser from 'react-html-parser';

class About extends Component {
	state = {
		cmsDetails: []
	}
	componentDidMount() {
		window.scrollTo(0, 0)
		fetchCmsDetails('about').then(response => {
			const cmsDetails = response.data.message;
			this.setState({ cmsDetails: cmsDetails });
		});
	}
	
	render() {
		return (
			<div>
				<div className="commonBanner">
					<div className="container">
						<div className="row">
							<div className="col-12">

							</div>
						</div>
					</div>
				</div>
				<div className="contentPan">
					<div className="container">
						<div className="row">
							<div className="col-12">								
								<h3>{this.state.cmsDetails.title}</h3>
								{ReactHtmlParser(this.state.cmsDetails.description)}
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default About;