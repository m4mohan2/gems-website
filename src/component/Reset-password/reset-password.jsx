import React, { Component } from 'react';
import axios from 'axios';
import * as AppConst from '../../common/constants';

class ResetPassword extends Component {
	constructor(props) {
		super(props);
		this.checkUrl();
		this.state = {
			password: '',
			confirm_password: '',
			errorMessage: '',
			successMessage: '',
			showResetBox: true
		};
	}
	handleChange = (e) => {
		this.setState({
			[e.target.name]: e.target.value
		})
	}
	handleSubmit = () => {
		let newValue = {
			password: this.state.password,
			password_confirmation: this.state.confirm_password,
			token: this.props.match.params.token
		}
		this.setState({ successMessage: '' });
		this.setState({ errorMessage: '' });
		axios
			.post(AppConst.APIURL + `/api/resetPassword`, newValue)
			.then(res => {
				if (res.data.success == true) {
					this.setState({ successMessage: res.data.message });
					this.setState({ password: '' });
					this.setState({ confirm_password: '' });
					this.refs.password.value = '';
					this.refs.confirm_password.value = '';
					setTimeout(() => {
						this.props.history.push(AppConst.SERVERFOLDER);
					}, 5000);
				}
				else {
					this.setState({ errorMessage: res.data.message });
				}

			})
			.catch(err => {
				this.setState({ errorMessage: err.message });
			});

	}
	checkUrl = () => {
		axios
			.get(AppConst.APIURL + `/api/forgotPasswordVerify/` + this.props.match.params.token)
			.then(res => {
				//console.log(res);
				if (res.data.success == true) {
					//console.log('if succ: ',res.data.success);
					this.state.showResetBox = true;
				} else {
					//console.log('else succ: ',res.data.success);
					this.state.showResetBox = false;
					this.setState({ errorMessage: res.data.message });
				}
				//console.log(this.state.showResetBox);	
			})
			.catch(err => {
				this.setState({ errorMessage: err.message });
			});
	}
	render() {
		return (
			<div className="resetPasswordPage">
				<div className="commonBanner">
					<div className="container">
						<div className="row">
							<div className="col-12">

							</div>
						</div>
					</div>
				</div>
				{this.state.showResetBox ?
					<div className="loginPan">
						<h4>Reset Password</h4>
						{this.state.successMessage &&
							<span className="successMessage">{this.state.successMessage} </span>
						}
						{this.state.errorMessage &&
							<span className="errorMessage">{this.state.errorMessage} </span>
						}
						<div className="form-group">
							<input ref="password" type="password" name="password" class="form-control" id="password" required onChange={this.handleChange} />
							<label for="password">Password</label>
						</div>
						<div className="form-group">
							<input ref="confirm_password" type="password" class="form-control" name="confirm_password" id="confirm_password" required onChange={this.handleChange} />
							<label for="retypepassword">Confirm Password</label>
						</div>
						<button type="submit" class="btn" onClick={this.handleSubmit}>Submit</button>
					</div>
					:
					<div className="invalidUrl">{this.state.errorMessage} </div>

				}
			</div>
		);
	}
}

export default ResetPassword;