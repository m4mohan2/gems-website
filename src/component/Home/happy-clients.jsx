import React, { Component } from 'react';
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import * as AppConst from '../../common/constants';
import ReactHtmlParser from 'react-html-parser';

class HappyClients extends Component {
	constructor(props) {
		super(props)
		this.testimonialRef = React.createRef();
		this.state = {
		}
	}

	componentDidMount() {
	}

	render() {
		const headingList = this.props.headingList;
		const testimonialList = this.props.testimonialList;
		var settings = {
			dots: false,
			infinite: true,
			speed: 500,
			slidesToShow: 1,
			slidesToScroll: 1,
		};

		const star = (count) => {
			let starList = [];
			for (let i = 0; i < count; i++) {
				starList.push(<i className="fa fa-star" key={i} aria-hidden="true"></i>);
			}
			return starList;
		};
		return (
			<div className="happyClientsPan" ref={this.testimonialRef}>
				{testimonialList.length > 0 ? (
					<div className="container">
						{headingList.map((data) => data.slug === 'section-4-testimonial-title' ?
							ReactHtmlParser(data.short_description)
							: '')}
						<div className="clientSliderPan">

							<Slider {...settings}>

								{testimonialList.map(list => (
									<div className="slideItem" key={list.id}>
										<div className="clientImg">
											<img src={AppConst.UPLOADURL + '/testimonial/' + list.image} alt="GEMS" />
										</div>
										<div className="starPan">
											{star(list.star)}
										</div>
										<div className="clientTxt">
											{ReactHtmlParser(list.description)}
											<div className="btn">{list.name}</div>
										</div>
									</div>
								))
								}
							</Slider>

						</div>
					</div>
				)
					: ''
				}
			</div>
		);
	}
}

export default HappyClients;