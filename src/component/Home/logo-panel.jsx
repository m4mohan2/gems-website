import React, { Component } from 'react';
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import * as AppConst from '../../common/constants';
import { fetchHomepageList } from '../../action/cmsAction'

class LogoPanel extends Component {
	state = {
		logoList: []
	};

	componentDidMount() {
		fetchHomepageList(3).then(response => {
			const logoList = response.data;
			this.setState({ logoList: logoList });
		});
	}

	render() {
		const headingList = this.props.headingList;
		var settings = {
			dots: false,
			infinite: true,
			speed: 500,
			slidesToShow: 6,
			slidesToScroll: 1,
			responsive: [
				{
					breakpoint: 1024,
					settings: {
						slidesToShow: 4,
						slidesToScroll: 1,
					}
				},
				{
					breakpoint: 800,
					settings: {
						slidesToShow: 3,
						slidesToScroll: 2,
						initialSlide: 2
					}
				},
				{
					breakpoint: 480,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 1
					}
				}
			]
		};
		return (
			<div className="container">
				<div className="logoPan">
					<div className="sliderPan">
						<Slider {...settings}>
							{this.state.logoList.length > 0 ? (
								this.state.logoList.map(data => (
									<div key={data.id} className="slideItem"><a href="#"><img src={AppConst.UPLOADURL + '/homepage/' + data.image} alt="App Store" /></a></div>
								))
							)
								: ''
							}
						</Slider>
					</div>
				</div>
			</div>
		);
	}
}

export default LogoPanel;