import React, { Component, Fragment } from 'react';
import ExploreWorld from "./explore-world";
import CoreFeature from "./core-feature";
import GemsOfTheWeek from "./gems-of-the-week";
import HappyClients from "./happy-clients";
import Question from "./question";
import LogoPanel from "./logo-panel";

import NewsPanel from "./news-panel";
import '../../assets/style/home.scss';
import { Link } from 'react-router-dom';

import appStore from '../../assets/images/app-store.png';
import playStore from '../../assets/images/play-store.png';

import { fetchCmsList, fetchHomepageList, fetchTestimonialList } from '../../action/cmsAction'
import ReactHtmlParser from 'react-html-parser';
import Dashboard from '../Dashboard/dashboard';

class Home extends Component {
	state = {
		cmsList: [],
		exploreWorldList: [],
		coreFeatureList: [],
		testimonialList: [],
		//logoList: [],
		headingList: [],
		selectedFeature: ''
	};

	componentDidMount() {
		window.scrollTo(0, 0)
		fetchCmsList().then(response => {
			const cmsList = response.data.map(data => {
				return data.status === 1 ? data : '';
			});
			this.setState({ cmsList: cmsList });
		});

		fetchHomepageList(1).then(response => {
			const exploreWorldList = response.data;
			this.setState({ exploreWorldList: exploreWorldList });
		});
		fetchHomepageList(2).then(response => {
			const coreFeatureList = response.data;
			this.setState({ coreFeatureList: coreFeatureList });
			const selectedFeature = this.state.coreFeatureList.map((data, i) => {
				//console.log('check--',i,'==',data.slug);
				return i == 0 ? data.slug : '';
			});
			this.setState({ selectedFeature: selectedFeature[0] });
		});
		// fetchHomepageList(3).then(response => {
		// 	const logoList = response.data;
		// 	this.setState({ logoList: logoList });
		// });
		fetchHomepageList(4).then(response => {
			const headingList = response.data;
			this.setState({ headingList: headingList });
		});
		fetchTestimonialList().then(response => {
			const testimonialList = response.data;
			this.setState({ testimonialList: testimonialList });
		});
	}

	render() {
		return (
			<div className="homePage">
				{localStorage.getItem('token') !== null ? <Dashboard /> :
					<Fragment>
						{this.state.headingList.map((data) => data.slug === 'top-video-section' ?
							<div className="container-fluid px-0" key={data.id}>
								<div className="videoBanner">
									{ReactHtmlParser(data.short_description.replace('watch?v=', 'embed/'))}
								</div>
							</div>
							: '')}

						<ExploreWorld cmsList={this.state.cmsList} exploreWorldList={this.state.exploreWorldList} headingList={this.state.headingList} />

						<div className="appBg d-flex flex-column flex-md-row align-items-center">
							<div className="container">
								<span>
									Available for both Android and iOS
						</span>
								<Link to="/"><img src={appStore} alt="App Store" /></Link>
								<Link to="/"><img src={playStore} alt="Google Play" /></Link>
							</div>
						</div>

						<CoreFeature cmsList={this.state.cmsList} headingList={this.state.headingList} coreFeatureList={this.state.coreFeatureList} selectedFeature={this.state.selectedFeature} />

						<GemsOfTheWeek cmsList={this.state.cmsList} headingList={this.state.headingList} />

						<HappyClients cmsList={this.state.cmsList} headingList={this.state.headingList} testimonialList={this.state.testimonialList} />

						<LogoPanel cmsList={this.state.cmsList} />

						<Question id="faq" cmsList={this.state.cmsList} headingList={this.state.headingList} />

						<NewsPanel cmsList={this.state.cmsList} headingList={this.state.headingList} />
					</Fragment>}

			</div >
		);
	}
}

export default Home;