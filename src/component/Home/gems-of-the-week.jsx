import React, { Component } from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import ReactHtmlParser from "react-html-parser";
import { fetchGemsTrendingList } from "../../action/cmsAction";
import slideImg1 from "../../assets/images/slide-img1.png";
import ReactCountryFlag from "react-country-flag";
import countrydata from "./Countries";

class GemsOfTheWeek extends Component {
  state = {
    trendingGems: [],
  };

  componentDidMount() {
    fetchGemsTrendingList().then((response) => {
      let trendingGems = response.data;
      this.setState({ trendingGems });
    });
  }

  render() {
    const headingList = this.props.headingList;
    var settings = {
      dots: false,
      infinite: true,
      speed: 500,
      slidesToShow: 5,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1,
          },
        },
        {
          breakpoint: 800,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            initialSlide: 2,
          },
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          },
        },
      ],
    };
    return (
      <div className="gemsWeek">
        <div className="container">
          {headingList.map((data) =>
            data.slug === "section-3-trending-gems-title"
              ? ReactHtmlParser(data.short_description)
              : ""
          )}

          <div className="sliderPan">
            <Slider {...settings}>
              {this.state.trendingGems.map((data) => {
                return (
                  data.status === 1 && (
                    <div className="slideItem" key={data.id}>
                      <div className="slideContent">
                        <div className="imgPan">
                          {data.images === undefined ||
                          data.images.length === 0 ? (
                            <img src={slideImg1} alt="GEMS" />
                          ) : (
                            data.images.map((img, index) => {
                              return index === 0 ? (
                                img.source === "google_link" ? (
                                  <img
                                    className="rounded"
                                    style={{ width: "237px", height: "170px" }}
                                    key={index}
                                    src={img.image}
                                    alt="GEMS"
                                  />
                                ) : (
                                  <img
                                    className="rounded"
                                    style={{ width: "237px", height: "170px" }}
                                    key={index}
                                    src={
                                      "http://54.147.235.207/gems_uat/service/public/upload/gems/" +
                                      img.image
                                    }
                                    alt="GEMS"
                                  />
                                )
                              ) : (
                                ""
                              );
                            })
                          )}
                          <div className="gemsCount">
                            {data.total_like_count}
                          </div>
                        </div>
                        <div className="txtPan">
                          <h4>{data.name}</h4>
                          <p style={{ textAlign: "justify" }}>{data.address}</p>
                        </div>
                        <div className="viewPan">
                          <p>{data.total_view_count}</p>

                          {/* <div className="flag india"></div> #c5c9cd*/}
                          <ReactCountryFlag
                            // countryCode={countrydata.find(
                            //   (da) => da.dial_code === data.country_code
                            // )}
                            countryCode={
                              data.country_code ? data.country_code : null
                            }
                            svg
                            style={{
                              width: "27px",
                              height: "16px",
                              float: "right",
                            }}
                            title={data.country_code ? data.country_code : null}

                            // title={countrydata.find(
                            //   (da) => da.dial_code === data.country_code
                            // )}
                          />
                        </div>
                      </div>
                    </div>
                  )
                );
              })}

              {/* <div className="slideItem">
								<div className="slideContent">
									<div className="imgPan">
										<img src={sliderImg2} alt="GEMS" />
										<div className="gemsCount">15</div>
									</div>
									<div className="txtPan">
										<h4>Night Ninja</h4>
										<p>Kolkata</p>
									</div>
									<div className="viewPan">
										<p>236 Views</p>
										<div className="flag india"></div>

									</div>
								</div>
							</div>
							<div className="slideItem">
								<div className="slideContent">
									<div className="imgPan">
										<img src={sliderImg1} alt="GEMS" />
										<div className="gemsCount">15</div>
									</div>
									<div className="txtPan">
										<h4>Night Ninja</h4>
										<p>Kolkata</p>
									</div>
									<div className="viewPan">
										<p>236 Views</p>
										<div className="flag india"></div>
									</div>
								</div>
							</div>
							<div className="slideItem">
								<div className="slideContent">
									<div className="imgPan">
										<img src={sliderImg2} alt="GEMS" />
										<div className="gemsCount">15</div>
									</div>
									<div className="txtPan">
										<h4>Night Ninja</h4>
										<p>Kolkata</p>
									</div>
									<div className="viewPan">
										<p>236 Views</p>
										<div className="flag india"></div>
									</div>
								</div>
							</div>
							<div className="slideItem">
								<div className="slideContent">
									<div className="imgPan">
										<img src={sliderImg1} alt="GEMS" />
										<div className="gemsCount">15</div>
									</div>
									<div className="txtPan">
										<h4>Night Ninja</h4>
										<p>Kolkata</p>
									</div>
									<div className="viewPan">
										<p>236 Views</p>
										<div className="flag india"></div>
									</div>
								</div>
							</div>
							<div className="slideItem">
								<div className="slideContent">
									<div className="imgPan">
										<img src={sliderImg2} alt="GEMS" />
										<div className="gemsCount">15</div>
									</div>
									<div className="txtPan">
										<h4>Night Ninja</h4>
										<p>Kolkata</p>
									</div>
									<div className="viewPan">
										<p>236 Views</p>
										<div className="flag india"></div>
									</div>
								</div>
							</div> */}
            </Slider>
          </div>
        </div>
      </div>
    );
  }
}

export default GemsOfTheWeek;
