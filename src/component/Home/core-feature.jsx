import React, { Component } from 'react';
import { Tab, Nav, Row, Col } from "react-bootstrap";
import manageLocation from '../../assets/images/core-mobile.png';
import ReactHtmlParser from 'react-html-parser';
import { Link } from 'react-router-dom';

class CoreFeature extends Component {
	state = { showing: false, };

	render() {
		const { showing } = this.state;
		const headingList = this.props.headingList;
		const featureData = this.props.coreFeatureList;
		return (
			<div className="coreFeaturePan">
				{featureData.length > 0 ?
					<div className="container">
						{headingList.map((data) => data.slug === 'section-2-tab-section-title' ?
							ReactHtmlParser(data.short_description)
							: '')}
						<Tab.Container id="core-feature" defaultActiveKey="register-your-gems-account">
							<Row>
								<Col className="tabTopNav" sm={12}>
									<Nav variant="pills" className="nav-tabs">
										{featureData.map((data) => data.slug === 'register-your-gems-account' ?
											<Nav.Item key={data.id}>
												<Nav.Link className="register-member" eventKey="register-your-gems-account">
													{ReactHtmlParser(data.title)}
												</Nav.Link>
											</Nav.Item>
											: '')}
										{featureData.map((data) => data.slug === 'browse-through-all-gems' ?
											<Nav.Item key={data.id}>
												<Nav.Link className="manage-account" eventKey="browse-through-all-gems">
													{ReactHtmlParser(data.title)}
												</Nav.Link>
											</Nav.Item>
											: '')}
										{featureData.map((data) => data.slug === 'create-interesting-gems' ?
											<Nav.Item key={data.id}>
												<Nav.Link className="create-gems" eventKey="create-interesting-gems">
													{ReactHtmlParser(data.title)}
												</Nav.Link>
											</Nav.Item>
											: '')}
										{featureData.map((data) => data.slug === 'share-gems-with-your-inner-circle' ?
											<Nav.Item key={data.id}>
												<Nav.Link className="share-location" eventKey="share-gems-with-your-inner-circle">
													{ReactHtmlParser(data.title)}
												</Nav.Link>
											</Nav.Item>
											: '')}
										{featureData.map((data) => data.slug === 'navigate-by-filtering-and-finding' ?
											<Nav.Item key={data.id}>
												<Nav.Link className="explore-world" eventKey="explore-world">
													{ReactHtmlParser(data.title)}
												</Nav.Link>
											</Nav.Item>
											: '')}
										{featureData.map((data) => data.slug === 'store-gems-on-your-own-tracklist' ?
											<Nav.Item key={data.id}>
												<Nav.Link className="trusted-network" eventKey="trusted-network">
													{ReactHtmlParser(data.title)}
												</Nav.Link>
											</Nav.Item>
											: '')}
										{featureData.map((data) => data.slug === 'receive-tailored-gems-experiences' ?
											<Nav.Item key={data.id}>
												<Nav.Link className="add-friends" eventKey="receive-tailored-gems-experiences">
													{ReactHtmlParser(data.title)}
												</Nav.Link>
											</Nav.Item>
											: '')}
									</Nav>
								</Col>
								<Col sm={12}>
									<Tab.Content>
										<Nav variant="pills" className="nav-bodytab">
											<Nav.Item>
												<Nav.Link className="register-member" eventKey="register-your-gems-account">
													{featureData.map((data) => data.slug === 'register-your-gems-account' ?
														ReactHtmlParser(data.title)
														: '')}
													{/* Register as new member */}
												</Nav.Link>
											</Nav.Item>
										</Nav>
										<Tab.Pane eventKey="register-your-gems-account">
											<div className="row">
												<div className="col-12 col-md-6 text-right tabImgPan">
													<div className="coreMobileWrap">
														<img src={manageLocation} alt="Manage Location" />
														<div className="yellowTip tip1" onClick={() => this.setState({ showing: !showing })}></div>
														<div className="coreMobileTips tipTxt1" style={{ display: (showing ? 'block' : 'none') }}>
															<span className="gems-share"></span>
														Share Location
													</div>
													</div>
												</div>
												<div className="col-12 col-md-6">
													<div className="tabTxtWrap">
														<div className="tabTitle">{featureData.map((data) => data.slug === 'register-your-gems-account' ?
															ReactHtmlParser(data.title)
															: '')}</div>
														{featureData.map((data) => data.slug === 'register-your-gems-account' ?
															ReactHtmlParser(data.short_description)
															: '')}
														<Link to="/" className="btn">Read More</Link>
													</div>
												</div>
											</div>
										</Tab.Pane>
										<Nav variant="pills" className="nav-bodytab">
											<Nav.Item>
												<Nav.Link className="manage-account" eventKey="browse-through-all-gems">
													{featureData.map((data) => data.slug === 'browse-through-all-gems' ?
														ReactHtmlParser(data.title)
														: '')}
													{/* Manage your account */}
												</Nav.Link>
											</Nav.Item>
										</Nav>
										<Tab.Pane eventKey="browse-through-all-gems">
											<div className="row">
												<div className="col-12 col-md-6 text-right tabImgPan">
													<div className="coreMobileWrap">
														<img src={manageLocation} alt="Manage Location" />
														<div className="yellowTip tip1" onClick={() => this.setState({ showing: !showing })}></div>
														<div className="coreMobileTips tipTxt1" style={{ display: (showing ? 'block' : 'none') }}>
															<span className="gems-share"></span>
														Share Location
													</div>
													</div>
												</div>
												<div className="col-12 col-md-6">
													<div className="tabTxtWrap">
														<div className="tabTitle">{featureData.map((data) => data.slug === 'browse-through-all-gems' ?
															ReactHtmlParser(data.title)
															: '')}</div>
														{featureData.map((data) => data.slug === 'browse-through-all-gems' ?
															ReactHtmlParser(data.short_description)
															: '')}
														<Link to="/" className="btn">Read More</Link>
													</div>
												</div>
											</div>
										</Tab.Pane>
										<Nav variant="pills" className="nav-bodytab">
											<Nav.Item>
												<Nav.Link className="create-gems" eventKey="create-interesting-gems">
													{featureData.map((data) => data.slug === 'create-interesting-gems' ?
														ReactHtmlParser(data.title)
														: '')}
													{/* Create your own Gems */}
												</Nav.Link>
											</Nav.Item>
										</Nav>
										<Tab.Pane eventKey="create-interesting-gems">
											<div className="row">
												<div className="col-12 col-md-6 text-right tabImgPan">
													<div className="coreMobileWrap">
														<img src={manageLocation} alt="Manage Location" />
														<div className="yellowTip tip1" onClick={() => this.setState({ showing: !showing })}></div>
														<div className="coreMobileTips tipTxt1" style={{ display: (showing ? 'block' : 'none') }}>
															<span className="gems-share"></span>
														Share Location
													</div>
													</div>
												</div>
												<div className="col-12 col-md-6">
													<div className="tabTxtWrap">
														<div className="tabTitle">{featureData.map((data) => data.slug === 'create-interesting-gems' ?
															ReactHtmlParser(data.title)
															: '')}</div>
														{featureData.map((data) => data.slug === 'create-interesting-gems' ?
															ReactHtmlParser(data.short_description)
															: '')}
														<Link to="/" className="btn">Read More</Link>
													</div>
												</div>
											</div>
										</Tab.Pane>
										<Nav variant="pills" className="nav-bodytab">
											<Nav.Item>
												<Nav.Link className="share-location" eventKey="share-gems-with-your-inner-circle">
													{featureData.map((data) => data.slug === 'share-gems-with-your-inner-circle' ?
														ReactHtmlParser(data.title)
														: '')}
													{/* Share your location */}
												</Nav.Link>
											</Nav.Item>
										</Nav>
										<Tab.Pane eventKey="share-gems-with-your-inner-circle">
											<div className="row">
												<div className="col-12 col-md-6 text-right tabImgPan">
													<div className="coreMobileWrap">
														<img src={manageLocation} alt="Manage Location" />
														<div className="yellowTip tip1" onClick={() => this.setState({ showing: !showing })}></div>
														<div className="coreMobileTips tipTxt1" style={{ display: (showing ? 'block' : 'none') }}>
															<span className="gems-share"></span>
														Share Location
													</div>
													</div>
												</div>
												<div className="col-12 col-md-6">
													<div className="tabTxtWrap">
														<div className="tabTitle">{featureData.map((data) => data.slug === 'share-gems-with-your-inner-circle' ?
															ReactHtmlParser(data.title)
															: '')}</div>
														{featureData.map((data) => data.slug === 'share-gems-with-your-inner-circle' ?
															ReactHtmlParser(data.short_description)
															: '')}
														<Link to="/" className="btn">Read More</Link>
													</div>
												</div>
											</div>
										</Tab.Pane>
										<Nav variant="pills" className="nav-bodytab">
											<Nav.Item>
												<Nav.Link className="explore-world" eventKey="explore-world">
													{featureData.map((data) => data.slug === 'navigate-by-filtering-and-finding' ?
														ReactHtmlParser(data.title)
														: '')}
													{/* Explore the world */}
												</Nav.Link>
											</Nav.Item>
										</Nav>
										<Tab.Pane eventKey="explore-world">
											<div className="row">
												<div className="col-12 col-md-6 text-right tabImgPan">
													<div className="coreMobileWrap">
														<img src={manageLocation} alt="Manage Location" />
														<div className="yellowTip tip1" onClick={() => this.setState({ showing: !showing })}></div>
														<div className="coreMobileTips tipTxt1" style={{ display: (showing ? 'block' : 'none') }}>
															<span className="gems-share"></span>
														Share Location
													</div>
													</div>
												</div>
												<div className="col-12 col-md-6">
													<div className="tabTxtWrap">
														<div className="tabTitle">{featureData.map((data) => data.slug === 'navigate-by-filtering-and-finding' ?
															ReactHtmlParser(data.title)
															: '')}</div>
														{featureData.map((data) => data.slug === 'navigate-by-filtering-and-finding' ?
															ReactHtmlParser(data.short_description)
															: '')}
														<Link to="/" className="btn">Read More</Link>
													</div>
												</div>
											</div>
										</Tab.Pane>
										<Nav variant="pills" className="nav-bodytab">
											<Nav.Item>
												<Nav.Link className="trusted-network" eventKey="trusted-network">
													{featureData.map((data) => data.slug === 'store-gems-on-your-own-tracklist' ?
														ReactHtmlParser(data.title)
														: '')}
													{/* Your trusted network */}
												</Nav.Link>
											</Nav.Item>
										</Nav>
										<Tab.Pane eventKey="trusted-network">
											<div className="row">
												<div className="col-12 col-md-6 text-right tabImgPan">
													<div className="coreMobileWrap">
														<img src={manageLocation} alt="Manage Location" />
														<div className="yellowTip tip1" onClick={() => this.setState({ showing: !showing })}></div>
														<div className="coreMobileTips tipTxt1" style={{ display: (showing ? 'block' : 'none') }}>
															<span className="gems-share"></span>
														Share Location
													</div>
													</div>
												</div>
												<div className="col-12 col-md-6">
													<div className="tabTxtWrap">
														<div className="tabTitle">{featureData.map((data) => data.slug === 'store-gems-on-your-own-tracklist' ?
															ReactHtmlParser(data.title)
															: '')}</div>
														{featureData.map((data) => data.slug === 'store-gems-on-your-own-tracklist' ?
															ReactHtmlParser(data.short_description)
															: '')}
														<Link to="/" className="btn">Read More</Link>
													</div>
												</div>
											</div>
										</Tab.Pane>
										<Nav variant="pills" className="nav-bodytab">
											<Nav.Item>
												<Nav.Link className="add-friends" eventKey="receive-tailored-gems-experiences">
													{featureData.map((data) => data.slug === 'receive-tailored-gems-experiences' ?
														ReactHtmlParser(data.title)
														: '')}
													{/* Add your friends */}
												</Nav.Link>
											</Nav.Item>
										</Nav>
										<Tab.Pane eventKey="receive-tailored-gems-experiences">
											<div className="row">
												<div className="col-12 col-md-6 text-right tabImgPan">
													<div className="coreMobileWrap">
														<img src={manageLocation} alt="Manage Location" />
														<div className="yellowTip tip1" onClick={() => this.setState({ showing: !showing })}></div>
														<div className="coreMobileTips tipTxt1" style={{ display: (showing ? 'block' : 'none') }}>
															<span className="gems-share"></span>
														Share Location
													</div>
													</div>
												</div>
												<div className="col-12 col-md-6">
													<div className="tabTxtWrap">
														<div className="tabTitle">{featureData.map((data) => data.slug === 'receive-tailored-gems-experiences' ?
															ReactHtmlParser(data.title)
															: '')}</div>
														{featureData.map((data) => data.slug === 'receive-tailored-gems-experiences' ?
															ReactHtmlParser(data.short_description)
															: '')}
														<Link to="/" className="btn">Read More</Link>
													</div>
												</div>
											</div>
										</Tab.Pane>

									</Tab.Content>
								</Col>
							</Row>
						</Tab.Container>

					</div>
					: ''
				}
			</div >
		);
	}
}

export default CoreFeature;