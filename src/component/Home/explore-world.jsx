import React, { Component } from 'react';
import ReactHtmlParser from 'react-html-parser';

class ExploreWorld extends Component {
	state = {
	};

	componentDidMount() {
		var promoArea = parseInt(this.refs.promo.offsetHeight);
		window.addEventListener('scroll', this._calcScroll.bind(this, promoArea));
		window.addEventListener('scroll', this.handleOnScroll);
	}

	componentWillUnmount() {
		window.removeEventListener('scroll', this._calcScroll)
	}

	_calcScroll(promoArea) {
		setTimeout(function () {
			var _window = window;
			var heightDiff = parseInt(promoArea);
			var scrollPos = _window.scrollY + 400;
			if (scrollPos > heightDiff) {
				document.body.classList.add("promoAnimation");
			}
		}, 1000);
	}

	render() {
		const data = this.props.headingList;
		const homepageData = this.props.exploreWorldList;
		return (
			<div className="promoPan" ref="promo">
				<div className="container-fluid diamondTop">
					{data.map((data) => data.slug === 'section-1-diamond-portion-1st-heading' ?
						ReactHtmlParser(data.short_description)
						: '')}
					{data.map((data) => data.slug === 'section-1-diamond-portion-2nd-line' ?
						ReactHtmlParser(data.short_description)
						: '')}</div>
				{homepageData.length > 0 ?
					<React.Fragment>
						<div className="container diamondBg">
							{data.map((data) => data.slug === 'section-1-diamond-portion-3rd-line' ?
								ReactHtmlParser(data.short_description)
								: '')}
							<ul className="featureBox">
								{homepageData.map((data) => data.slug === 'manage-gems' ?
									<li className="manage" key={data.id}><span className="fontIcon gems-diamond"></span>
										<div>
											{ReactHtmlParser(data.title)}
										</div>
									</li>
									: '')}
								{homepageData.map((data) => data.slug === 'share-locations' ?
									<li className="share" key={data.id}><span className="fontIcon gems-share"></span>
										<div>
											{ReactHtmlParser(data.title)}
										</div>
									</li>
									: '')}
								{homepageData.map((data) => data.slug === 'online-chat' ?
									<li className="online" key={data.id}><span className="fontIcon gems-chat"></span>
										<div>
											{ReactHtmlParser(data.title)}
										</div>
									</li>
									: '')}
								{homepageData.map((data) => data.slug === 'add-friends' ?
									<li className="add" key={data.id}><span className="fontIcon gems-followers"></span>
										<div>
											{ReactHtmlParser(data.title)}
										</div>
									</li>
									: '')}
							</ul>
						</div>
						<a href="#" className="btn">Read More</a>
					</React.Fragment>
					: ''}
			</div>
		);
	}
}

export default ExploreWorld;