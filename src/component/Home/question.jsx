import React, { Component } from 'react';
import { Tab, Nav, Accordion, Card, } from "react-bootstrap";
import ReactHtmlParser from 'react-html-parser';
import { fetchFaqCategoryList } from '../../action/cmsAction';
import * as AppConst from '../../common/constants';
import { Link } from 'react-router-dom';

class Question extends Component {
	constructor(props) {
		super(props);
		this.state = {
			errorMessage: '',
			successMessage: '',
			CategoryList: [],
		};
	}

	componentDidMount() {
		this.faqList();
	}
	faqList = () => {
		fetchFaqCategoryList().then(res => {
			const CategoryList = res.data;
			this.setState({ CategoryList });
		}).catch(err => {
			this.setState({ errorMessage: err.message });
		});
	}

	render() {
		const headingList = this.props.headingList;
		return (
			<div className="questionPan">
				<div className="container">
					{headingList.map((data) => data.slug === 'section-5-faq-title' ?
						ReactHtmlParser(data.short_description)
						: '')}
					<Tab.Container id="left-tabs-example" defaultActiveKey="using-gems">
						<div className="row">
							<div className="tabTopNav col-12 col-lg-4 col-md-4">
								<Nav variant="pills" className="flex-column">
									{this.state.CategoryList.length > 0 ? (
										this.state.CategoryList.map(list => (
											<React.Fragment key={list.id}>
												{list.faq.length > 0 ?
													<Nav.Item>
														<Nav.Link eventKey={list.slug}>
															{list.title}
														</Nav.Link>
													</Nav.Item>
													: ''}
											</React.Fragment>
										))
									)
										: ''
									}
								</Nav>
							</div>
							<div className="col-12 col-lg-8 col-md-8">
								<Tab.Content>
									{this.state.CategoryList.length > 0 ? (
										this.state.CategoryList.map(list => (
											<React.Fragment key={list.id}>
												<Nav variant="pills" className="tabBodynav">
													<Nav.Item>
														<Nav.Link className="manage-account" eventKey={list.slug}>
															{list.title}
														</Nav.Link>
													</Nav.Item>
												</Nav>
												<Tab.Pane eventKey={list.slug}>
													<Accordion defaultActiveKey="">
														{list.faq.length > 0 ? (
															list.faq.map((faqlist, i) => (
																<Card key={i}>
																	<Accordion.Toggle as={Card.Header} eventKey={i}>
																		{faqlist.question}
																		{/* Etiam nibh mauris, elementum nec egestas dignissim, tristique sed nequ nullam */}
																	</Accordion.Toggle>
																	<Accordion.Collapse eventKey={i}>
																		<Card.Body>
																			{ReactHtmlParser(faqlist.answer)}
																			{/* Etiam nibh mauris, elementum nec egestas dignissim, tristique sed nequ nullam Etiam nibh mauris, elementum nec egestas dignissim, tristique sed nequ nullam Etiam nibh mauris, elementum nec egestas dignissim, tristique sed nequ nullam Etiam nibh mauris, elementum nec egestas dignissim, tristique sed nequ nullam */}
																		</Card.Body>
																	</Accordion.Collapse>
																</Card>

															))
														)
															: ''
														}
													</Accordion>
												</Tab.Pane>
											</React.Fragment>
										))
									)
										: ''
									}

								</Tab.Content>
							</div>
						</div>
					</Tab.Container>
					<div className="text-center"><Link to="/" className="btn">View All</Link></div>

				</div>
			</div>
		);
	}
}

export default Question;