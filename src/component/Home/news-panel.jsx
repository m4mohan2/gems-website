import React, { Component } from 'react';
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import * as AppConst from '../../common/constants';
import Moment from 'react-moment';
import { Link } from 'react-router-dom';
import { fetchNewsList } from '../../action/cmsAction';

import ReactHtmlParser from 'react-html-parser';

class NewsPanel extends Component {
	constructor(props) {
		super(props);
		this.newsRef = React.createRef();
		this.state = {
			errorMessage: '',
			successMessage: '',
			newsData: []
		};
	}
	newsList = () => {
		fetchNewsList().then(res => {
			const newsData = res.data;
			this.setState({
				newsData,
				loading: false
			});
		}).catch(err => {
			this.setState({ errorMessage: err.message });
		});
	}

	componentDidMount() {
		this.newsList();
	}

	render() {
		const headingList = this.props.headingList;
		var settings = {
			dots: false,
			infinite: true,
			speed: 500,
			slidesToShow: 3,
			slidesToScroll: 1,
			responsive: [
				{
					breakpoint: 800,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 2,
						initialSlide: 2
					}
				},
				{
					breakpoint: 480,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1
					}
				}
			]
		};
		return (
			<div className="newsPan" ref={this.newsRef}>
				<div className="container">
					{headingList.map((data) => data.slug === 'section-6-news-section-title' ?
						ReactHtmlParser(data.short_description)
						: '')}

					<div className="sliderPan">
						<Slider {...settings}>

							{this.state.newsData.length > 0 ? (
								this.state.newsData.map(news => (

									<div className="slideItem" key={news.id}>
										<div className="slideImg">
											<img
												src={AppConst.UPLOADURL + '/news/' + news.image}
												alt="GEMS"
											/>
										</div>
										<div className="txtPan">
											<p className="date"><Moment format="MMMM Do, YYYY">{news.created_at}</Moment></p>

											{ReactHtmlParser(news.short_description)}
											{/* Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. */}
											<Link to={'news/' + news.slug}>Read More</Link>

										</div>
									</div>
								))
							)
								: ''
							}
						</Slider>
					</div>

					{/* <a href="#" className="viewAllLink">View All</a> */}
				</div>
			</div>
		);
	}
}

export default NewsPanel;