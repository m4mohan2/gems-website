import React, { Component } from 'react';
import * as AppConst from '../../common/constants';
import * as am4core from '@amcharts/amcharts4/core';
import * as am4charts from '@amcharts/amcharts4/charts';
import am4themes_animated from '@amcharts/amcharts4/themes/animated';
import { Table, Image } from 'react-bootstrap';
import * as am4maps from '@amcharts/amcharts4/maps';
import am4geodata_worldLow from '@amcharts/amcharts4-geodata/worldLow';
import Moment from 'moment';
import { Link } from 'react-router-dom';
import LoadingSpinner from '../../common/LoadingSpinner/LoadingSpinner';
import axios from 'axios';
import DatePicker from 'react-datepicker';
am4core.useTheme(am4themes_animated);

class Dashboard extends Component {
    state = {
        selectedGem: '',
        gemsLists: [],
        loading: false,
        errorMessge: null,
        engagementSummeryList: [],
        popularGemsList: [],
        genderWiseList: [],
        audienceCountryWiseList: [],
        fromDate: new Date(Moment(new Date()).subtract(1, 'months').toISOString()),
        toDate: new Date(),
        geodata: [],
        totallikes: 0,
        totalsaved: 0,
        totalview: 0,
        totalshare: 0,
        linechartLoader: false,
        popularGemsLoader: false,
        demographicLoader: false,
        chartErrorMessge: ''
    }

    displayError = (e) => {
        let errorMessge = '';
        try {
            errorMessge = e.data.message ? e.data.message : e.data.error_description;
        } catch (e) {
            errorMessge = 'Unknown error!';
        }
        return errorMessge;
    }

    lineChart = () => {
        let chart = am4core.create('chartdiv', am4charts.XYChart);
        chart.colors.step = 2;
        chart.data = this.state.engagementSummeryList;
        let dateAxis = chart.xAxes.push(new am4charts.DateAxis());
        dateAxis.renderer.minGridDistance = 50;
        function createAxisAndSeries(field, name, opposite, bullet) {
            let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
            valueAxis.min = 0;
            valueAxis.maxPrecision = 0;
            if (chart.yAxes.indexOf(valueAxis) != 0) {
                valueAxis.syncWithAxis = chart.yAxes.getIndex(0);
            }

            let series = chart.series.push(new am4charts.LineSeries());
            series.dataFields.valueY = field;
            series.dataFields.dateX = 'date';
            series.strokeWidth = 2;
            series.yAxis = valueAxis;
            series.name = name;
            series.tooltipText = '{name}: [bold]{valueY}[/]';
            series.tensionX = 0.8;
            series.showOnInit = true;

            let interfaceColors = new am4core.InterfaceColorSet();

            switch (bullet) {
                case 'triangle': {
                    bullet = series.bullets.push(new am4charts.Bullet());
                    bullet.width = 12;
                    bullet.height = 12;
                    bullet.horizontalCenter = 'middle';
                    bullet.verticalCenter = 'middle';

                    let triangle = bullet.createChild(am4core.Triangle);
                    triangle.stroke = interfaceColors.getFor('background');
                    triangle.strokeWidth = 2;
                    triangle.direction = 'top';
                    triangle.width = 12;
                    triangle.height = 12;
                    break;
                }
                case 'rectangle': {
                    bullet = series.bullets.push(new am4charts.Bullet());
                    bullet.width = 10;
                    bullet.height = 10;
                    bullet.horizontalCenter = 'middle';
                    bullet.verticalCenter = 'middle';

                    let rectangle = bullet.createChild(am4core.Rectangle);
                    rectangle.stroke = interfaceColors.getFor('background');
                    rectangle.strokeWidth = 2;
                    rectangle.width = 10;
                    rectangle.height = 10;
                    break;
                }
                default:
                    bullet = series.bullets.push(new am4charts.CircleBullet());
                    bullet.circle.stroke = interfaceColors.getFor('background');
                    bullet.circle.strokeWidth = 2;
                    break;
            }

            valueAxis.renderer.line.strokeOpacity = 1;
            valueAxis.renderer.line.strokeWidth = 2;
            valueAxis.renderer.line.stroke = series.stroke;
            valueAxis.renderer.labels.template.fill = series.stroke;
            valueAxis.renderer.opposite = opposite;
        }

        createAxisAndSeries('like', 'Like', false, 'circle');
        createAxisAndSeries('saved', 'Saved', true, 'triangle');
        createAxisAndSeries('share', 'Share', true, 'rectangle');
        createAxisAndSeries('view', 'View', true, 'triangle');
        chart.legend = new am4charts.Legend();
        chart.cursor = new am4charts.XYCursor();
    }

    pieChart = (data) => {
        let chart = am4core.create('piediv', am4charts.PieChart);
        chart.data = data;
        let pieSeries = chart.series.push(new am4charts.PieSeries());
        pieSeries.dataFields.value = 'percent';
        pieSeries.dataFields.category = 'gender';

        pieSeries.labels.template.disabled = true;
        pieSeries.ticks.template.disabled = true;

        pieSeries.slices.template.tooltipText = '';
        chart.legend = new am4charts.Legend();
        chart.legend.labels.template.fill = am4core.color('black');
        chart.legend.valueLabels.template.fill = am4core.color('black');
    }

    demographicChart = () => {
        let chart = am4core.create('demographicdiv', am4maps.MapChart);
        chart.geodata = am4geodata_worldLow;
        chart.projection = new am4maps.projections.Miller();
        let polygonSeries = chart.series.push(new am4maps.MapPolygonSeries());
        polygonSeries.useGeodata = true;
        let polygonTemplate = polygonSeries.mapPolygons.template;
        polygonTemplate.tooltipText = '{name}: {value}';
        polygonTemplate.fill = am4core.color('#74B266');
        let hs = polygonTemplate.states.create('hover');
        hs.properties.fill = am4core.color('#367B25');
        polygonSeries.exclude = ['AQ'];

        polygonSeries.heatRules.push({
            'property': 'fill',
            'target': polygonSeries.mapPolygons.template,
            'min': am4core.color('#C7D5BB'),
            'max': am4core.color('#3D7F05')
        });

        polygonSeries.data = this.state.geodata;
        polygonTemplate.propertyFields.fill = 'fill';
    }

    gemList = () => {
        this.setState({ loading: true });
        const sessionKey = localStorage.getItem('token');
        axios.interceptors.request.use(req => {
            req.headers.Authorization = 'Bearer ' + `${sessionKey}`;
            return req;
        });
        axios.get(AppConst.APIURL + '/api/allGemList')
            .then(res => {
                const gemsLists = res.data;
                this.setState({
                    gemsLists: gemsLists,
                    selectedGem: res.data[0].id
                });
                this.engagementSummary(res.data.length !== 0 && res.data[0].id, Moment(this.state.fromDate).format('yyyy-MM-DD'), Moment(this.state.toDate).format('yyyy-MM-DD'));
                this.audienceCountryWise(res.data.length !== 0 && res.data[0].id, Moment(this.state.fromDate).format('yyyy-MM-DD'), Moment(this.state.toDate).format('yyyy-MM-DD'));
                this.engagementGenderWise(res.data.length !== 0 && res.data[0].id, Moment(this.state.fromDate).format('yyyy-MM-DD'), Moment(this.state.toDate).format('yyyy-MM-DD'));
                this.setState({ loading: false });
            })
            .catch(e => {
                let errorMsg = this.displayError(e);
                this.setState({
                    errorMessge: errorMsg,
                    loading: false
                });
                setTimeout(() => {
                    this.setState({ errorMessge: null });
                }, 5000);
            });
    }

    popularGems = (fromdate, todate) => {
        this.setState({ popularGemsLoader: true });
        const sessionKey = localStorage.getItem('token');
        axios.interceptors.request.use(req => {
            req.headers.Authorization = 'Bearer ' + `${sessionKey}`;
            return req;
        });
        axios.get(AppConst.APIURL + `/api/popularGems?from_date=${fromdate}&to_date=${todate}`)
            .then(res => {
                this.setState({ popularGemsList: res.data.message, popularGemsLoader: false });
            })
            .catch(e => {
                let errorMsg = this.displayError(e);
                this.setState({
                    errorMessge: errorMsg,
                    popularGemsLoader: false
                });
                setTimeout(() => {
                    this.setState({ errorMessge: null });
                }, 5000);
            });
    }

    engagementSummary = (id, fromdate, todate) => {
        this.setState({ linechartLoader: true });
        const sessionKey = localStorage.getItem('token');
        axios.interceptors.request.use(req => {
            req.headers.Authorization = 'Bearer ' + `${sessionKey}`;
            return req;
        });
        axios.get(AppConst.APIURL + `/api/engagementSummery/${id}?from_date=${fromdate}&to_date=${todate}`)
            .then(res => {
                let totallikes = 0;
                let totalsaved = 0;
                let totalview = 0;
                let totalshare = 0;
                res.data.message.map(data => {
                    return totallikes = totallikes + data.like;
                });
                res.data.message.map(data => {
                    return totalsaved = totalsaved + data.saved;
                });
                res.data.message.map(data => {
                    return totalview = totalview + data.view;
                });
                res.data.message.map(data => {
                    return totalshare = totalshare + data.share;
                });
                this.setState({
                    engagementSummeryList: res.data.message,
                    totallikes,
                    totalsaved,
                    totalview,
                    totalshare,
                    linechartLoader: false
                });
                this.lineChart();
            })
            .catch(e => {
                let errorMsg = this.displayError(e);
                this.setState({
                    errorMessge: errorMsg,
                    linechartLoader: false
                });
                setTimeout(() => {
                    this.setState({ errorMessge: null });
                }, 5000);
            });
    }

    audienceCountryWise = (id, fromdate, todate) => {
        const sessionKey = localStorage.getItem('token');
        axios.interceptors.request.use(req => {
            req.headers.Authorization = 'Bearer ' + `${sessionKey}`;
            return req;
        });
        axios.get(AppConst.APIURL + `/api/audienceCountryWise/${id}?from_date=${fromdate}&to_date=${todate}`)
            .then(res => {
                const geodata = [];
                res.data.message.map((data) => {
                    return geodata.push({ id: data.iso2, value: data.gems_comment_count + data.gems_like_count + data.gems_share_count });
                });
                this.setState({ audienceCountryWiseList: res.data.message, geodata });
                this.demographicChart();
            })
            .catch(e => {
                let errorMsg = this.displayError(e);
                this.setState({
                    errorMessge: errorMsg,
                });
                setTimeout(() => {
                    this.setState({ errorMessge: null });
                }, 5000);
            });
    }

    engagementGenderWise = (id, fromdate, todate) => {
        const sessionKey = localStorage.getItem('token');
        axios.interceptors.request.use(req => {
            req.headers.Authorization = 'Bearer ' + `${sessionKey}`;
            return req;
        });
        axios.get(AppConst.APIURL + `/api/engagementGenderWise/${id}?from_date=${fromdate}&to_date=${todate}`)
            .then(res => {
                this.setState({ genderWiseList: res.data.message });
                this.pieChart(res.data.message);
            })
            .catch(e => {
                let errorMsg = this.displayError(e);
                this.setState({
                    errorMessge: errorMsg,
                    loading: false
                });
                setTimeout(() => {
                    this.setState({ errorMessge: null });
                }, 5000);
            });
    }

    componentDidMount() {
        if (localStorage.getItem('token') === null) {
            this.props.history.push(AppConst.SERVERFOLDER);
        } else {
            window.scrollTo(0, 0);
            this.gemList();
            this.popularGems(Moment(this.state.fromDate).format('yyyy-MM-DD'), Moment(this.state.toDate).format('yyyy-MM-DD'));
        }
    }

    handleChange = (e) => {
        this.setState({ selectedGem: e.target.value, chartErrorMessge: '' });
    }

    handleFromDate = date => {
        if (date > this.state.toDate) {
            this.setState({ fromDate: date, toDate: '', chartErrorMessge: '' });
        } else {
            this.setState({ fromDate: date, chartErrorMessge: '' });
        }
    }

    handleToDate = date => {
        this.setState({ toDate: date, chartErrorMessge: '' });
    }

    resetSearch = () => {
        const fromDate = new Date(Moment(new Date()).subtract(1, 'months').toISOString());
        const toDate = new Date();
        this.setState({
            fromDate,
            toDate,
            chartErrorMessge: ''
        }, () => { this.gemList(); this.popularGems(Moment(this.state.fromDate).format('yyyy-MM-DD'), Moment(this.state.toDate).format('yyyy-MM-DD')); });
    }

    handleSubmit = (e) => {
        e.preventDefault();
        if (this.state.selectedGem === '' || this.state.fromDate === '' || this.state.toDate === '') {
            this.setState({ chartErrorMessge: 'Please choose gems, from date and to date to get the chart' });
        } else {
            this.engagementSummary(this.state.selectedGem, Moment(this.state.fromDate).format('yyyy-MM-DD'), Moment(this.state.toDate).format('yyyy-MM-DD'));
            this.engagementGenderWise(this.state.selectedGem, Moment(this.state.fromDate).format('yyyy-MM-DD'), Moment(this.state.toDate).format('yyyy-MM-DD'));
            this.audienceCountryWise(this.state.selectedGem, Moment(this.state.fromDate).format('yyyy-MM-DD'), Moment(this.state.toDate).format('yyyy-MM-DD'));
            this.popularGems(Moment(this.state.fromDate).format('yyyy-MM-DD'), Moment(this.state.toDate).format('yyyy-MM-DD'));
        }
    }

    render() {
        return (
            <div className="claimSearchPage">
                <div className="claimBannerPan">
                    <div className="container">
                        <h2>Dashboard</h2>
                        <h6 className="text-center text-light mt-n3">Period&nbsp;&nbsp;&nbsp;&nbsp;
                            {this.state.fromDate && Moment(this.state.fromDate).format('D MMM YYYY')}
                            &nbsp;&nbsp;
                            {this.state.toDate && Moment(this.state.toDate).format('D MMM YYYY')}
                        </h6>
                    </div>
                </div>
                <div className="container mt-4">
                    {this.state.loading ? <LoadingSpinner /> :
                        <React.Fragment>
                            <div className='row'>
                                <div className='col-6'>
                                    <button className='btn-sm btn-secondary' disabled>Engagement Summary</button>
                                </div>
                                <div className='col-1'></div>
                            </div>

                            <form onSubmit={this.handleSubmit}>
                                <div className='row mt-3'>
                                    <div className='col-md-6'>
                                        <select value={this.state.selectedGem} onChange={this.handleChange} className='form-control'>
                                            <option value=''>{this.state.loading === true ? 'Loading...' : 'Select GEM'}</option>
                                            {this.state.gemsLists.map(data => {
                                                return <option value={data.id} key={data.id}>{data.name}</option>;
                                            })}
                                        </select>
                                    </div>
                                    <div className='col-md-2'>
                                        <DatePicker
                                            name='fromDate'
                                            selected={this.state.fromDate}
                                            onChange={this.handleFromDate}
                                            className='form-control'
                                            maxDate={new Date()}
                                            required
                                            dateFormat="dd-MM-yyyy"
                                            showMonthDropdown
                                            showYearDropdown
                                            placeholderText="From date"
                                        />
                                    </div>
                                    <div className='col-md-2'>
                                        <DatePicker
                                            name='toDate'
                                            selected={this.state.toDate}
                                            onChange={this.handleToDate}
                                            className='form-control'
                                            maxDate={new Date()}
                                            minDate={this.state.fromDate}
                                            required
                                            dateFormat="dd-MM-yyyy"
                                            showMonthDropdown
                                            showYearDropdown
                                            placeholderText="To date"
                                        />
                                    </div>
                                    <div className='col-md-2'>
                                        <input type='submit' value='Search' className='btn btn-primary' />
                                        <Link to="#" className='refreshIcon ml-3' onClick={() => this.resetSearch()}>
                                            <i className="fa fa-refresh" aria-hidden="true"></i>
                                        </Link>
                                    </div>
                                </div>
                            </form>
                            {this.state.chartErrorMessge ? (
                                <div className="text-center bold mt-3 text-success">
                                    {this.state.chartErrorMessge}
                                </div>
                            ) : null}

                            <hr style={{ border: '1px solid gray' }} />

                            {this.state.linechartLoader ? <LoadingSpinner /> :
                                <div className=''>
                                    <div className='row text-body'>
                                        <div className='col-2 mt-3'></div>
                                        <div className='col mt-3' style={{ color: '#33A4FF' }}>
                                            Like: {this.state.totallikes}
                                        </div>
                                        <div className='col mt-3' style={{ color: '#5633FF' }}>
                                            Saved: {this.state.totalsaved}
                                        </div>
                                        <div className='col mt-3' style={{ color: '#473A86' }}>
                                            Share: {this.state.totalshare}
                                        </div>
                                        <div className='col mt-3' style={{ color: '#843494' }}>
                                            View: {this.state.totalview}
                                        </div>
                                        <div className='col-2 mt-3'></div>
                                    </div>
                                    <div id='chartdiv' style={{ width: '100%', height: '300px' }}></div>
                                </div>}

                            <div className='row mt-5'>
                                <div className='col-12'>
                                    <button className='btn-sm btn-secondary' disabled>Your Most Popular GEMS</button>
                                </div>
                            </div>

                            {this.state.popularGemsLoader ? <LoadingSpinner /> :
                                <div className='mt-3'>
                                    <Table responsive hover>
                                        <thead className='theaderBg'>
                                            <tr>
                                                <th>&nbsp;</th>
                                                <th>Published</th>
                                                <th className="text-center">Shared With</th>
                                                <th className="text-center">Likes</th>
                                                <th className="text-center">Comments</th>
                                                <th className="text-center">Share</th>
                                                <th className="text-center">Growth</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {this.state.popularGemsList.map(data => {
                                                return <tr key={data.id}>
                                                    <td>{data.name}</td>
                                                    <td>{Moment(data.created_at).format('ddd MMM D YYYY')}</td>
                                                    <td className="text-center">{data.privacy === 1 ? <i className='fa fa-globe' aria-hidden='true' title='globe'></i> :
                                                        <i className='fa fa-users' aria-hidden='true' title='users'></i>}</td>
                                                    <td className="text-center">{data.total_like_count}</td>
                                                    <td className="text-center">{data.total_comment_count}</td>
                                                    <td className="text-center">{data.total_share_count}</td>
                                                    <td className="text-center">{data.growth} % {data.growth === 0 ? '' :
                                                        Math.sign(data.growth) === -1 ? <i className='fa fa-arrow-down text-danger' aria-hidden='true' title='users'></i> :
                                                            <i className='fa fa-arrow-up text-success' aria-hidden='true' title='users'></i>}</td>
                                                </tr>;
                                            })}
                                        </tbody>
                                    </Table>
                                </div>}

                            <div className='row mt-5'>
                                <div className='col-12'>
                                    <button className='btn-sm btn-secondary' disabled>Engagement Audience</button>
                                </div>
                            </div>
                            <div className='mt-3 mb-3'>
                                <div className='row'>
                                    <div className='col-md-7 col-sm-12'>
                                        <div id='demographicdiv' style={{ width: '100%', height: '300px' }}></div>
                                    </div>
                                    <div className='col-md-5 col-sm-12'>
                                        <div id='piediv' style={{ width: '100%', height: '300px' }}></div>
                                    </div>
                                </div>
                            </div>
                        </React.Fragment>}
                </div>
            </div>
        )
    };
}

export default Dashboard;