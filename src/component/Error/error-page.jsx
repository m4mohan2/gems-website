import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class ErrorPage extends Component {

	componentDidMount() {
		window.scrollTo(0, 0)
	}

	state = {}
	render() {
		return (
			<div>
				<div className="commonBanner">
					<div className="container">
						<div className="row">
							<div className="col-12">

							</div>
						</div>
					</div>
				</div>
				<div className="contentPan">
					<div className="container">
						<div className="row">
							<div className="col-12">

								<div className="errorPan">
									<div className="errorCircle">
										404
									</div>
									<h4>Page not found</h4>
									<p>Maybe this page moved, got deleted or does not exist.<br />
									Let's back to <Link to="/">home</Link>.</p>
								</div>


							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default ErrorPage;