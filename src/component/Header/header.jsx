import React, { Component, Fragment } from 'react';
import { Link } from "react-router-dom";
import * as Yup from 'yup';
import * as AppConst from '../../common/constants';
import axios from '../../shared/eaxios';
import { Button, Col, FormControl, FormGroup, Modal, Row, Image } from 'react-bootstrap';
import { Field, Form, Formik } from 'formik';
import LoadingSpinner from '../../common/LoadingSpinner/LoadingSpinner';
import { connect } from 'react-redux';
import darkLogo from '../../assets/images/logo-dark.png';
import lightLogo from '../../assets/images/logo-white.png';
import { ToastContainer, toast } from 'react-toastify';
import clientImg from '../../assets/images/blank-profile.jpg';
import { fetchCountryList, fetchStateList, fetchCityList, updateProvider, viewProvider, updatePassword } from '../../action/cmsAction';
import SuccessIco from '../../assets/images/success-ico.png';

const loginValues = {
	email: '',
	password: ''
};

const forgotPasswordValues = {
	email: ''
};

const changePasswordValues = {
	old_password: '',
	new_password: '',
	confirm_password: ''
};

const LoginSchema = Yup.object().shape({
	email:
		Yup.string()
			.trim('Please remove whitespace')
			.email('Email must be valid')
			.strict()
			.required('Please enter email address'),
	password:
		Yup
			.string()
			.trim('Please remove whitespace')
			.strict()
			.required('Please enter password'),
});

const changePasswordSchema = Yup.object().shape({
	old_password:
		Yup
			.string()
			.trim('Please remove whitespace')
			.strict()
			.required('Please enter old password'),
	new_password:
		Yup
			.string()
			.trim('Please remove whitespace')
			.strict()
			.required('Please enter new password'),
	confirm_password:
		Yup.string()
			.oneOf([Yup.ref('new_password'), null], 'Passwords must match')
});

const ForgotPasswordSchema = Yup.object().shape({
	email:
		Yup.string()
			.trim('Please remove whitespace')
			.email('Email must be valid')
			.strict()
			.required('Please enter email address')
});

const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/;

const editProfileSchema = Yup.object().shape({
	first_name: Yup.string()
		.trim('Please remove whitespace')
		.strict()
		.required('Please enter first name')
		.max(40, 'maximum characters length 40'),
	last_name: Yup.string()
		.trim('Please remove whitespace')
		.strict()
		.required('Please enter last name')
		.max(40, 'maximum characters length 40'),
	phone: Yup.string()
		.required('Please enter phone number')
		.min(10, 'Phone number must be 10 digit')
		.max(10, 'Phone number must be 10 digit')
		.matches(phoneRegExp, 'Phone number is not valid'),
	address: Yup.string()
		.trim('Please remove whitespace')
		.required('Please enter address')
		.strict()
		.max(40, 'maximum characters length 40'),
	address2: Yup.string()
		.trim('Please remove whitespace')
		.strict()
		.max(40, 'maximum characters length 40'),
	// personalCountry: Yup.string().required('Please select country'),
	personalZip: Yup.string()
		.required('Please enter zip')
		.max(6, 'ZIP must be 6 digit')
		.min(6, 'ZIP must be 6 digit'),
	company_name: Yup.string()
		.trim('Please remove whitespace')
		.strict()
		.required('Please enter company name')
		.max(40, 'maximum characters length 40'),
	company_phone: Yup.string()
		.required('Please enter company phone number')
		.max(10, 'Phone number must be 10 digit')
		.min(10, 'Phone number must be 10 digit')
		.matches(phoneRegExp, 'Phone number is not valid'),
	addr1: Yup.string()
		.trim('Please remove whitespace')
		.required('Please enter company address')
		.strict()
		.max(40, 'maximum characters length 40'),
	addr2: Yup.string()
		.trim('Please remove whitespace')
		.strict()
		.max(40, 'maximum characters length 40'),
	// companyCountry: Yup.string().required('Please select company country'),
	companyZip: Yup.string()
		.required('Please enter zip')
		.max(6, 'ZIP must be 6 digit')
		.min(6, 'ZIP must be 6 digit')
});

class Header extends Component {
	state = {
		dropmenuShow: false,
		usermenuShow: false,
		loginModel: false,
		loader: false,
		token: '',
		success: '',
		user: '',
		viewProfileModel: false,

		errorMessge: null,
		editProfileEnable: false,
		disabled: false,
		editProfileLoader: false,
		editErrorMessge: false,
		countryList: [],
		personalStateList: [],
		personalCityList: [],
		companyStateList: [],
		companyCityList: [],
		providerLoader: false,
		personalCountry: '',
		personalState: '',
		companyCountry: '',
		companyState: '',

		statusConfirMmsg: false,
		forgotPasswordModel: false,
		forgotPasswordLoader: false,
		changePasswordModel: false,
		changePasswordLoader: false,

		selectedFile: '',
		uploadErrorMessage: ''
	};

	componentDidMount = () => {
		window.addEventListener('scroll', this.handleOnScroll);
		fetchCountryList().then(response => {
			this.setState({ countryList: response.data.data });
		});
		localStorage.getItem('user_id') !== null &&
			viewProvider(localStorage.getItem('user_id')).then(res => {
				localStorage.removeItem('profile_pic');
				localStorage.setItem('profile_pic', res.data.user.user_detail.profile_pic);
				localStorage.removeItem('first_name');
				localStorage.setItem('first_name', res.data.user.user_detail.first_name);
				localStorage.removeItem('last_name');
				localStorage.setItem('last_name', res.data.user.user_detail.last_name);
			});
	}

	displayError = (e) => {
		let errorMessge = '';
		try {
			errorMessge = e.data.message ? e.data.message : e.data.error_description;
		} catch (e) {
			errorMessge = 'Access is denied!';
		}
		return errorMessge;
	}

	componentWillUnmount = () => {
		window.removeEventListener('scroll', this.handleOnScroll);
	}

	// handle onScroll event
	handleOnScroll = () => {

		if (window.scrollY === 0) {
			//console.log(window.scrollY)
			document.body.classList.remove("scrolled");
		}
		else if (window.scrollY >= 50) {
			document.body.classList.add("scrolled");
		}
	}

	handleClick(e) {
		e.preventDefault();
		document.body.classList.toggle('mobileMenuOpen');
	}

	linkClick() {
		document.body.classList.remove('mobileMenuOpen');
	}

	handleLoginModel = () => {
		this.setState({
			loginModel: true,
			forgotPasswordModel: false,
			changePasswordModel: false,
		});
	}

	handleLoginModelClose = () => {
		this.setState({
			loginModel: false,
		});
	}

	handleStateList = (data) => {
		fetchStateList(data).then(response => {
			this.setState({ personalStateList: response.data.data });
		});
	}

	handleCityList = (data) => {
		fetchCityList(data).then(response => {
			this.setState({ personalCityList: response.data.data });
		});
	}

	handleLoginSubmit = (values, { resetForm }) => {
		const val = {
			'email': values.email,
			'password': values.password,
			'user_type': 2
		};
		this.setState({ loader: true });
		axios.post(AppConst.APIURL + '/api/login', val)
			.then(res => {
				resetForm({});
				this.setState({
					loader: false,
					message: res.data.message,
					token: res.data.token,
					success: true
				});
				toast.success(res.data.message, {
					position: "bottom-right",
					autoClose: 5000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true,
					progress: undefined,
				});

				const details = {
					token: res.data.token,
					active_status: res.data.user.active_status,
					email: res.data.user.email,
					first_name: res.data.user.user_detail.first_name,
					last_name: res.data.user.user_detail.last_name,
					user_id: res.data.user.user_detail.user_id,
					profile_pic: res.data.user.user_detail.profile_pic
				}
				localStorage.clear();
				localStorage.setItem('token', res.data.token);
				localStorage.setItem('active_status', res.data.user.active_status);
				localStorage.setItem('email', res.data.user.email);
				localStorage.setItem('first_name', res.data.user.user_detail.first_name);
				localStorage.setItem('last_name', res.data.user.user_detail.last_name);
				localStorage.setItem('user_id', res.data.user.user_detail.user_id);
				localStorage.setItem('profile_pic', res.data.user.user_detail.profile_pic);
				this.props.AddUser(details);
				this.handleLoginModelClose();
				window.location.href = AppConst.SERVERFOLDER + "/dashboard";
			})
			.catch(error => {
				console.log(error);
				toast.error('Email and password are not correct.', {
					position: "bottom-right",
					autoClose: 5000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true,
					progress: undefined,
				});
				this.setState({ success: false, loader: false });
				this.handleLoginModelClose();
			});
	}

	handleLogout = () => {
		const sessionKey = localStorage.getItem('token');
		axios.interceptors.request.use(req => {
			req.headers.Authorization = 'Bearer ' + `${sessionKey}`;
			return req;
		});

		axios.post(AppConst.APIURL + '/api/logout')
			.then(res => {
				const details = {
					token: '',
					active_status: '',
					email: '',
					first_name: '',
					last_name: '',
					user_id: ''
				}
				localStorage.clear();
				this.props.AddUser(details);
				toast.success(res.data.message, {
					position: "bottom-right",
					autoClose: 5000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true,
					progress: undefined,
				});
				window.location.href = AppConst.SERVERFOLDER + "/";
			})
			.catch(err => {
				toast.error('Something went wrong!', {
					position: "bottom-right",
					autoClose: 5000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true,
					progress: undefined,
				});
			});
	}

	handleProfile = () => {
		this.setState({
			viewProfileModel: true,
			providerLoader: true,
		})
		viewProvider(localStorage.getItem('user_id')).then(res => {
			this.setState({
				providerLoader: false,
				user: res.data.user,
				personalCountry: res.data.user.user_detail.country,
				personalState: res.data.user.user_detail.state,
				companyCountry: res.data.user.company_detail.country,
				companyState: res.data.user.company_detail.state,
			});
			localStorage.removeItem('profile_pic');
			localStorage.setItem('profile_pic', res.data.user.user_detail.profile_pic);
			localStorage.removeItem('first_name');
			localStorage.setItem('first_name', res.data.user.user_detail.first_name);
			localStorage.removeItem('last_name');
			localStorage.setItem('last_name', res.data.user.user_detail.last_name);
			this.props.AddUserDetails(res.data.user);
			this.handleStateList(res.data.user.user_detail.country);
			this.handleCityList(res.data.user.user_detail.state);
			fetchStateList(res.data.user.company_detail.country).then(response => {
				this.setState({ companyStateList: response.data.data });
			});
			fetchCityList(res.data.user.company_detail.state).then(response => {
				this.setState({ companyCityList: response.data.data });
			});
		});
	}

	handleProfileModelClose = () => {
		this.setState({
			viewProfileModel: false,
			disabled: false,
			editProfileEnable: false
		});
	}

	handleEditVendorEnable = () => {
		this.setState({
			editProfileEnable: true,
			disabled: true
		});
	}

	handelviewEditModalClose = () => {
		this.setState({
			editProfileEnable: false,
			disabled: false
		});
	}

	handleChange = (e) => {
		this.setState({
			[e.target.name]: e.target.value,
		});
		if (e.target.name === 'personalCountry') {
			this.handleStateList(e.target.value);
		}
		if (e.target.name === 'personalState') {
			this.handleCityList(e.target.value);
		}
		if (e.target.name === 'companyCountry') {
			fetchStateList(e.target.value).then(response => {
				this.setState({ companyStateList: response.data.data });
			});
		}
		if (e.target.name === 'companyState') {
			fetchCityList(e.target.value).then(response => {
				this.setState({ companyCityList: response.data.data });
			});
		}
	}

	handleProfileSubmit = (values, { setSubmitting }) => {
		if (this.state.uploadErrorMessage !== '') {
			setSubmitting(false);
		}
		else {
			this.setState({
				providerLoader: true,
			});
			const formData = new window.FormData();
			formData.append('first_name', values.first_name);
			formData.append('last_name', values.last_name);
			formData.append('phone', values.phone);
			formData.append('email', values.email);
			formData.append('address', values.address);
			formData.append('address2', values.address2);
			formData.append('personalCountry', this.state.personalCountry);
			formData.append('personalState', this.state.personalState);
			formData.append('personalCity', values.personalCity);
			formData.append('personalZip', values.personalZip);
			formData.append('active_status', values.active_status);
			formData.append('company_name', values.company_name);
			formData.append('company_phone', values.company_phone);
			formData.append('addr1', values.addr1);
			formData.append('addr2', values.addr2);
			formData.append('companyCountry', this.state.companyCountry);
			formData.append('companyState', this.state.companyState);
			formData.append('companyCity', values.companyCity);
			formData.append('companyZip', values.companyZip);
			formData.append('profile_pic', this.state.selectedFile);

			updateProvider(values.id, formData)
				.then(res => {
					setSubmitting(false);
					viewProvider(localStorage.getItem('user_id')).then(res => {
						localStorage.removeItem('profile_pic');
						localStorage.setItem('profile_pic', res.data.user.user_detail.profile_pic);
						localStorage.removeItem('first_name');
						localStorage.setItem('first_name', res.data.user.user_detail.first_name);
						localStorage.removeItem('last_name');
						localStorage.setItem('last_name', res.data.user.user_detail.last_name);
					});
					this.setState({
						providerLoader: false,
						editProfileEnable: false,
						disabled: false,
						statusConfirMmsg: true,
						viewProfileModel: false,
						successMessage: res.data.message,
					});
				})
				.catch(e => {
					let errorMsg = this.displayError(e);
					this.setState({
						providerLoader: false,
						editErrorMessge: errorMsg,
					});
					setTimeout(() => {
						this.setState({ editErrorMessge: null });
					}, 5000);

				});
		}
	};

	handleStatusChangedClose = () => {
		this.setState({
			statusConfirMmsg: false,
			successMessage: null
		});
	}

	handleForgotPassword = () => {
		this.setState({
			loginModel: false,
			forgotPasswordModel: true,
			changePasswordModel: false,
		});
	}

	handleForgotPasswordClose = () => {
		this.setState({
			forgotPasswordModel: false,
		});
	}

	handleForgotPasswordSubmit = (values, { setSubmitting }) => {
		this.setState({
			forgotPasswordLoader: true,
		});
		let newValue = {
			email: values.email,
		};
		axios
			.post(AppConst.APIURL + `/api/forgotPassword`, newValue)
			.then(res => {
				setSubmitting(false);
				this.setState({
					forgotPasswordLoader: false,
					forgotPasswordModel: false,
					statusConfirMmsg: true,
					successMessage: res.data.message,
				});
			})
			.catch(e => {
				let errorMsg = this.displayError(e);
				this.setState({
					forgotPasswordLoader: false,
					editErrorMessge: errorMsg,
				});
				setTimeout(() => {
					this.setState({ editErrorMessge: null });
				}, 5000);
			});
	};

	handleChangePassword = () => {
		this.setState({
			loginModel: false,
			forgotPasswordModel: false,
			changePasswordModel: true,
		});
	}

	handleChangePasswordClose = () => {
		this.setState({
			changePasswordModel: false,
		});
	}

	handleChangePasswordSubmit = (values, { setSubmitting }) => {
		this.setState({
			changePasswordLoader: true,
		});
		let newValue = {
			old_password: values.old_password,
			new_password: values.new_password,
			confirm_password: values.confirm_password
		};
		updatePassword(newValue)
			.then(res => {
				setSubmitting(false);
				this.setState({
					changePasswordLoader: false,
					changePasswordModel: false,
					statusConfirMmsg: true,
					successMessage: res.data.message,
				});
				this.handleLogout();
			})
			.catch(e => {
				let errorMsg = this.displayError(e);
				this.setState({
					changePasswordLoader: false,
					editErrorMessge: errorMsg,
				});
				toast.error('Old Password does not match', {
					position: "bottom-right",
					autoClose: 5000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true,
					progress: undefined,
				});
				setTimeout(() => {
					this.setState({ editErrorMessge: null, changePasswordModel: false });
				}, 5000);
			});
	};

	onFileChange = event => {
		this.setState({ uploadErrorMessage: '' });
		if (event.target.files.length !== 0) {
			if (event.target.files[0].size > 600000) {
				this.setState({
					uploadErrorMessage: 'File size should be less than 600KB',
					isDrop: false,
				});
			}
			else if ((event.target.files[0].type !== 'image/png') && (event.target.files[0].type !== 'image/jpeg')) {
				this.setState({
					uploadErrorMessage: 'Please upload jpeg/png file',
					isDrop: false,
				});
			}
			else {
				this.setState({ selectedFile: event.target.files[0], uploadErrorMessage: '' });
			}
		}
	};

	render() {
		const { dropmenuShow } = this.state;
		const { usermenuShow } = this.state;
		const initialValues = this.state.user;
		const values = {
			id: initialValues && initialValues.id,
			first_name: initialValues && initialValues.user_detail.first_name,
			last_name: initialValues && initialValues.user_detail.last_name,
			address: initialValues && initialValues.user_detail.address,
			address2: initialValues && initialValues.user_detail.address2,
			phone: initialValues && initialValues.user_detail.phone,
			personalCountry: initialValues && initialValues.user_detail.country,
			personalState: initialValues && initialValues.user_detail.state,
			personalCity: initialValues && initialValues.user_detail.city,
			personalZip: initialValues && initialValues.user_detail.zip,
			active_status: initialValues && initialValues.active_status,
			company_email: initialValues && initialValues.company_detail.company_email,
			company_name: initialValues && initialValues.company_detail.company_name,
			company_phone: initialValues && initialValues.company_detail.company_phone,
			addr1: initialValues && initialValues.company_detail.addr1,
			addr2: initialValues && initialValues.company_detail.addr2,
			companyCountry: initialValues && initialValues.company_detail.country,
			companyState: initialValues && initialValues.company_detail.state,
			companyCity: initialValues && initialValues.company_detail.city,
			companyZip: initialValues && initialValues.company_detail.zip,
			profile_pic: initialValues && initialValues.user_detail.profile_pic,
			email: initialValues && initialValues.email
		};
		const { disabled } = this.state;
		return (
			<div>
				{localStorage.getItem('token') ?
					<React.Fragment>
						<div className="navbar navbar-expand-lg providerHeader header">
							<div className="container-fluid topHead">
								<div className="logo navbar-brand">
									<Link to={AppConst.SERVERFOLDER + "/dashboard"}>
										<figure className="logoDark"><img src={darkLogo} alt="GEMS" /></figure>
										{/* <figure className="logoLight"><img src={lightLogo} alt="GEMS" /></figure> */}
									</Link>
								</div>
								<div className="my-0 my-md-0">
									<div className="userPan">
										<div className="userImg" onClick={() => this.setState({ usermenuShow: !usermenuShow })}>
											{localStorage.getItem('profile_pic') !== 'null' ?
												< img src={AppConst.UPLOADURL + '/profile/' + localStorage.getItem('profile_pic')} alt="GEMS" />
												: <img src={clientImg} alt="GEMS" />
											}
										</div>
										{/*onClick={() => this.setState({ usermenuShow: !usermenuShow })}*/}
										<div className="userName">
											Welcome {localStorage.getItem('first_name')}&nbsp;{localStorage.getItem('last_name')}
										</div>
										{/*<div className="userDrop" style={{ display: (usermenuShow ? 'block' : 'none') }}>
											<Link onClick={this.handleProfile}>Profile</Link>
											<Link onClick={this.handleChangePassword} style={{ whiteSpace: 'nowrap' }}>Change Password</Link>
											<Link onClick={this.handleLogout}>Logout</Link>
										</div>*/}
									</div>
								</div>
							</div>
							<div className="container-fluid bottoHead">
								<div className="my-0 my-md-0 bd-navbar">
									<ul className="navbar-nav bd-navbar-nav flex-row topNav">
										<li className="nav-item"><Link to={AppConst.SERVERFOLDER + "/dashboard"} className="nav-link">Dashboard</Link></li>
										<li className="nav-item"><Link to={AppConst.SERVERFOLDER + "/claim-your-gem"} className="nav-link">Claim your gems</Link></li>
										<li className="nav-item"><Link to={AppConst.SERVERFOLDER + "/provider"} className="nav-link">My Gem</Link></li>
										<li className="nav-item"><Link to={AppConst.SERVERFOLDER + "/coupon"} className="nav-link">Coupon</Link></li>
										<li className="nav-item"><Link to={AppConst.SERVERFOLDER + "/paymentdetails"} className="nav-link">Payment Details</Link></li>
										<li className="nav-item" onClick={this.handleProfile}><Link className="nav-link">Profile</Link></li>
										<li className="nav-item" onClick={this.handleChangePassword}><Link className="nav-link">Change Password</Link></li>
										<li className="nav-item" onClick={this.handleLogout}><Link className="nav-link">Logout</Link></li>
									</ul>
								</div>
								{/* <div className="toggleMenu desktopMenu" onClick={() => this.setState({ dropmenuShow: !dropmenuShow })}><i className="fa fa-bars"></i>
									<div className="dropMenu" style={{ display: (dropmenuShow ? 'block' : 'none') }}>
										<ul>
											<li><Link to={AppConst.SERVERFOLDER + "/claim-your-gem"}>Claim your gems</Link></li>
											{localStorage.getItem('token') ?
												<React.Fragment>
													<li><Link to={AppConst.SERVERFOLDER + "/dashboard"}>Dashboard</Link></li>
													<li><Link to={AppConst.SERVERFOLDER + "/provider"}>My Gem</Link></li>
													<li><Link to={AppConst.SERVERFOLDER + "/coupon"}>Coupon</Link></li>
													<li><Link to={AppConst.SERVERFOLDER + "/paymentorders"}>Payment Orders</Link></li>
													<li><Link onClick={this.handleProfile}>Profile</Link></li>
													<li><Link onClick={this.handleChangePassword}>Change Password</Link></li>
													<li onClick={this.handleLogout}><Link>Logout</Link></li>
												</React.Fragment>
												:
												<React.Fragment>
													<li><Link to={AppConst.SERVERFOLDER + "/error"}>What is Gems</Link></li>
													<li><Link to={AppConst.SERVERFOLDER + "/how-it-works"}>How it Works</Link></li>
													<li onClick={this.handleLoginModel}><Link>Login</Link></li>
												</React.Fragment>
											}
										</ul>
									</div>
								</div> */}

								<div className="toggleMenu mobileMenu" onClick={e => this.handleClick(e)}>
									<i className="fa fa-bars bars"></i>
									<i className="fa fa-times close"></i>
								</div>
								<div className="mobileSideMenu">
									<ul>
										<li><Link to={AppConst.SERVERFOLDER + "/dashboard"} onClick={e => this.linkClick()}>Dashboard</Link></li>
										<li><Link to={AppConst.SERVERFOLDER + "/claim-your-gem"} onClick={e => this.linkClick()}>Claim your gems</Link></li>
										<li><Link to={AppConst.SERVERFOLDER + "/provider"} onClick={e => this.linkClick()}>My Gem</Link></li>
										<li><Link to={AppConst.SERVERFOLDER + "/coupon"} onClick={e => this.linkClick()}>Coupon</Link></li>
										<li><Link to={AppConst.SERVERFOLDER + "/paymentdetails"} onClick={e => this.linkClick()}>Payment Details</Link></li>
										<li><Link onClick={this.handleProfile}>Profile</Link></li>
										<li><Link onClick={this.handleChangePassword}>Change Password</Link></li>
										<li onClick={this.handleLogout}><Link>Logout</Link></li>
									</ul>
								</div>
							</div>
							<ToastContainer />
						</div>
					</React.Fragment>
					:
					<React.Fragment>
						<div className="navbar navbar-expand-lg header scrollHeader normalHeader">
							<div className="container-fluid">
								<div className="logo navbar-brand">
									<Link to={AppConst.SERVERFOLDER + "/"}>
										<figure className="logoDark"><img src={darkLogo} alt="GEMS" /></figure>
										<figure className="logoLight"><img src={lightLogo} alt="GEMS" /></figure>
									</Link>
								</div>
								<div className="my-0 my-md-0 bd-navbar">
									<ul className="navbar-nav bd-navbar-nav flex-row topNav">
										<li className="nav-item">
											<Link to={AppConst.SERVERFOLDER + "/"} className="nav-link">
												Home
											</Link>
										</li>
										<li className="nav-item">
											<Link to={AppConst.SERVERFOLDER + "/explore"} className="nav-link">
												Explore
											</Link>
										</li>
										<li className="nav-item">
											<Link to={AppConst.SERVERFOLDER + "/error"} className="nav-link">
												Download
											</Link>
										</li>
										<li className="nav-item">
											<Link to={AppConst.SERVERFOLDER + "/error"} className="nav-link">
												Core Features
											</Link>
										</li>
										<li className="nav-item">
											<Link to={AppConst.SERVERFOLDER + "/error"} className="nav-link">
												Plans
											</Link>
										</li>
										<li className="nav-item">
											<Link to={AppConst.SERVERFOLDER + "/error"} className="nav-link">
												Testimonials
											</Link>
										</li>
										<li className="nav-item">
											<Link to="faq" className="nav-link">
												FAQ
											</Link>
										</li>
										<li className="nav-item">
											<Link to={AppConst.SERVERFOLDER + "/error"} className="nav-link">
												Contact
											</Link>
										</li>
									</ul>
								</div>
								<div className="toggleMenu desktopMenu" onClick={() => this.setState({ dropmenuShow: !dropmenuShow })}><i className="fa fa-bars"></i>
									<div className="dropMenu" style={{ display: (dropmenuShow ? 'block' : 'none') }}>
										<ul>
											<li><Link to={AppConst.SERVERFOLDER + "/claim-your-gem"}>Claim your gems</Link></li>
											<li><Link to={AppConst.SERVERFOLDER + "/error"}>What is Gems</Link></li>
											<li><Link to={AppConst.SERVERFOLDER + "/how-it-works"}>How it Works</Link></li>
											<li onClick={this.handleLoginModel}><Link>Login</Link></li>
										</ul>
									</div>
								</div>
								<div className="toggleMenu mobileMenu" onClick={e => this.handleClick(e)}>
									<i className="fa fa-bars bars"></i>
									<i className="fa fa-times close"></i>
								</div>
								<div className="mobileSideMenu">
									<ul>
										<li><Link to={AppConst.SERVERFOLDER + "/"} onClick={e => this.linkClick()}>Home</Link></li>
										<li><Link to={AppConst.SERVERFOLDER + "/explore"} onClick={e => this.linkClick()}>Explore</Link></li>
										<li><Link to={AppConst.SERVERFOLDER + "/error"} onClick={e => this.linkClick()}>Download</Link></li>
										<li><Link to={AppConst.SERVERFOLDER + "/error"} onClick={e => this.linkClick()}>Core Features</Link></li>
										<li><Link to={AppConst.SERVERFOLDER + "/error"} onClick={e => this.linkClick()}>Plans</Link></li>
										<li><Link to={AppConst.SERVERFOLDER + "/error"} onClick={e => this.linkClick()}>Testimonials</Link></li>
										<li><Link to={AppConst.SERVERFOLDER + "/error"} onClick={e => this.linkClick()}>Providers</Link></li>
										<li><Link to={AppConst.SERVERFOLDER + "/error"} onClick={e => this.linkClick()}>FAQ</Link></li>
										<li><Link to={AppConst.SERVERFOLDER + "/error"} onClick={e => this.linkClick()}>Contact</Link></li>
										<li><Link to={AppConst.SERVERFOLDER + "/error"} onClick={e => this.linkClick()}>What is Gems</Link></li>
										<li><Link to={AppConst.SERVERFOLDER + "/how-it-works"} onClick={e => this.linkClick()}>How it Works</Link></li>
										<li><Link to={AppConst.SERVERFOLDER + "/claim-your-gem"} onClick={e => this.linkClick()}>Claim your gems</Link></li>
										{/* <li onClick={this.handleChangePassword}>Change Password</li> */}
										<li onClick={this.handleLoginModel}><Link>Login</Link></li>
									</ul>
								</div>
							</div>
							<ToastContainer />
						</div>
					</React.Fragment>
				}

				<Modal show={this.state.loginModel} className="payOptionPop" size="md" onHide={this.handleLoginModelClose}>
					<Modal.Header closeButton>
						<Modal.Title>Login</Modal.Title>
					</Modal.Header>
					<Modal.Body className="">
						<div className="loginPan loginPopup">

							{this.state.loader ? <LoadingSpinner /> : null}

							<Formik
								initialValues={loginValues}
								validationSchema={LoginSchema}
								onSubmit={this.handleLoginSubmit}
							>
								{({ values, errors, touched, isSubmitting, handleChange }) => {
									return (
										<Form>
											<div className="formSection">
												<div className="row">
													<div className="col-12">
														<FormGroup controlId="formControlsTextarea">
															<Field
																required
																name="email"
																type="text"
																className={'form-control'}
																autoComplete="nope"
																value={values.email || ''}
															/>
															<label htmlFor="email">Email</label>
															{errors.email && touched.email ? (
																<span className="errorMsg ml-1">{errors.email}</span>
															) : null}
															<FormControl.Feedback />
														</FormGroup>
													</div>
													<div className="col-12">
														<FormGroup controlId="formControlsTextarea">
															<Field
																required
																name="password"
																type="password"
																className={'form-control'}
																autoComplete="nope"
																value={values.password || ''}
															/>
															<label htmlFor="password">Password</label>
															{errors.password && touched.password ? (
																<span className="errorMsg ml-1">{errors.password}</span>
															) : null}
															<FormControl.Feedback />
														</FormGroup>
													</div>
												</div>
												<div className="row">
													<div className="col-6"></div>
													<div className="col-6">
														<Link onClick={this.handleForgotPassword}>Forgot password?</Link>
													</div>
												</div>
											</div>

											<div className="btnPan">
												<Button className="btn blue" type="submit">
													Login
		                    				</Button>
											</div>
										</Form>
									);
								}}
							</Formik>
						</div>
					</Modal.Body>
				</Modal>

				<Modal show={this.state.forgotPasswordModel} className="payOptionPop" size="md" onHide={this.handleForgotPasswordClose}>
					<Modal.Header closeButton>
						<Modal.Title>Forgot Password</Modal.Title>
					</Modal.Header>
					<Modal.Body className="">
						<div className="loginPan loginPopup">

							{this.state.forgotPasswordLoader ? <LoadingSpinner /> : null}

							<Formik
								initialValues={forgotPasswordValues}
								validationSchema={ForgotPasswordSchema}
								onSubmit={this.handleForgotPasswordSubmit}
							>
								{({ values, errors, touched, isSubmitting }) => {
									return (
										<Form>
											<div className="formSection">
												<div className="row">
													<div className="col-12">
														<FormGroup controlId="formControlsTextarea">
															<Field
																required
																name="email"
																type="text"
																className={'form-control'}
																autoComplete="nope"
																value={values.email || ''}
															/>
															<label htmlFor="email">Email</label>
															{errors.email && touched.email ? (
																<span className="errorMsg ml-1">{errors.email}</span>
															) : null}
															<FormControl.Feedback />
														</FormGroup>
													</div>
												</div>
												<div className="row">
													<div className="col-8"></div>
													<div className="col-4">
														<Link onClick={this.handleLoginModel}>Login</Link>
													</div>
												</div>
											</div>
											<div className="btnPan">
												<Button className="btn blue" type="submit" disabled={isSubmitting}>Submit</Button>
											</div>
										</Form>
									);
								}}
							</Formik>
						</div>
					</Modal.Body>
				</Modal>

				<Modal show={this.state.changePasswordModel} className="payOptionPop" size="md" onHide={this.handleChangePasswordClose}>
					<Modal.Header closeButton>
						<Modal.Title>Change Password</Modal.Title>
					</Modal.Header>
					<Modal.Body className="">
						<div className="loginPan loginPopup">

							{this.state.changePasswordLoader ? <LoadingSpinner /> : null}

							<Formik
								initialValues={changePasswordValues}
								validationSchema={changePasswordSchema}
								onSubmit={this.handleChangePasswordSubmit}
							>
								{({ values, errors, touched, isSubmitting }) => {
									return (
										<Form>
											<div className="formSection">
												<div className="row">
													<div className="col-12">
														<FormGroup controlId="formControlsTextarea">
															<Field
																required
																name="old_password"
																type="password"
																className={'form-control'}
																autoComplete="nope"
																value={values.old_password || ''}
															/>
															<label htmlFor="old_password">Old Password</label>
															{errors.old_password && touched.old_password ? (
																<span className="errorMsg ml-1">{errors.old_password}</span>
															) : null}
															<FormControl.Feedback />
														</FormGroup>
													</div>
													<div className="col-12">
														<FormGroup controlId="formControlsTextarea">
															<Field
																required
																name="new_password"
																type="password"
																className={'form-control'}
																autoComplete="nope"
																value={values.new_password || ''}
															/>
															<label htmlFor="new_password">New Password</label>
															{errors.new_password && touched.new_password ? (
																<span className="errorMsg ml-1">{errors.new_password}</span>
															) : null}
															<FormControl.Feedback />
														</FormGroup>
													</div>
													<div className="col-12">
														<FormGroup controlId="formControlsTextarea">
															<Field
																required
																name="confirm_password"
																type="password"
																className={'form-control'}
																autoComplete="nope"
																value={values.confirm_password || ''}
															/>
															<label htmlFor="confirm_password">Confirm Password</label>
															{errors.confirm_password && touched.confirm_password ? (
																<span className="errorMsg ml-1">{errors.confirm_password}</span>
															) : null}
															<FormControl.Feedback />
														</FormGroup>
													</div>
												</div>
												{/* {localStorage.getItem('token') ? '' :
													<div className="row">
														<div className="col-8"></div>
														<div className="col-4">
															<Link onClick={this.handleLoginModel}>Login</Link>
														</div>
														<div className="col-6"></div>
														<div className="col-6">
															<Link onClick={this.handleForgotPassword}>Forgot password?</Link>
														</div>
													</div>} */}
											</div>
											<div className="btnPan">
												<Button className="btn blue" type="submit" disabled={isSubmitting}>Submit</Button>
											</div>
										</Form>
									);
								}}
							</Formik>
						</div>
					</Modal.Body>
				</Modal>

				<Modal show={this.state.viewProfileModel} size="xl" onHide={this.handleProfileModelClose}>
					<Modal.Header closeButton><h2>{this.state.editProfileEnable !== true ? 'View Profile' : 'Edit Profile'}</h2></Modal.Header>
					<Modal.Body className="registerForm">
						{this.state.editErrorMessge ? (
							<div className="alert alert-danger" role="alert">
								{this.state.editErrorMessge}
							</div>
						) : null}

						{this.state.providerLoader ? <LoadingSpinner /> : null}

						<Row className="show-grid">
							<Col xs={12} className="brd-right">
								<Formik
									initialValues={values}
									validationSchema={editProfileSchema}
									onSubmit={this.handleProfileSubmit}
									enableReinitialize={true}
								>
									{({
										values,
										errors,
										touched,
										isSubmitting,
									}) => {
										return (
											<Form className={disabled === false ? ('hideRequired') : null}>
												<Row className="show-grid">
													<Col xs={12} md={12}>
														<div className="sectionTitle mb-3">
															<h3>Company Information</h3>
														</div>
													</Col>
													<Col xs={12} md={6}>
														<FormGroup controlId="formControlsTextarea">
															<label>Company Email <span className="required">*</span></label>
															<Field
																name="email"
																type="text"
																className={'form-control'}
																autoComplete="nope"
																placeholder="Enter"
																value={values.company_email || ''}
																disabled="disabled"
															/>
															<FormControl.Feedback />
														</FormGroup>
													</Col>

													<Col xs={12} md={6}>
														<FormGroup controlId="formControlsTextarea">
															<label>Company Name <span className="required">*</span></label>
															<Field
																name="company_name"
																type="text"
																className={'form-control'}
																autoComplete="nope"
																placeholder="Enter"
																value={values.company_name || ''}
																disabled={disabled === false ? 'disabled' : ''}
															/>
															{errors.company_name && touched.company_name ? (
																<span className="errorMsg ml-3">{errors.company_name}</span>
															) : null}
															<FormControl.Feedback />
														</FormGroup>
													</Col>
													<Col xs={12} md={6}>
														<FormGroup controlId="formControlsTextarea">
															<label>Company Phone <span className="required">*</span></label>
															<Field
																name="company_phone"
																type="text"
																className={'form-control'}
																autoComplete="nope"
																placeholder="Enter"
																value={values.company_phone || ''}
																disabled={disabled === false ? 'disabled' : ''}
															/>
															{errors.company_phone && touched.company_phone ? (
																<span className="errorMsg ml-3">{errors.company_phone}</span>
															) : null}
															<FormControl.Feedback />
														</FormGroup>
													</Col>
													<Col xs={12} md={6}>
														<FormGroup controlId="formControlsTextarea">
															<label>Address <span className="required">*</span></label>
															<Field
																name="addr1"
																type="text"
																className={'form-control'}
																autoComplete="nope"
																placeholder="Enter"
																value={values.addr1 || ''}
																disabled={disabled === false ? 'disabled' : ''}
															/>
															{errors.addr1 && touched.addr1 ? (
																<span className="errorMsg ml-3">{errors.addr1}</span>
															) : null}
															<FormControl.Feedback />
														</FormGroup>
													</Col>
													<Col xs={12} md={6}>
														<FormGroup controlId="formControlsTextarea">
															<label>ADDRESS2 LINE 2(optional)</label>
															<Field
																name="addr2"
																type="text"
																className={'form-control'}
																autoComplete="nope"
																placeholder="Enter"
																value={values.addr2 || ''}
																disabled={disabled === false ? 'disabled' : ''}
															/>
															{errors.addr2 && touched.addr2 ? (
																<span className="errorMsg ml-3">{errors.addr2}</span>
															) : null}
															<FormControl.Feedback />
														</FormGroup>
													</Col>
													<Col xs={12} md={6}>
														<FormGroup controlId="formBasicText">
															<label>Country <span className="required">*</span></label>
															<Field
																required
																name="companyCountry"
																component="select"
																className={`input-elem ${values.companyCountry &&
																	'input-elem-filled'} form-control`}
																autoComplete="nope"
																value={this.state.companyCountry}
																disabled={disabled === false ? 'disabled' : ''}
																onChange={this.handleChange}
															>
																<option value="">
																	{this.state.countryList.length ? 'Select Country' : 'Loading...'}
																</option>
																{this.state.countryList.map(country => (
																	<option
																		key={country.id}
																		value={country.id}
																	>
																		{country.name}
																	</option>
																))}
															</Field>
															{errors.companyCountry && touched.companyCountry ? (
																<span className="errorMsg ml-3">
																	{errors.companyCountry}
																</span>
															) : null}
														</FormGroup>
													</Col>
													<Col xs={12} md={6}>
														<FormGroup controlId="formBasicText">
															<label>State <span className="required">*</span></label>
															<Field
																required
																name="companyState"
																component="select"
																className={`input-elem topShift ${values.companyState &&
																	'input-elem-filled'} form-control`}
																autoComplete="nope"
																value={this.state.companyState}
																disabled={disabled === false ? 'disabled' : ''}
																onChange={this.handleChange}
															>
																<option value="">
																	{this.state.companyStateList.length ? 'Select State' : 'Loading...'}
																</option>
																{this.state.companyStateList.map(state => (
																	<option value={state.id} key={state.id}>
																		{state.name}
																	</option>
																))}
															</Field>
															{errors.companyState &&
																touched.companyState ? (
																	<span className="errorMsg ml-3">
																		{errors.companyState}
																	</span>
																) : null}
														</FormGroup>
													</Col>
													<Col xs={12} md={6}>
														<FormGroup controlId="formBasicText">
															<label>City <span className="required">*</span></label>
															<Field
																required
																component="select"
																name="companyCity"
																placeholder="select"
																className={`input-elem topShift ${values.companyCity &&
																	'input-elem-filled'} form-control`}
																value={values.companyCity || ''}
																disabled={disabled === false ? 'disabled' : ''}
															>
																<option value="">
																	{this.state.companyCityList.length ? 'Select City' : 'Loading...'}
																</option>
																{this.state.companyCityList.map(city => (
																	<option value={city.id} key={city.id}>
																		{city.name}
																	</option>
																))}
															</Field>
															{errors.companyCity && touched.companyCity ? (
																<span className="errorMsg ml-3">
																	{errors.companyCity}
																</span>
															) : null}
														</FormGroup>
													</Col>
													<Col xs={12} md={6}>
														<label>Zip <span className="required">*</span></label>
														<FormGroup controlId="formBasicText">
															<Field
																name="companyZip"
																type="text"
																className={'form-control'}
																autoComplete="nope"
																placeholder="Zip"
																value={values.companyZip || ''}
																disabled={disabled === false ? 'disabled' : ''}
															/>
															{errors.companyZip && touched.companyZip ? (
																<span className="errorMsg ml-3">{errors.companyZip}</span>
															) : null}
															<FormControl.Feedback />
														</FormGroup>
													</Col>

													<Col xs={12} md={12}>
														<div className="sectionTitle my-3">
															<h3>Personal Information</h3>
														</div>
													</Col>
													<Col xs={12} md={6}>
														<FormGroup controlId="formControlsTextarea">
															<label>First Name <span className="required">*</span></label>
															<Field
																name="first_name"
																type="text"
																className={'form-control'}
																autoComplete="nope"
																placeholder="Enter"
																value={values.first_name || ''}
																disabled={disabled === false ? 'disabled' : ''}
															/>
															{errors.first_name && touched.first_name ? (
																<span className="errorMsg ml-3">{errors.first_name}</span>
															) : null}
															<FormControl.Feedback />
														</FormGroup>
													</Col>
													<Col xs={12} md={6}>
														<FormGroup controlId="formControlsTextarea">
															<label>Last Name <span className="required">*</span></label>
															<Field
																name="last_name"
																type="text"
																className={'form-control'}
																autoComplete="nope"
																placeholder="Enter"
																value={values.last_name || ''}
																disabled={disabled === false ? 'disabled' : ''}
															/>
															{errors.last_name && touched.last_name ? (
																<span className="errorMsg ml-3">{errors.last_name}</span>
															) : null}
															<FormControl.Feedback />
														</FormGroup>
													</Col>
													<Col xs={12} md={6}>
														<FormGroup controlId="formControlsTextarea">
															<label>Phone <span className="required">*</span></label>
															<Field
																name="phone"
																type="text"
																className={'form-control'}
																autoComplete="nope"
																placeholder="Enter"
																value={values.phone || ''}
																disabled={disabled === false ? 'disabled' : ''}
															/>
															{errors.phone && touched.phone ? (
																<span className="errorMsg ml-3">{errors.phone}</span>
															) : null}
															<FormControl.Feedback />
														</FormGroup>
													</Col>
													<Col xs={12} md={6}>
														<FormGroup controlId="formControlsTextarea">
															<label>Address <span className="required">*</span></label>
															<Field
																name="address"
																type="text"
																className={'form-control'}
																autoComplete="nope"
																placeholder="Enter"
																value={values.address || ''}
																disabled={disabled === false ? 'disabled' : ''}
															/>
															{errors.address && touched.address ? (
																<span className="errorMsg ml-3">{errors.address}</span>
															) : null}
															<FormControl.Feedback />
														</FormGroup>
													</Col>
													<Col xs={12} md={6}>
														<FormGroup controlId="formControlsTextarea">
															<label>ADDRESS2 LINE 2(optional)</label>
															<Field
																name="address2"
																type="text"
																className={'form-control'}
																autoComplete="nope"
																placeholder="Enter"
																value={values.address2 || ''}
																disabled={disabled === false ? 'disabled' : ''}
															/>
															{errors.address2 && touched.address2 ? (
																<span className="errorMsg ml-3">{errors.address2}</span>
															) : null}
															<FormControl.Feedback />
														</FormGroup>
													</Col>
													<Col xs={12} md={6}>
														<FormGroup controlId="formBasicText">
															<label>Country<span className="required">*</span></label>
															<Field
																required
																name="personalCountry"
																component="select"
																className={`input-elem ${values.personalCountry && 'input-elem-filled'} form-control`}
																autoComplete="nope"
																value={this.state.personalCountry}
																disabled={disabled === false ? 'disabled' : ''}
																onChange={this.handleChange}
															>
																<option value="">{this.state.countryList.length ? 'Select Country' : 'Loading...'}</option>
																{this.state.countryList.map(country => (
																	<option key={country.id} value={country.id}>
																		{country.name}
																	</option>
																))}
															</Field>
															{errors.personalCountry && touched.personalCountry ? (
																<span className="errorMsg ml-3">
																	{errors.personalCountry}
																</span>
															) : null}
														</FormGroup>
													</Col>
													<Col xs={12} md={6}>
														<FormGroup controlId="formBasicText">
															<label>State<span className="required">*</span></label>
															<Field
																required
																name="personalState"
																component="select"
																className={`input-elem topShift ${values.personalState && 'input-elem-filled'} form-control`}
																autoComplete="nope"
																value={this.state.personalState}
																disabled={disabled === false ? 'disabled' : ''}
																onChange={this.handleChange}
															>
																<option value="">{this.state.personalStateList.length ? 'Select Sate' : 'Loading...'}</option>
																{this.state.personalStateList.map(state => (
																	<option value={state.id} key={state.id}>
																		{state.name}
																	</option>
																))}
															</Field>
															{errors.personalState &&
																touched.personalState ? (
																	<span className="errorMsg ml-3">
																		{errors.personalState}
																	</span>
																) : null}
														</FormGroup>
													</Col>
													<Col xs={12} md={6}>
														<FormGroup controlId="formBasicText">
															<label>City<span className="required">*</span></label>
															<Field
																required
																component="select"
																name="personalCity"
																placeholder="select"
																className={`input-elem topShift ${values.personalCity && 'input-elem-filled'} form-control`}
																value={values.personalCity || ''}
																disabled={disabled === false ? 'disabled' : ''}
															>
																<option value="">{this.state.personalCityList.length ? 'Select City' : 'Loading...'}</option>
																{this.state.personalCityList.map(city => (
																	<option value={city.id} key={city.id}>
																		{city.name}
																	</option>
																))}
															</Field>
															{errors.personalCity && touched.personalCity ? (
																<span className="errorMsg ml-3">
																	{errors.personalCity}
																</span>
															) : null}
														</FormGroup>
													</Col>
													<Col xs={12} md={6}>
														<FormGroup controlId="formBasicText">
															<label>Zip <span className="required">*</span></label>
															<Field
																name="personalZip"
																type="text"
																className={'form-control'}
																autoComplete="nope"
																placeholder="Enter"
																value={values.personalZip || ''}
																disabled={disabled === false ? 'disabled' : ''}
															/>
															{errors.personalZip && touched.personalZip ? (
																<span className="errorMsg ml-3">{errors.personalZip}</span>
															) : null}
															<FormControl.Feedback />
														</FormGroup>
													</Col>
													<Col xs={12} sm={6} md={6}>
														<FormGroup controlId="formBasicText">
															<label>Profile Pic</label>
															<input
																name="selectedFile"
																type="file"
																onChange={this.onFileChange}
																className={'form-control'}
																autoComplete="nope"
																placeholder="Enter"
																disabled={disabled === false ? 'disabled' : ''}
															/>
															{this.state.uploadErrorMessage && <span className="errorMsg ml-3">{this.state.uploadErrorMessage}</span>}
															<FormControl.Feedback />
														</FormGroup>
													</Col>
													{values.profile_pic &&
														<Row className="show-grid">
															<Col xs={12} md={12}>
																<img height="150" width="150" className="ml-3 rounded" src={AppConst.UPLOADURL + '/profile/' + values.profile_pic}></img>
															</Col>
														</Row>
													}
												</Row>
												<Row className="show-grid text-center mt-3">
													<Col xs={12} md={12}>
														<Fragment>
															{
																this.state.editProfileEnable !== true ? (
																	<Fragment>
																		<Button className="blue-btn border-0" onClick={this.handleEditVendorEnable}>Edit</Button>
																	</Fragment>
																) : (
																		<Fragment>
																			<Button onClick={this.handleProfileModelClose} className="but-gray border-0 mr-2"> Cancel </Button>
																			<Button type="submit" className="blue-btn ml-2 border-0" disabled={isSubmitting}>Save</Button>
																		</Fragment>
																	)
															}
														</Fragment>
													</Col>
												</Row>
												{disabled === false ? null : (<Fragment>
													<Row>
														<Col md={12}>
															<p style={{ paddingTop: '10px' }}><span className="required">*</span> These fields are required.</p>
														</Col>
													</Row>
												</Fragment>)}
											</Form>
										);
									}}
								</Formik>
							</Col>
						</Row>
					</Modal.Body>
				</Modal>

				{/*====== confirmation popup  ===== */}
				<Modal show={this.state.statusConfirMmsg} onHide={this.handleStatusChangedClose} className="payOptionPop">
					<Modal.Body className="text-center">
						<Row>
							<Col md={12} className="text-center">
								<Image src={SuccessIco} />
							</Col>
						</Row>
						<Row>
							<Col md={12} className="text-center">
								<h5>{this.state.successMessage}</h5>
							</Col>
						</Row>
						<Button onClick={this.handleStatusChangedClose} className="but-gray mt-3">Return</Button>
					</Modal.Body>
				</Modal>

			</div>
		);
	}
}

/*Header.propTypes = {
	location: PropTypes.object,
	history: PropTypes.object,
	sessionTokenUpdate: PropTypes.func,
	handelUser: PropTypes.func,
	token: PropTypes.any,
	authlogoutMsg: PropTypes.string,
	resetLogoutMsg: PropTypes.func,
};

const mapStateToProps = (state) => {
	return {
		token: state.auth.token,
		authlogoutMsg: state.auth.logoutMsg,

	};
};

const mapDispatchToProps = dispatch => {
	return {
		sessionTokenUpdate: (key, token) => dispatch(accessToken(key, token)),
		updateAuthState: (authItem) => dispatch(authDataUpdate(authItem)),
		handelUser: (user) => dispatch(handelUser(user)),
		resetLogoutMsg: data => dispatch(logoutMsg(data)),
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);*/

const mapStateToProps = state => {
	return {
		token: state.loginDetails.token,
		user: state.userDetails
	};
};

const mapDispatchToProps = dispatch => {
	return {
		AddUser: (login) => dispatch({ type: 'SAVE_LOGINDETAILS', login: login }),
		AddUserDetails: (user) => dispatch({ type: 'SAVE_USERDETAILS', user: user })
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);