import axios from 'axios';
import * as AppConst from '../common/constants';

export const fetchCmsList = () => {
    const url = `${AppConst.APIURL}/api/getCmsList`;
    return axios.get(url);
};

export const fetchGemsTrendingList = () => {
    const url = `${AppConst.APIURL}/api/trendingGems`;
    return axios.get(url);
}

export const fetchCmsDetails = (slug) => {
    const url = `${AppConst.APIURL}/api/getCmsDetails/${slug}`;
    return axios.get(url);
};

export const checkCmsSlug = () => {
    const url = `${AppConst.APIURL}/api/checkCmsSlug`;
    return axios.get(url);
};

export const fetchGemsList = (gems_name, latitude, longitude, pageSize, page) => {
    const url = `${AppConst.APIURL}/api/gemsSearchFront?gems_name=${gems_name}&latitude=${latitude}&longitude=${longitude}&pageSize=${pageSize}&page=${page}`;
    return axios.get(url);
};

export const fetchCountryList = () => {
    const url = `${AppConst.APIURL}/api/countries`;
    return axios.get(url);
};

export const fetchStateList = (id) => {
    let params = {
        'id': id,
    };
    const url = `${AppConst.APIURL}/api/getStates`;
    return axios.post(url, params);
};

export const fetchCityList = (id) => {
    let params = {
        'id': id,
    };
    const url = `${AppConst.APIURL}/api/getCities`;
    return axios.post(url, params);
};

export const fetchHomepageList = (type = 0) => {
    const url = `${AppConst.APIURL}/api/homepageList?type=${type}`;
    return axios.get(url);
};

export const fetchTestimonialList = () => {
    const url = `${AppConst.APIURL}/api/testimonialList`;
    return axios.get(url);
};

export const fetchNewsDetails = (slug) => {
    const url = `${AppConst.APIURL}/api/news/${slug}`;
    return axios.get(url);
};

export const fetchNewsList = () => {
    const url = `${AppConst.APIURL}/api/newsList`;
    return axios.get(url);
};

export const fetchFaqCategoryList = () => {
    const url = `${AppConst.APIURL}/api/faqCategoryList`;
    return axios.get(url);
};

export const fetchCoupon = (pageSize, page, couponSearchKey) => {
    const sessionKey = localStorage.getItem('token');
    axios.interceptors.request.use(req => {
        req.headers.Authorization = 'Bearer ' + `${sessionKey}`;
        return req;
    });
    const url = `${AppConst.APIURL}/api/couponList?creator_id=${localStorage.getItem('user_id')}&pageSize=${pageSize}&page=${page}&coupon=${couponSearchKey}`;
    return axios.get(url);
}

export const couponStatusChange = (id, status) => {
    const sessionKey = localStorage.getItem('token');
    axios.interceptors.request.use(req => {
        req.headers.Authorization = 'Bearer ' + `${sessionKey}`;
        return req;
    });
    let updateArray = {
        'status': status
    };
    const url = `${AppConst.APIURL}/api/statusChange/${id}`;
    return axios.put(url, updateArray);
}

export const addCoupon = (value) => {
    const sessionKey = localStorage.getItem('token');
    axios.interceptors.request.use(req => {
        req.headers.Authorization = 'Bearer ' + `${sessionKey}`;
        return req;
    });
    const url = `${AppConst.APIURL}/api/generateCoupon`;
    return axios.post(url, value);
}

export const viewCouponDetails = (code) => {
    const sessionKey = localStorage.getItem('token');
    axios.interceptors.request.use(req => {
        req.headers.Authorization = 'Bearer ' + `${sessionKey}`;
        return req;
    });
    const url = `${AppConst.APIURL}/api/couponDetails/${code}`;
    return axios.get(url);
}

export const providerGemList = (user_id, pageSize, page, searchKey) => {
    const sessionKey = localStorage.getItem('token');
    axios.interceptors.request.use(req => {
        req.headers.Authorization = 'Bearer ' + `${sessionKey}`;
        return req;
    });
    const url = `${AppConst.APIURL}/api/providerGemList/${user_id}?pageSize=${pageSize}&page=${page}&coupon=${searchKey}`;
    return axios.get(url);
}

export const deleteCoupon = (id) => {
    const sessionKey = localStorage.getItem('token');
    axios.interceptors.request.use(req => {
        req.headers.Authorization = 'Bearer ' + `${sessionKey}`;
        return req;
    });
    const url = `${AppConst.APIURL}/api/deleteCoupon/${id}`;
    return axios.delete(url);
}

export const userList = () => {
    const sessionKey = localStorage.getItem('token');
    axios.interceptors.request.use(req => {
        req.headers.Authorization = 'Bearer ' + `${sessionKey}`;
        return req;
    });
    const url = `${AppConst.APIURL}/api/userLists`;
    return axios.get(url);
}

export const assignCoupon = (value) => {
    const sessionKey = localStorage.getItem('token');
    axios.interceptors.request.use(req => {
        req.headers.Authorization = 'Bearer ' + `${sessionKey}`;
        return req;
    });
    const url = `${AppConst.APIURL}/api/assignCoupon`;
    return axios.post(url, value);
}

export const redeemCoupon = (value) => {
    const sessionKey = localStorage.getItem('token');
    axios.interceptors.request.use(req => {
        req.headers.Authorization = 'Bearer ' + `${sessionKey}`;
        return req;
    });
    const url = `${AppConst.APIURL}/api/redeemCoupon`;
    return axios.post(url, value);
}

export const editCoupon = (id, value) => {
    const sessionKey = localStorage.getItem('token');
    axios.interceptors.request.use(req => {
        req.headers.Authorization = 'Bearer ' + `${sessionKey}`;
        return req;
    });
    const url = `${AppConst.APIURL}/api/updateCoupon/${id}`;
    return axios.put(url, value);
}

export const updateProvider = (id, value) => {
    const sessionKey = localStorage.getItem('token');
    axios.interceptors.request.use(req => {
        req.headers.Authorization = 'Bearer ' + `${sessionKey}`;
        return req;
    });
    const url = `${AppConst.APIURL}/api/updateProvider/${id}`;
    return axios.post(url, value);
}

export const viewProvider = (id) => {
    const sessionKey = localStorage.getItem('token');
    axios.interceptors.request.use(req => {
        req.headers.Authorization = 'Bearer ' + `${sessionKey}`;
        return req;
    });
    const url = `${AppConst.APIURL}/api/providerInfo?user_id=${id}`;
    return axios.get(url);
}

export const updatePassword = (value) => {
    const sessionKey = localStorage.getItem('token');
    axios.interceptors.request.use(req => {
        req.headers.Authorization = 'Bearer ' + `${sessionKey}`;
        return req;
    });
    const url = `${AppConst.APIURL}/api/updatePassword`;
    return axios.post(url, value);
}

export const paymentOrders = (id, pageSize, page) => {
    const sessionKey = localStorage.getItem('token');
    axios.interceptors.request.use(req => {
        req.headers.Authorization = 'Bearer ' + `${sessionKey}`;
        return req;
    });
    const url = `${AppConst.APIURL}/api/subscriptionList?user_id=${id}?pageSize=${pageSize}&page=${page}`;
    return axios.get(url);
}

export const verifyAccount = (token) => {
    const url = `${AppConst.APIURL}/api/verifyAccount/${token}`;
    return axios.get(url);
}

export const fetchCardList = (userId) => {
    const sessionKey = localStorage.getItem('token');
    axios.interceptors.request.use(req => {
        req.headers.Authorization = 'Bearer ' + `${sessionKey}`;
        return req;
    });
    const url = `${AppConst.APIURL}/api/creditCardLists?user_id=${userId}`;
    return axios.get(url);
}

export const deleteCard = (card_id) => {
    const sessionKey = localStorage.getItem('token');
    axios.interceptors.request.use(req => {
        req.headers.Authorization = 'Bearer ' + `${sessionKey}`;
        return req;
    });
    const url = `${AppConst.APIURL}/api/deleteCard/${card_id}`;
    return axios.delete(url);
}

export const createCard = (value) => {
    const sessionKey = localStorage.getItem('token');
    axios.interceptors.request.use(req => {
        req.headers.Authorization = 'Bearer ' + `${sessionKey}`;
        return req;
    });
    const url = `${AppConst.APIURL}/api/createCard`;
    return axios.post(url, value);
}

export const updateCard = (value) => {
    const sessionKey = localStorage.getItem('token');
    axios.interceptors.request.use(req => {
        req.headers.Authorization = 'Bearer ' + `${sessionKey}`;
        return req;
    });
    const url = `${AppConst.APIURL}/api/updateCard`;
    return axios.post(url, value);
}

export const fetchInvoice = (id) => {
    const sessionKey = localStorage.getItem('token');
    axios.interceptors.request.use(req => {
        req.headers.Authorization = 'Bearer ' + `${sessionKey}`;
        return req;
    });
    const url = `${AppConst.APIURL}/api/invoice?id=${id}`;
    return axios.post(url);
}